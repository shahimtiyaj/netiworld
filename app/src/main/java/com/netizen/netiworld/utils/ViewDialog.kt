package com.netizen.netiworld.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.Window
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import com.netizen.netiworld.R

class ViewDialog(internal var activity: Activity) {
    internal lateinit var dialog: Dialog

    fun showDialog() {

        dialog = Dialog(activity as Context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loader_layout)

        val gifImageView = dialog.findViewById<ImageView>(R.id.custom_loading_imageView)
        val imageViewTarget = GlideDrawableImageViewTarget(gifImageView)

        Glide.with(activity)
            .load(R.drawable.loader)
            .placeholder(R.drawable.loader)
            .centerCrop()
            .crossFade()
            .into(imageViewTarget)
        dialog.show()
    }

    fun hideDialog() {
        dialog.dismiss()
    }
}