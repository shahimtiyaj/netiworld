package com.netizen.netiworld.reports

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.BalanceDepositListAdapter
import com.netizen.netiworld.model.BalanceDepositGetData
import com.netizen.netiworld.model.BalanceDepositPostForList
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.EndlessRecyclerViewScrollListener
import com.netizen.netiworld.utils.MyUtilsClass
import com.netizen.netiworld.utils.PaginationScrollListener
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class BalanceDepositListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: BalanceDepositListAdapter? = null
    private var balanceDepositArrayList: ArrayList<BalanceDepositGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var total_found: TextView? = null

    private var v: View? = null
    private var searchView: SearchView? = null

    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    //private lateinit var scrollListener: RecyclerView.OnScrollListener

    private var progress: ProgressBar? = null

    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var page: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        total_found = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        progressBarInit()
        toolBarInit()
        recyclerViewInit()

        getBalanceDepositListData(page!!)

        //onScrollBarInit()

     recyclerView?.addOnScrollListener(object : PaginationScrollListener(mLayoutManager!!) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                page = page!! + 1
                getBalanceDepositListData(page!!)
            }
        })

        /*try {
            getBalanceDepositListData(0)
            onScrollBarInit()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }*/
    }

    /*
  recycler view initialization method
  */
    fun recyclerViewInit() {
        // Initialize item list
        //balanceDepositArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
       // adapter = BalanceDepositListAdapter(applicationContext, balanceDepositArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.setHasFixedSize(true)
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
       // recyclerView?.adapter = adapter

    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (BalanceDepositDashboard.requestType == "Deposit") {
            toolbarTitle?.text = "Balance Deposit List"
        } else if (BalanceDepositDashboard.requestType == "Withdraw") {
            toolbarTitle?.text = "Balance Withdraw List"
        }
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceDepositDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext?.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint = Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_mssg_deposit_log_serach) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.context?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null)

        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.context?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null)

        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)

        val closeId = searchView?.context?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )

        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)

    }

    private fun onScrollBarInit() {
        scrollListener = object : EndlessRecyclerViewScrollListener(mLayoutManager!!) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //Each page return 5 items from web service  when scroll.

                getBalanceDepositListData(page)
            }
        }
        //Adds the scroll listener to RecyclerView
        recyclerView?.addOnScrollListener(scrollListener!!)
        //recyclerView?.isNestedScrollingEnabled = false
    }

    /**
     * Progress dialogue initialization
     */
    private fun progressBarInit() {
       progress = findViewById(R.id.progress_id)
    }

    private fun getBalanceDepositListData(page: Int) {

        progress?.visibility = View.VISIBLE

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostForList(
            MyUtilsClass.getDateFormatForSerachData(BalanceDepositDateSelection.startDate),
            MyUtilsClass.getDateFormatForSerachData(BalanceDepositDateSelection.endDate),
            BalanceDepositDashboard.requestType, "", 5, page)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceDepositGetData(balanceDeposit, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceDepositGetData>> {
            override fun onResponse(
                call: Call<List<BalanceDepositGetData>>,
                response: Response<List<BalanceDepositGetData>>
            ) {
                Log.d("onResponse", "Balance Deposit:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())
                    balanceDepositArrayList = response.body() as ArrayList<BalanceDepositGetData>?
                    adapter = BalanceDepositListAdapter(applicationContext, balanceDepositArrayList!!)
                    recyclerView?.adapter = adapter
                    total_found?.text = balanceDepositArrayList?.size.toString()
                }

                 if(balanceDepositArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                progress?.visibility = View.GONE
                isLastPage=false
                isLoading=false
                adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<BalanceDepositGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                progress?.visibility = View.GONE

                Toasty.error(applicationContext, "Sorry ! No Data Found.", Toast.LENGTH_LONG, true).show() } })
    }
}
