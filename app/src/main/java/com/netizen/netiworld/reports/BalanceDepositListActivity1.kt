package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.PaginationAdapter
import com.netizen.netiworld.model.BalanceDepositGetData
import com.netizen.netiworld.model.BalanceDepositPostForList
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.EndlessRecyclerViewScrollListener
import com.netizen.netiworld.utils.PaginationScrollListener
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.set

class BalanceDepositListActivity1: AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: PaginationAdapter? = null
    private var balanceDepositArrayList: List<BalanceDepositGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var total_found: TextView? = null

    private var v: View? = null
    private var searchView: SearchView? = null

    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    // private lateinit var scrollListener: RecyclerView.OnScrollListener

    private var progress: ProgressBar? = null

    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var page: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)

        total_found = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        progressBarInit()
        toolBarInit()
        recyclerViewInit()

        getBalanceDepositListData(0)

        recyclerView?.addOnScrollListener(object : PaginationScrollListener(mLayoutManager!!) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                page = page!! + 1
                getBalanceDepositListDataNext(page!!)
            }
        })
    }

    /*
  recycler view initialization method
  */
    fun recyclerViewInit() {
        balanceDepositArrayList = ArrayList<BalanceDepositGetData>()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = PaginationAdapter(applicationContext, balanceDepositArrayList as MutableList<BalanceDepositGetData>)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.setHasFixedSize(true)
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter

    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (BalanceDepositDashboard.requestType == "Deposit") {
            toolbarTitle?.text = "Balance Deposit List"
        } else if (BalanceDepositDashboard.requestType == "Withdraw") {
            toolbarTitle?.text = "Balance Withdraw List"
        }
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceDepositDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext?.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_mssg_deposit_log_serach) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.context?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.white)

        val searchiconId = searchView?.context?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )

        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.context?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null)

        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)

    }

    /**
     * Progress dialogue initialization
     */
    private fun progressBarInit() {
        progress = findViewById(R.id.progress_id)
    }

    private fun getBalanceDepositListData(page: Int) {
        progress?.visibility = View.VISIBLE

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostForList(
            BalanceDepositDateSelection.startDate,
            BalanceDepositDateSelection.endDate, BalanceDepositDashboard.requestType, "", 5, page
        )

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceDepositGetData(balanceDeposit, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceDepositGetData>> {
            override fun onResponse(
                call: Call<List<BalanceDepositGetData>>,
                response: Response<List<BalanceDepositGetData>>
            ) {
                Log.d("onResponse", "Balance Deposit:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())
                    val balanceDepositArrayList = response.body()!!

                    progress?.visibility = View.GONE
                    adapter?.addAll(balanceDepositArrayList)
                    recyclerView?.adapter = PaginationAdapter(applicationContext, balanceDepositArrayList as MutableList<BalanceDepositGetData>)

                    if (page <= 4) adapter?.addLoadingFooter()
                     else isLastPage = true

                    total_found?.text = balanceDepositArrayList.size.toString()
                }

            }

            override fun onFailure(call: Call<List<BalanceDepositGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                progress?.visibility = View.GONE

                Toasty.error(
                    applicationContext,
                    "Balance Deposit Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }

    private fun getBalanceDepositListDataNext(page: Int) {
        progress?.visibility = View.VISIBLE

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostForList(
            BalanceDepositDateSelection.startDate,
            BalanceDepositDateSelection.endDate, BalanceDepositDashboard.requestType, "", 5, page)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceDepositGetData(balanceDeposit, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceDepositGetData>> {
            override fun onResponse(
                call: Call<List<BalanceDepositGetData>>,
                response: Response<List<BalanceDepositGetData>>
            ) {
                Log.d("onResponse", "Balance Deposit:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())
                    adapter?.removeLoadingFooter()
                    isLoading = false

                    val balanceDepositArrayList = response.body()!!
                    adapter?.addAll(balanceDepositArrayList)
                    recyclerView?.adapter = PaginationAdapter(applicationContext, balanceDepositArrayList as MutableList<BalanceDepositGetData>)

                    if (page != 4) adapter?.addLoadingFooter()
                    else isLastPage = true

                    total_found?.text = balanceDepositArrayList.size.toString()
                }
            }

            override fun onFailure(call: Call<List<BalanceDepositGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                progress?.visibility = View.GONE

                Toasty.error(
                    applicationContext,
                    "Balance Deposit Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()
            }
        })
    }
}
