package com.netizen.netiworld.reports

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R

class  BalanceDepositDashboard : AppCompatActivity() {

    private var deposit: CardView? = null
    private var withdraw: CardView? = null
    private var transfer: CardView? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_dashboard)
        toolBarInit()

        deposit = findViewById<View>(R.id.deposit) as CardView
        withdraw = findViewById<View>(R.id.withdraw) as CardView
        transfer = findViewById<View>(R.id.transfer) as CardView

        OnclickView()
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet"

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    fun OnclickView() {

        deposit?.setOnClickListener {
            try {
                requestType="Deposit"
                val intent = Intent(this, BalanceDepositDateSelection::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        withdraw?.setOnClickListener {
            requestType="Withdraw"
            val intent = Intent(this, BalanceDepositDateSelection::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        transfer?.setOnClickListener {
            requestType="Transfer"

            val intent = Intent(this, BalanceDepositDateSelection::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

    }

    companion object {
        private val TAG = "BalanceDepositDateSelection"
        var requestType = ""
    }
}
