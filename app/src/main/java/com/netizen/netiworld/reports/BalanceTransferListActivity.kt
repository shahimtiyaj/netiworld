package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.BalanceDepositListAdapter
import com.netizen.netiworld.adapter.BalanceTransferListAdapter
import com.netizen.netiworld.model.BalanceDepositGetData
import com.netizen.netiworld.model.BalanceDepositPostForList
import com.netizen.netiworld.model.BalanceTransferGetData
import com.netizen.netiworld.model.JSONResponse
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class BalanceTransferListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: BalanceTransferListAdapter? = null
    private var balanceTransferArrayList: ArrayList<BalanceTransferGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var total_found: TextView? = null

    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        total_found = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        toolBarInit()
        recyclerViewInit()
        getBalanceTransferListData()
    }

    /*
  recycler view initialization method
  */
    fun recyclerViewInit() {
        // Initialize item list
       // balanceTransferArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
        //adapter = BalanceTransferListAdapter(applicationContext, balanceTransferArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        //recyclerView?.adapter = adapter
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (BalanceDepositDashboard.requestType == "Deposit") {
            toolbarTitle?.text = "Balance Deposit List"
        } else if (BalanceDepositDashboard.requestType == "Withdraw") {
            toolbarTitle?.text = "Balance Withdraw List"
        } else if (BalanceDepositDashboard.requestType == "Transfer") {
            toolbarTitle?.text = "Balance Transfer List"
        }

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceDepositDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }


    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_deposit_transfer) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)

    }

   private fun getBalanceTransferListData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

       /* val balanceTransfer = BalanceDepositPostForList(
            BalanceDepositDateSelection.startDate,
            BalanceDepositDateSelection.endDate, BalanceDepositDashboard.requestType,
            BalanceDepositDateSelection.selectTransType, 100, 0)*/

       val balanceTransfer = BalanceDepositPostForList(
           MyUtilsClass.getDateFormatForSerachData(BalanceDepositDateSelection.startDate),
           MyUtilsClass.getDateFormatForSerachData(BalanceDepositDateSelection.endDate), 100)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceTransferGetData(balanceTransfer, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceTransferGetData>> {
            override fun onResponse(
                call: Call<List<BalanceTransferGetData>>,
                response: Response<List<BalanceTransferGetData>>
            ) {
                Log.d("onResponse", "Balance Transfer:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())

                     balanceTransferArrayList = response.body() as ArrayList<BalanceTransferGetData>?
                     adapter = BalanceTransferListAdapter(applicationContext, balanceTransferArrayList!!)
                     recyclerView?.adapter = adapter

                     total_found?.text = balanceTransferArrayList?.size.toString()
                }

                if(balanceTransferArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            }

            override fun onFailure(call: Call<List<BalanceTransferGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Balance Transfer Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }

   /* private fun getBalanceTransferListDataTest() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceTransfer = BalanceDepositPostForList(
            BalanceDepositDateSelection.startDate,
            BalanceDepositDateSelection.endDate, BalanceDepositDashboard.requestType,
            BalanceDepositDateSelection.selectTransType, 50, 0)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceTransferGetData(balanceTransfer, header)

        //calling the api
        call?.enqueue(object : Callback<JSONResponse> {
            override fun onResponse(
                call: Call<JSONResponse>,
                response: Response<JSONResponse>
            ) {
                Log.d("onResponse", "Balance Transfer:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())


                    val jsonResponse: JSONResponse
                    jsonResponse= response.body()!!
                    balanceTransferArrayList= jsonResponse as ArrayList<BalanceTransferGetData>?
                    recyclerView?.adapter = BalanceTransferListAdapter(applicationContext, balanceTransferArrayList!!)
                    total_found?.text = balanceTransferArrayList?.size.toString()
                }

            }

            override fun onFailure(call: Call<JSONResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Balance Transfer Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }*/
}
