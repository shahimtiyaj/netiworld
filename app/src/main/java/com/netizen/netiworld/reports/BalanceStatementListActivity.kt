package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.MainActivity.Companion.balanceStatement
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.BalanceStatementListAdapter
import com.netizen.netiworld.model.BalanceDepositPostForList
import com.netizen.netiworld.model.BalanceMessageGetData
import com.netizen.netiworld.model.BalanceStatementGetData
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.logs_wallet_transfer_row.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.logging.Logger

class BalanceStatementListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: BalanceStatementListAdapter? = null
    private var balanceStatementArrayList: ArrayList<BalanceStatementGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var total_found: TextView? = null
    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        total_found = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        toolBarInit()

        recyclerViewInit()

        getBalanceStatementListData()


    }

    /*
 recycler view initialization method
 */
    fun recyclerViewInit() {
        // Initialize item list
       // balanceStatementArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
        //adapter = BalanceStatementListAdapter(applicationContext, balanceStatementArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
       // recyclerView?.adapter = adapter
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbarTitle?.text = "Balance Statement"

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceStatementDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_balance_statement) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return true

    }


    private fun getBalanceStatementListData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostForList(
            MyUtilsClass.getDateFormatForSerachData(BalanceStatementDateSelection.startDateStatement),
            MyUtilsClass.getDateFormatForSerachData(BalanceStatementDateSelection.endDateStatement),
            "", "", 50, 0)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceStatementGetData(balanceDeposit, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceStatementGetData>> {
            override fun onResponse(
                call: Call<List<BalanceStatementGetData>>,
                response: Response<List<BalanceStatementGetData>>
            ) {
                Log.d("onResponse", "Balance Deposit:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Token:" + response.body())

                    balanceStatementArrayList = response.body()?.let(fun(it: List<BalanceStatementGetData>): ArrayList<BalanceStatementGetData> {
                        return ArrayList(
                            it
                        )
                    })

                    adapter = BalanceStatementListAdapter(applicationContext, balanceStatementArrayList!!)
                    recyclerView?.adapter = adapter
                    total_found?.text = balanceStatementArrayList?.size.toString()
                }

                 if(balanceStatementArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                adapter?.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<List<BalanceStatementGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Balance Deposit Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }

}
