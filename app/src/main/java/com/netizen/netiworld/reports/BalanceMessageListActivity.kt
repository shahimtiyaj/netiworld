package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.BalanceMessageListAdapter
import com.netizen.netiworld.model.BalanceDepositPostForList
import com.netizen.netiworld.model.BalanceMessageGetData
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.set

class BalanceMessageListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: BalanceMessageListAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var total_found: TextView? = null

    private var v: View? = null
    private var searchView: SearchView? = null


    private var messageRechargeArrayList: ArrayList<BalanceMessageGetData>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        total_found = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        toolBarInit()
        recyclerViewInit()
        getMessageRechargeListData()
    }


    /*
recycler view initialization method
*/
    fun recyclerViewInit() {
        // Initialize item list
        messageRechargeArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = BalanceMessageListAdapter(applicationContext, messageRechargeArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbarTitle?.text = "Message Recharge List"
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceMessageDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_mssg_type) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)

    }


    private fun getMessageRechargeListData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val balanceTransfer = BalanceDepositPostForList(MyUtilsClass.getDateFormatForSerachData(BalanceMessageDateSelection.startDateMessage),
                MyUtilsClass.getDateFormatForSerachData(BalanceMessageDateSelection.endDateMessage), BalanceDepositDashboard.requestType,
            "Recharge", 0, 0)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.messageRechargeGetData(balanceTransfer, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceMessageGetData>> {
            override fun onResponse(
                call: Call<List<BalanceMessageGetData>>,
                response: Response<List<BalanceMessageGetData>>
            ) {
                Log.d("onResponse", "Message Recharge List:" + response.body())

                if (response.code() == 302) {
                    Log.d("onResponse", "Recharge List:" + response.body())

                    val messageRecharge = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(messageRecharge!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        val c1 = c.getJSONObject("productInfoDTO")
                        val c2 = c.getJSONObject("productPurchaseLogDTO")
                        val productName = c1.getString("productName")
                        val salesPrice = c2.getDouble("unitPrice")
                        val productQuantity=c.getLong("quantity")
                        val paidAmount=c2.getString("paidAmount")

                        val payableAmount= salesPrice * productQuantity

                        // Get the item model
                        val messageRechargeList = BalanceMessageGetData()
                        //set the json data in the model
                        messageRechargeList.setTrxDate(c.getString("trxDate"))
                        messageRechargeList.setQuantity(productQuantity)
                        messageRechargeList.setPayableAmount(paidAmount.toDouble())
                        messageRechargeList.setProductName(productName)

                        messageRechargeArrayList?.add(messageRechargeList)

                    }

                }

                 if(messageRechargeArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                total_found?.text = messageRechargeArrayList?.size.toString()

                adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<BalanceMessageGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Balance Transfer Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }

}
