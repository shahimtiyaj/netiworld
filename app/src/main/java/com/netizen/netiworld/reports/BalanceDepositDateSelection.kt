package com.netizen.netiworld.reports

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity.Companion.messageRecharge
import com.netizen.netiworld.R
import com.netizen.netiworld.reports.BalanceDepositDashboard.Companion.requestType
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class BalanceDepositDateSelection : AppCompatActivity(), AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var fromDatePickerDialog: DatePickerDialog? = null
    private var toDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private var v: View? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    private var btn_search_deposit_logs: Button? = null
    private var fromDate: EditText? = null
    private var toDate: EditText? = null

    private var linearLayout5: LinearLayout? = null
    private var linearLayout6: LinearLayout? = null

    private var spinner_transactionType: Spinner? = null
    //private var selectTransType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_selection)

        toolBarInit()
        viewInit()
    }

    /**
     * SetUp View method
     */
    fun viewInit() {
        fromDate = findViewById<View>(R.id.start_date) as EditText
        toDate = findViewById<View>(R.id.end_date) as EditText

        if (requestType == "Transfer") {
            spinner_transactionType = findViewById<View>(R.id.spinner_trans_type) as Spinner
            spinner_transactionType?.onItemSelectedListener = this

           // linearLayout5 = findViewById(R.id.linear5) as LinearLayout
           // linearLayout6 = findViewById(R.id.linear6) as LinearLayout

           // linearLayout5?.visibility = View.VISIBLE
           // linearLayout6?.visibility = View.VISIBLE
        }

        setStartDateTimeField()
        setEndDateTimeField()

        dateFormatter = SimpleDateFormat("dd/MM/yyyy", Locale.US) //yyyy-MM-dd

        fromDate?.inputType = InputType.TYPE_NULL
        fromDate?.setOnClickListener(this)

        toDate?.inputType = InputType.TYPE_NULL
        toDate?.setOnClickListener(this)

        btn_search_deposit_logs = this.findViewById<View>(R.id.search) as Button

        btn_search_deposit_logs?.setOnClickListener {

            try {
                try {

                    startDate = fromDate?.text.toString()
                    endDate = toDate?.text.toString()

                   // startDate =  MyUtilsClass.getDateFormatForSerachData(fromDate?.text.toString())
                    //endDate =  MyUtilsClass.getDateFormatForSerachData(toDate?.text.toString())

                    when {
                        fromDate?.text.toString().trim { it <= ' ' }.isEmpty() -> applicationContext?.let {
                            Toasty.error(
                                it,
                                "Select From Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }
                        toDate?.text.toString().trim { it <= ' ' }.isEmpty() -> applicationContext?.let {
                            Toasty.error(
                                it,
                                "Select To Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }

                        fromDate?.text.toString()>toDate?.text.toString() -> applicationContext?.let {

                            Toasty.error(
                                it,
                                "From Date cannot be greater than To Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }

                        requestType == "Transfer" -> {

                           /* if (selectTransType.trim { it <= ' ' }.isEmpty() || selectTransType == "Select Transaction Type") {
                                Toasty.error(
                                    applicationContext,
                                    "Select Transaction Type",
                                    Toast.LENGTH_SHORT,
                                    true
                                ).show()
                            } else {*/
                                val intent = Intent(this, BalanceTransferListActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                                finish()
                          //  }
                        }

                        messageRecharge == "Recharge" -> {
                            val intent = Intent(this, BalanceMessageListActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        }

                        else -> {

                            val intent = Intent(this, BalanceDepositListActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (requestType == "Deposit") {
            toolbarTitle?.text = "Balance Deposit List"
        } else if (requestType == "Withdraw") {
            toolbarTitle?.text = "Balance Withdraw List"
        } else if (requestType == "Transfer") {
            toolbarTitle?.text = "Balance Transfer List"
        } else if (messageRecharge == "Recharge") {
            toolbarTitle?.text = "Message Recharge List"
        }

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceDepositDashboard::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_trans_type) {
                selectTransType = spinner_transactionType?.selectedItem.toString()
                if (selectTransType == resources.getString(R.string.select_transType)) {
                    selectTransType = ""
                } else {
                    selectTransType = parent.getItemAtPosition(position).toString()

                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark Update ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {
        if (v === fromDate) {
            fromDatePickerDialog?.show()
        }

        if (v === toDate) {
            toDatePickerDialog?.show()
        }
    }

    private fun setStartDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                fromDate?.setText(dateFormatter!!.format(newDate.time))
            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
    }

    private fun setEndDateTimeField() {
        val newCalendar = Calendar.getInstance()
        toDatePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                toDate?.setText(dateFormatter!!.format(newDate.time))
            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        toDatePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
    }

    companion object {
        private val TAG = "BalanceDepositDateSelection"
        var startDate = ""
        var endDate = ""
        var selectTransType = ""
    }
}
