package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.PurchaseUsedCodeListAdapter
import com.netizen.netiworld.model.PurchaseCodeLogGetdata
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class UsedPurchaseCodeActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapterUnused: PurchaseUsedCodeListAdapter? = null
    private var unusedCodeArrayList: ArrayList<PurchaseCodeLogGetdata>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var totalFound: TextView? = null

    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        totalFound = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        toolBarInit()
        recyclerViewInit()
        getPurchaseUnusedData()
    }

    /*
   recycler view initialization method
  */
    fun recyclerViewInit() {
        // Initialize item list
        unusedCodeArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapterUnused passing in the sample item data
        adapterUnused = PurchaseUsedCodeListAdapter(applicationContext, unusedCodeArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapterUnused to the recyclerview to populate items
        recyclerView?.adapter = adapterUnused
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbarTitle?.text = "Used Purchase Code List"

        toolbar?.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, PurchaseCodeDashboard::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint = Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.search_product_name) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapterUnused?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapterUnused?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }


    private fun getPurchaseUnusedData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getPurchaseCodeData(header, 1)

        //calling the api
        call?.enqueue(object : Callback<List<PurchaseCodeLogGetdata>> {
            override fun onResponse(
                call: Call<List<PurchaseCodeLogGetdata>>,
                response: Response<List<PurchaseCodeLogGetdata>>
            ) {
                Log.d("onResponse", "Purchase Code List:" + response.body())

                if (response.code() == 302) {
                    Log.d("onResponse", "Unused List:" + response.body())

                    val purchaseCodeUnuse = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(purchaseCodeUnuse!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val c1 = c.getJSONObject("productPurchaseLogDTO")
                        val c11 = c1.getJSONObject("productInfoDTO")
                        val c2 = c11.getJSONObject("productTypeInfoDTO")
                        val c3 = c1.getJSONObject("productRoleAssignDTO")
                        val c4 = c3.getJSONObject("coreUserRoleDTO")

                        val purchaseDate = c1.getString("purchaseDate")
                        val usedDate = c.getString("usedDate")
                        val purchasePoint = c4.getString("coreRoleNote") //Product Purchase Point
                        val categoryName = c2.getString("categoryName") //Product Type
                        val productName = c11.getString("productName")
                        val purchaseCode = c.getString("purchaseCode")

                        // tem model
                        val unusedCodeList = PurchaseCodeLogGetdata()
                        //set the json data in the model-------------
                        unusedCodeList.setPurchaseDate(purchaseDate)
                        unusedCodeList.setCoreRoleNote(purchasePoint)
                        unusedCodeList.setProductType(categoryName)
                        unusedCodeList.setProductName(productName)
                        unusedCodeList.setPurchaseCode(purchaseCode)
                        unusedCodeList.setProductUseDate(usedDate)

                        unusedCodeArrayList?.add(unusedCodeList)

                    }
                }

                 if(unusedCodeArrayList?.isNullOrEmpty()!!){
                    Toasty.error(applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                adapterUnused?.notifyDataSetChanged()
                totalFound?.text = unusedCodeArrayList?.size.toString()
            }

            override fun onFailure(call: Call<List<PurchaseCodeLogGetdata>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(applicationContext, "Purchase Unused Code Get Data Fail", Toast.LENGTH_LONG, true).show()
            }
        })
    }
}
