package com.netizen.netiworld.reports

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.ProductsOfferListAdapter
import com.netizen.netiworld.model.ProductsOfferGetData
import com.netizen.netiworld.model.ProductsOfferPostForList
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ProductsOfferListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: ProductsOfferListAdapter? = null
    private var productOfferArrayList: ArrayList<ProductsOfferGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var totalFound: TextView? = null

    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_list)
        totalFound = findViewById<View>(R.id.total_deposit_balance_logs) as TextView

        toolBarInit()
        recyclerViewInit()
        getproductOfferListData()

    }

    /*
 recycler view initialization method
*/
    fun recyclerViewInit() {
        // Initialize item list
        productOfferArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = ProductsOfferListAdapter(applicationContext, productOfferArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbarTitle?.text = "Offer Product Purchase List"

        toolbar?.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ProductOfferDateSelection::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            applicationContext!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #FFFFFF>" + resources.getString(R.string.err_balance_offer_code) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(resources.getColor(R.color.white))

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName!!))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)

    }

    private fun getproductOfferListData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val productOffer = ProductsOfferPostForList(
            MyUtilsClass.getDateFormatForSerachData(ProductOfferDateSelection.startDatePO),
            MyUtilsClass.getDateFormatForSerachData(ProductOfferDateSelection.endDatePO),
            100,
            0)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getProductOfferData(productOffer, header)

        //calling the api
        call?.enqueue(object : Callback<List<ProductsOfferGetData>> {
            override fun onResponse(
                call: Call<List<ProductsOfferGetData>>,
                response: Response<List<ProductsOfferGetData>>
            ) {
                Log.d("onResponse", "Product offer List:" + response.body())

                if (response.code() == 302) {
                    Log.d("onResponse", "Offer List:" + response.body())

                    val productOfferRes = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(productOfferRes!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val offerUsedDate = c.getString("offerUseDate")
                        val productType = c.getString("productType")
                        val productName = c.getString("productName")
                        val offerCode = c.getString("offerCode")
                        val productQnty = c.getString("productQuantity")
                        val totalAmt = c.getString("totalPrice")
                        val discountAmt = c.getString("totalDiscount")
                        val payableAmt = c.getString("payableAmount")

                        val productPurchaseLogID = c.getString("productPurchaseLogID")
                        val purchasePoint = c.getString("purchasePoint")
                        val genCodeStatus = c.getInt("genCodeStatus")


                        // tem model
                        val productOfferList = ProductsOfferGetData()
                        //set the json data in the model
                        productOfferList.setOfferUseDate(offerUsedDate)
                        productOfferList.setProductType(productType)
                        productOfferList.setProductName(productName)
                        productOfferList.setOfferCode(offerCode)
                        productOfferList.setProductQuantity(productQnty.toLong())
                        productOfferList.setTotalPrice(totalAmt.toDouble())
                        productOfferList.setTotalDiscount(discountAmt.toDouble())
                        productOfferList.setPayableAmount(payableAmt.toDouble())

                        productOfferList.setProductPurchaseLogID(productPurchaseLogID.toInt())
                        productOfferList.setPurchasePoint(purchasePoint)
                        productOfferList.setGenCodeStatus(genCodeStatus)

                        productOfferArrayList?.add(productOfferList)

                    }

                }

                 if(productOfferArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                totalFound?.text = productOfferArrayList?.size.toString()
                adapter?.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<List<ProductsOfferGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Product Offer Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }

}
