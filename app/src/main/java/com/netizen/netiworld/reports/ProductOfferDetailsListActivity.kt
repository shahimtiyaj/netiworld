package com.netizen.netiworld.reports

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.ProductODetailsListAdapter
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.model.ProductGDetailsGetData
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.InvocationTargetException
import java.util.*

class ProductOfferDetailsListActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: ProductODetailsListAdapter? = null
    private var productGDetailsArrayList: ArrayList<ProductGDetailsGetData>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    private var purchase_point_val: TextView? = null
    private var date_val: TextView? = null
    private var productType_val: TextView? = null
    private var offerCode_val: TextView? = null
    private var productName_val: TextView? = null
    private var productQnty_val: TextView? = null

    private var productPurchaseId: String? = null
    private var date: String? = null
    private var purchasePoint: String? = null
    private var productType: String? = null
    private var offerCode: String? = null
    private var productName: String? = null
    private var productQnty: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_offer_details_recyler_list)

        toolBarInit()

        recyclerViewInit()

        purchase_point_val = findViewById<View>(R.id.purchase_point_val) as TextView
        date_val = findViewById<View>(R.id.date_val) as TextView
        productType_val = findViewById<View>(R.id.product_type_val) as TextView
        offerCode_val = findViewById<View>(R.id.offer_code_val) as TextView
        productName_val = findViewById<View>(R.id.product_name_val) as TextView
        productQnty_val = findViewById<View>(R.id.product_quantity_val) as TextView

        try {

            if (savedInstanceState == null) {
                val arguments = intent.extras

                if (arguments != null) {
                    productPurchaseId = arguments.getString("productPurchaseLogID")
                    getProductGDetailListData()

                    purchasePoint = arguments.getString("purchasePoint")
                    date = arguments.getString("date")
                    productType = arguments.getString("productType")
                    offerCode = arguments.getString("offerCode")
                    productName = arguments.getString("productName")
                    productQnty = arguments.getString("productQnty")

                    purchase_point_val?.text = purchasePoint
                    date_val?.text = date?.toLong()?.let { AppController.getDate(it) }
                    productType_val?.text = productType
                    offerCode_val?.text = offerCode
                    productName_val?.text = productName
                    productQnty_val?.text = productQnty

                }
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }

    /*
   recycler view initialization method
*/
    fun recyclerViewInit() {
        // Initialize item list
        productGDetailsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById<View>(R.id.logs_product_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = ProductODetailsListAdapter(applicationContext, productGDetailsArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(applicationContext)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbarTitle?.text = "Purchase Code List"

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ProductsOfferListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    private fun getProductGDetailListData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getProductODetailsData(header, productPurchaseId?.toIntOrNull())

        Log.d("ID", "Product Purchase ID:" + productPurchaseId)


        //calling the api
        call?.enqueue(object : Callback<List<ProductGDetailsGetData>> {
            override fun onResponse(
                call: Call<List<ProductGDetailsGetData>>,
                response: Response<List<ProductGDetailsGetData>>
            ) {
                Log.d("onResponse", "Product Details List:" + response.body())

                if (response.code() == 302) {
                    Log.d("onResponse", "Details List:" + response.body())

                    val productGeneral = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(productGeneral!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val purchaseCode = c.getString("purchaseCode")
                        val usedStatus = c.getString("usedStatus")
                        val usedDate = c.getString("usedDate")


                        // Get the item model
                        val productGDetailsList = ProductGDetailsGetData()
                        //set the json data in the model
                        productGDetailsList.setPurchaseCode(purchaseCode)
                        productGDetailsList.setUsedStatus(usedStatus.toInt())
                        productGDetailsList.setUsedDate(usedDate)

                        productGDetailsArrayList?.add(productGDetailsList)

                    }

                }

                 if(productGDetailsArrayList?.isNullOrEmpty()!!){
                    Toasty.error(
                        applicationContext,
                        "Sorry ! No Data Found.",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                adapter?.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<List<ProductGDetailsGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Balance Transfer Get Data Fail",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }
}
