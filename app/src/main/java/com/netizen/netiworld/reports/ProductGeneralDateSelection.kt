package com.netizen.netiworld.reports

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class ProductGeneralDateSelection : AppCompatActivity(), View.OnClickListener {

    private var fromDatePickerDialog: DatePickerDialog? = null
    private var toDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    private var btn_search_deposit_logs: Button? = null
    private var fromDate: EditText? = null
    private var toDate: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logs_wallet_deposit_selection)

        toolBarInit()
        viewInit()
    }

    /**
     * SetUp View method
     */
    fun viewInit() {
        fromDate = findViewById<View>(R.id.start_date) as EditText
        toDate = findViewById<View>(R.id.end_date) as EditText


        setStartDateTimeField()
        setEndDateTimeField()

        dateFormatter = SimpleDateFormat("dd/MM/yyyy", Locale.US) //yyyy-MM-dd

        fromDate?.inputType = InputType.TYPE_NULL
        fromDate?.setOnClickListener(this)

        toDate?.inputType = InputType.TYPE_NULL
        toDate?.setOnClickListener(this)

        btn_search_deposit_logs = this.findViewById<View>(R.id.search) as Button

        btn_search_deposit_logs?.setOnClickListener {

            try {
                try {
                    startDatePG = fromDate?.text.toString()
                    endDatePG = toDate?.text.toString()

                    when {
                        fromDate?.text.toString().trim { it <= ' ' }.isEmpty() -> applicationContext?.let {
                            Toasty.error(
                                it,
                                "Select From Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }
                        toDate?.text.toString().trim { it <= ' ' }.isEmpty() -> applicationContext?.let {
                            Toasty.error(
                                it,
                                "Select To Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }

                        fromDate?.text.toString()>toDate?.text.toString() -> applicationContext?.let {

                            Toasty.error(
                                it,
                                "From Date cannot be greater than To Date",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }

                        else -> {

                            val intent = Intent(this, ProductGeneralListActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "General Product Purchase"

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }


    override fun onClick(v: View) {
        if (v === fromDate) {
            fromDatePickerDialog?.show()
        }

        if (v === toDate) {
            toDatePickerDialog?.show()
        }
    }

    private fun setStartDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                fromDate?.setText(dateFormatter!!.format(newDate.time))
            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
    }

    private fun setEndDateTimeField() {
        val newCalendar = Calendar.getInstance()
        toDatePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                toDate?.setText(dateFormatter!!.format(newDate.time))
            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        toDatePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
    }

    companion object {
        private val TAG = "ProductGeneralDateSelection"
        var startDatePG = ""
        var endDatePG = ""
    }
}


