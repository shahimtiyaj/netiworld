package com.netizen.netiworld.rest

import com.netizen.netiworld.model.*
import retrofit2.Call
import retrofit2.http.*
import java.util.ArrayList


interface ApiInterface {

    @FormUrlEncoded
    @POST("oauth/token")
    fun postData(@FieldMap params: HashMap<String?, String?>, @HeaderMap headers: HashMap<String?, String?>): Call<PostResponse>

    @GET("user/profile")
    fun getUserProfileInfo(@HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/account/by/corebankid")
    fun getAccountNoInfo(@HeaderMap headers: HashMap<String?, String?>, @Query("coreBankID") localBankCategoryID: String): Call<MobileBankAcNo>

    @GET("user/category/by/type/2nd_parent_type?typeDefaultCode=T100")
    fun getAccountNameInfo(@HeaderMap headers: HashMap<String?, String?>): Call<MobileBank>

    @GET("user/message/types/by/point")
    fun getMessageType(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String): Call<MessageType>

    @GET("user/roles/assigned")
    fun getPurchasePoint(@HeaderMap headers: HashMap<String?, String?>): Call<PurchasePoint>

    @GET("user/products/by/role")
    fun getProductName(@HeaderMap headers: HashMap<String?, String?>,  @Query("roleID") code: String): Call<Products>

    @POST("user/balance/deposit")
    fun depositpostData(@Body obj: DepositMobile, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @POST("user/message/recharge")
    fun messageRechargepostData(@Body obj: MessageRecharge, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @POST("user/balance/withdraw")
    fun balanceWithdrawData(@Body obj: UserWithdraw, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @POST("user/product/purchase")
    fun generalProductPostData(@Body obj: GeneralProductPost, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/product/offer/by/code")
    fun getOfferProduct(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<OfferProduct>

    @POST("user/product/purchase/offer")
    fun offerProductPostData(@Body obj: OfferProductPost, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/profile/by/custom_id")
    fun getDataSearchByNetiID(@HeaderMap headers: HashMap<String?, String?>, @Query("custom_id") code: String): Call<String>

    @POST("user/transfer/check/balance")
    fun WalletBalanceTransfer(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @GET("guest/core/check-otp")
    fun OTPVarify(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<String>

    @POST("user/balance/transfer")
    fun WalletBalanceTransferFinal(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @POST("user/balance/requests/by/date_range")
    fun balanceDepositGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceDepositGetData>>

    /*@POST("user/get-transfer-records")
    fun balanceTransferGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceTransferGetData>>
    */
    @POST("user/balance/transfer/by/date")
    fun balanceTransferGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceTransferGetData>>

    @POST("user/message/by/date-range")
    fun messageRechargeGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceMessageGetData>>

    @POST("user/balance/statement")
    fun balanceStatementGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceStatementGetData>>

    @GET("user/product/purchases/by/date-range")
    fun getProductGeneralData(@HeaderMap headers: HashMap<String?, String?>, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<List<ProductsGeneralGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductGDetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @POST("user/product/offer/by/date-range")
    fun getProductOfferData(@Body obj: ProductsOfferPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<ProductsOfferGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductODetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @GET("user/purchase/codes/by/usedstatus")
    fun getPurchaseCodeData(@HeaderMap headers: HashMap<String?, String?>, @Query("usedStatus") usedStatus: Int?): Call<List<PurchaseCodeLogGetdata>>

    @GET("guest/file/find")
    fun getProfile(@HeaderMap headers: HashMap<String?, String?>, @Query("filePath") filePath: String): Call<ProfileImage>


    //------------------------------------------
    @Headers(
        "Authorization: Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ=",
        "Content-Type:application/x-www-form-urlencoded",
        "NZUSER:absiddik:123:password"
    )
    @FormUrlEncoded
    @POST("oauth/token")
    fun userLogIn(@Field("name") name: String, @Field("password") password: String, @Field("grant_type") grant_type: String): Call<MSG>

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") id: Int, @Query("api_key") apiKey: String): Call<UserLogin>

    fun getTopRatedMovies(@Query("api_key") apiKey: String): Call<String>

}

