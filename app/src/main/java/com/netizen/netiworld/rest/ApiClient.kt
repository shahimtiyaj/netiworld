package com.netizen.netiworld.rest

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    //val BASE_URL = "http://192.168.0.122:9000/"
    val BASE_URL = "https://api.netizendev.com:2087/"
     //val BASE_URL = "https://api.netizendev.com:2053/"

    private var retrofit: Retrofit? = null

    val getClient: Retrofit?

        get() {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            /*val builder = OkHttpClient.Builder()
            builder.followRedirects(false)
            val client = builder.build()*/

            val gson = GsonBuilder()
                .setLenient()
                .create()

          val client = OkHttpClient.Builder()
                .followRedirects(false)
                .followSslRedirects(false)
                .connectTimeout(15, TimeUnit.SECONDS)
               .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build()

            if (retrofit == null) {
                retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build()
            }

            return retrofit
        }
}