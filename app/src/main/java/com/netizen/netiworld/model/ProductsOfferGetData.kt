package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class ProductsOfferGetData {

    @SerializedName("offerUseDate")
    private var offerUseDate: String? = null

    @SerializedName("productType")
    private var productType: String? = null

    @SerializedName("productName") //Purchase Point
    private var productName: String? = null

    @SerializedName("offerCode") //Product Type
    private var offerCode: String? = null

    @SerializedName("productQuantity") //Product Quantity
    private var productQuantity: Long? = null

    @SerializedName("totalPrice") //Payable Amount
    private var totalPrice: Double? = null

    @SerializedName("totalDiscount") //Payable Amount
    private var totalDiscount: Double? = null

    @SerializedName("payableAmount") //Info view enable or disable
    private var payableAmount: Double? = null


    @SerializedName("productPurchaseLogID") //Payable Amount
    private var productPurchaseLogID: Int? = null

    @SerializedName("genCodeStatus") //Info view enable or disable
    private var genCodeStatus: Int? = null

    @SerializedName("purchasePoint") //Info view enable or disable
    private var purchasePoint: String? = null


    constructor() {

    }

    constructor(
        offerUseDate: String?,
        productType: String?,
        productName: String?,
        offerCode: String?,
        productQuantity: Long?,
        totalPrice: Double?,
        totalDiscount: Double?,
        payableAmount: Double?
    ) {
        this.offerUseDate = offerUseDate
        this.productType = productType
        this.productName = productName
        this.offerCode = offerCode
        this.productQuantity = productQuantity
        this.totalPrice = totalPrice
        this.totalDiscount = totalDiscount
        this.payableAmount = payableAmount
    }


    fun getOfferUseDate(): String? {
        return offerUseDate
    }

    fun setOfferUseDate(offerUseDate: String) {
        this.offerUseDate = offerUseDate
    }

    fun getProductType(): String? {
        return productType
    }

    fun setProductType(productType: String) {
        this.productType = productType
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String) {
        this.productName = productName
    }

    fun getOfferCode(): String? {
        return offerCode
    }

    fun setOfferCode(offerCode: String) {
        this.offerCode = offerCode
    }

    fun getProductQuantity(): Long? {
        return productQuantity
    }

    fun setProductQuantity(productQuantity: Long) {
        this.productQuantity = productQuantity
    }


    fun getTotalPrice(): Double? {
        return totalPrice
    }

    fun setTotalPrice(totalPrice: Double) {
        this.totalPrice = totalPrice
    }

    fun getTotalDiscount(): Double? {
        return totalDiscount
    }

    fun setTotalDiscount(totalDiscount: Double) {
        this.totalDiscount = totalDiscount
    }


    fun getPayableAmount(): Double? {
        return payableAmount
    }

    fun setPayableAmount(payableAmount: Double) {
        this.payableAmount = payableAmount
    }

    //--------------------------------------------

    fun getProductPurchaseLogID(): Int? {
        return productPurchaseLogID
    }

    fun setProductPurchaseLogID(productPurchaseLogID: Int) {
        this.productPurchaseLogID = productPurchaseLogID
    }

    fun getGenCodeStatus(): Int? {
        return genCodeStatus
    }

    fun setGenCodeStatus(genCodeStatus: Int) {
        this.genCodeStatus = genCodeStatus
    }

    fun getPurchasePoint(): String? {
        return purchasePoint
    }

    fun setPurchasePoint(purchasePoint: String) {
        this.purchasePoint = purchasePoint
    }
}

