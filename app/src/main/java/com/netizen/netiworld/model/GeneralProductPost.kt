package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName


class GeneralProductPost {

    @SerializedName("purchaseQuantity")
    var purchaseQuantity: Int? = 0

    @SerializedName("unitPrice")
    var unitPrice: Double? = 0.0

    @SerializedName("totalAmount")
    var totalAmount: Double? = 0.0

    @SerializedName("vatAmount")
    var vatAmount: Double? = 0.0

    @SerializedName("paidAmount")
    var paidAmount: Double? = 0.0

    @SerializedName("productInfoDTO")
    var productInfoDTO: _ProductInfoDTO? = null

    @SerializedName("productRoleAssignDTO")
    var productRoleAssignDTO: _ProductRoleAssignDTO? = null

    constructor() {
    }

    constructor(
        purchaseQuantity: Int?,
        unitPrice: Double?,
        totalAmount: Double?,
        vatAmount: Double?,
        paidAmount: Double?,
        productInfoDTO: _ProductInfoDTO?,
        productRoleAssignDTO: _ProductRoleAssignDTO?
    ) {
        this.purchaseQuantity = purchaseQuantity
        this.unitPrice = unitPrice
        this.totalAmount = totalAmount
        this.vatAmount = vatAmount
        this.paidAmount = paidAmount
        this.productInfoDTO = productInfoDTO
        this.productRoleAssignDTO = productRoleAssignDTO
    }

}


class _ProductInfoDTO {
    @SerializedName("productID")
    var productID: Int? = 0

    constructor(productID: Int?) {
        this.productID = productID
    }
}

class _ProductRoleAssignDTO {
    @SerializedName("productRoleAssignID")
    var productRoleAssignID: Int? = 0

    constructor(productRoleAssignID: Int?) {
        this.productRoleAssignID = productRoleAssignID
    }
}

