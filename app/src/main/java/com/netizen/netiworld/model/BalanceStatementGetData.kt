package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class BalanceStatementGetData {

    @SerializedName("transactionDate")
    var transactionDate: String? = null

    @SerializedName("transactionFor")
     var transactionFor: String? = null

    @SerializedName("income")
    var income: Double? = null

    @SerializedName("expense")
    var expense: Double? = null

    constructor(

    ) {

    }


    constructor(
        transactionDate: String?,
        transactionFor: String?,
        income: Double?,
        expense: Double?
    ) {
        this.transactionDate = transactionDate
        this.transactionFor = transactionFor
        this.income = income
        this.expense = expense
    }

    fun getTransType(): String?{
        return transactionFor
    }

    fun setTransType(transactionFor: String) {
        this.transactionFor = transactionFor
    }


}

