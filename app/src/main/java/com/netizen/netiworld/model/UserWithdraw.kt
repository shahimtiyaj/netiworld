package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName

class UserWithdraw {

    @SerializedName("requestedAmount")
    var requestedAmount: Double? = 0.0

    @SerializedName("requestNote")
    var requestNote: String? = ""

    @SerializedName("userBankAccountInfoDTO")
    var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

    constructor() {
    }

    constructor(
        requestedAmount: Double?,
        requestNote: String?,
        userBankAccountInfoDTO: UserBankAccountInfoDTO?
    ) {
        this.requestedAmount = requestedAmount
        this.requestNote = requestNote
        this.userBankAccountInfoDTO = userBankAccountInfoDTO
    }
}

class UserBankAccountInfoDTO {
    @SerializedName("userBankAccId")
    var userBankAccId: Long? = 0

    constructor(userBankAccId: Long?) {
        this.userBankAccId = userBankAccId
    }
}