package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName


class ProductsOfferPostForList {

    @SerializedName("startDate")
    var startDate: String? = null

    @SerializedName("endDate")
    var endDate: String? = null

    @SerializedName("pageLimit")
    var pageLimit: Int? = null

    @SerializedName("pageNo")
    var pageNo: Int? = null

    constructor() {

    }

    constructor(startDate: String?, endDate: String?, pageLimit: Int?, pageNo: Int?) {
        this.startDate = startDate
        this.endDate = endDate
        this.pageLimit = pageLimit
        this.pageNo = pageNo
    }
}

