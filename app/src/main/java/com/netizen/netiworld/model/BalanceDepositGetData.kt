package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class BalanceDepositGetData {

    @SerializedName("requestDate")
    var requestDate: String? = null

    @SerializedName("bank")
    var bank: String? = null

    @SerializedName("fromWhere")
    var fromWhere: String? = null

    @SerializedName("accountNumber")
    var accountNumber: String? = null

    @SerializedName("requestedAmount")
    var requestedAmount: Double? = null

    @SerializedName("transactionDate")
    var transactionDate: String? = null

    @SerializedName("transactionNumber")
    var transactionNumber: String? = null

    @SerializedName("approveDate")
    var approveDate: String? = null

    @SerializedName("requestStatus")
    var requestStatus: String? = null

    private var balanceDeposit: List<BalanceDepositGetData>? = null
    fun getBalanceDepositList(): List<BalanceDepositGetData>? {
        return balanceDeposit
    }

    constructor() {
    }

    constructor(
        requestDate: String?,
        bank: String?,
        accountNumber: String?,
        requestedAmount: Double?,
        transactionDate: String?,
        transactionNumber: String?,
        approveDate: String?,
        requestStatus: String?
    ) {
        this.requestDate = requestDate
        this.bank = bank
        this.accountNumber = accountNumber
        this.requestedAmount = requestedAmount
        this.transactionDate = transactionDate
        this.transactionNumber = transactionNumber
        this.approveDate = approveDate
        this.requestStatus = requestStatus
    }


}

