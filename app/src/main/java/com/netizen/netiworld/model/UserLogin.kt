package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName


class UserLogin(
    @field:SerializedName("name")
    var name: String?,
    @field:SerializedName("password")
    var password: String?
)