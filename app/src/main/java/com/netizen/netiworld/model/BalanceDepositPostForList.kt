package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName


class BalanceDepositPostForList {

    @SerializedName("requestStartDate")
    var requestStartDate: String? = null

    @SerializedName("requestEndDate")
    var requestEndDate: String? = null

    @SerializedName("requestType")
    var requestType: String? = null

    @SerializedName("transactionType")
    var transactionType: String? = null

    @SerializedName("limit")
    var limit: Int? = null

    @SerializedName("pageNo")
    var pageNo: Int? = null

    constructor() {
    }

    constructor(
        requestStartDate: String?,
        requestEndDate: String?,
        requestType: String?,
        transactionType: String?,
        limit: Int?,
        pageNo: Int?
    ) {
        this.requestStartDate = requestStartDate
        this.requestEndDate = requestEndDate
        this.requestType = requestType
        this.transactionType = transactionType
        this.limit = limit
        this.pageNo = pageNo
    }

    constructor(
        requestStartDate: String?,
        requestEndDate: String?,
        limit: Int?
    ) {
        this.requestStartDate = requestStartDate
        this.requestEndDate = requestEndDate
        this.limit = limit
    }
}

