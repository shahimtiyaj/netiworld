package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName


class Information {
    
    @SerializedName("netiID")
    private var netiID: Int? = null

    @SerializedName("customNetiID")
    private var customNetiID: Int? = null

    @SerializedName("fullName")
    var fullName: String? = null

    @SerializedName("gender")
    private var gender: String? = null

    @SerializedName("religion")
    private var religion: String? = null

    @SerializedName("dateOfBirth")
    private var dateOfBirth: String? = null

    @SerializedName("basicMobile")
    private var basicMobile: String? = null

    @SerializedName("basicEmail")
    private var basicEmail: String? = null

    @SerializedName("imagePath")
    private var imagePath: String? = null

    @SerializedName("imageName")
    private var imageName: String? = null

    @SerializedName("userWalletBalance")
    private var userWalletBalance: Float? = null

    @SerializedName("userReserveBalance")
    private var userReserveBalance: Float? = null

    @SerializedName("smsBalance")
    private var smsBalance: Float? = null

    @SerializedName("voiceBalance")
    private var voiceBalance: Float? = null

    @SerializedName("emailBalance")
    private var emailBalance: Float? = null

    @SerializedName("validationStatus")
    private var validationStatus: Int? = null

    @SerializedName("userEnableStatus")
    private var userEnableStatus: Int? = null

    @SerializedName("registrationDate")
    private var registrationDate: Int? = null

    @SerializedName("userName")
    private var userName: Any? = null

    @SerializedName("userPassword")
    private var userPassword: Any? = null

    @SerializedName("imageContent")
    private var imageContent: Any? = null

    @SerializedName("imageSaveOrEditable")
    private var imageSaveOrEditable: Boolean? = null

    @SerializedName("lastUserExecuted")
    private var lastUserExecuted: String? = null

    @SerializedName("lastIpExecuted")
    private var lastIpExecuted: String? = null

    @SerializedName("lastDateExecuted")
    private var lastDateExecuted: Int? = null

    @SerializedName("globalAreaInfoDTO")
    private var globalAreaInfoDTO: GlobalAreaInfoDTO? = null


    internal inner class GlobalAreaInfoDTO {
        @SerializedName("coreCategoryID")
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        private var categoryName: String? = null

        @SerializedName("categoryNote")
        private var categoryNote: Any? = null

        @SerializedName("categoryEnableStatus")
        private var categoryEnableStatus: Int? = null

        @SerializedName("categorySerial")
        private var categorySerial: Int? = null

        @SerializedName("typeStatus")
        private var typeStatus: Int? = null

        @SerializedName("parentStatus")
        private var parentStatus: Int? = null

        @SerializedName("parentTypeInfoDTO")
        private var parentTypeInfoDTO: Any? = null

        @SerializedName("parentCoreCategoryInfoDTO")
        private var parentCoreCategoryInfoDTO: Any? = null

        @SerializedName("lastUserExecuted")
        private var lastUserExecuted: String? = null

        @SerializedName("lastIpExecuted")
        private var lastIpExecuted: String? = null

        @SerializedName("lastDateExecuted")
        private var lastDateExecuted: Int? = null
    }

}
