package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class PurchaseCodeLogGetdata {

    @SerializedName("purchaseDate")
    private var purchaseDate: String? = null

    @SerializedName("coreRoleNote") // purchase point
    private var coreRoleNote: String? = null

    @SerializedName("categoryName") //Purchase type
    private var categoryName: String? = null

    @SerializedName("productName") //product name
    private var productName: String? = null

    @SerializedName("purchaseCode") //Product Quantity
    private var purchaseCode: String? = null

    @SerializedName("usedDate") // Product Used Date
    private var usedDate: String? = null


    constructor() {

    }

    fun getPurchaseDate(): String? {
        return purchaseDate
    }

    fun setPurchaseDate(purchaseDate: String) {
        this.purchaseDate = purchaseDate
    }

    fun getCoreRoleNote(): String? {
        return coreRoleNote
    }

    fun setCoreRoleNote(coreRoleNote: String) {
        this.coreRoleNote = coreRoleNote
    }

    fun getProductType(): String? {
        return categoryName
    }

    fun setProductType(categoryName: String) {
        this.categoryName = categoryName
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String) {
        this.productName = productName
    }

    fun getPurchaseCode(): String? {
        return purchaseCode
    }

    fun setPurchaseCode(purchaseCode: String) {
        this.purchaseCode = purchaseCode
    }

    fun getProductUseDate(): String? {
        return usedDate
    }

    fun setProductUseDate(usedDate: String) {
        this.usedDate = usedDate
    }

}

