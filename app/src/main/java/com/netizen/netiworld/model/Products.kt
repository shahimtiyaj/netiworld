package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class Products {

    @SerializedName("productID")
    @Expose
    private var productID: Int? = null

    @SerializedName("productName")
    @Expose
    private var productName: String? = null

    @SerializedName("salesPrice")
    @Expose
    private var salesPrice: Double? = null

    @SerializedName("percentVat")
    @Expose
    private var percentVat: Double? = null

    constructor() {
    }

    constructor(productName: String?) {
        this.productName = productName
    }

    constructor(productID: Int?, productName: String?, salesPrice: Double?, percentVat: Double?) {
        this.productID = productID
        this.productName = productName
        this.salesPrice = salesPrice
        this.percentVat = percentVat
    }

    fun getProductID(): Int? {
        return productID
    }

    fun setProductID(productID: Int?) {
        this.productID = productID
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String) {
        this.productName = productName
    }

    fun getSalesPrice(): Double? {
        return salesPrice
    }

    fun setSalesPrice(salesPrice: Double?) {
        this.salesPrice = salesPrice
    }

    fun getPercentVat(): Double? {
        return percentVat
    }

    fun setPercentVat(percentVat: Double?) {
        this.percentVat = percentVat
    }

}
