package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class MobileBank {

    @SerializedName("coreCategoryID")
    @Expose
    private var coreCategoryID: Int? = null

    @SerializedName("categoryName")
    @Expose
    private var categoryName: String? = null

    constructor() {

    }

    constructor(categoryName : String) {
        this.categoryName = categoryName
    }

    fun getCoreCategoryID(): Int? {
        return coreCategoryID
    }

    fun setCoreCategoryID(coreCategoryID: Int?) {
        this.coreCategoryID = coreCategoryID
    }

    fun getCategoryName(): String?{
        return categoryName
    }

    fun setCategoryName(categoryName: String) {
        this.categoryName = categoryName
    }

}