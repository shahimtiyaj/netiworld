package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class ProductGDetailsGetData {

    @SerializedName("purchaseCode")
    private var purchaseCode: String? = null

    @SerializedName("usedStatus")
    private var usedStatus: Int? = null

    @SerializedName("usedDate")
    private var usedDate: String? = null


    constructor() {

    }

    constructor(purchaseCode: String?, usedStatus: Int?, usedDate: String?) {
        this.purchaseCode = purchaseCode
        this.usedStatus = usedStatus
        this.usedDate = usedDate
    }


    fun getPurchaseCode(): String? {
        return purchaseCode
    }

    fun setPurchaseCode(purchaseCode: String) {
        this.purchaseCode = purchaseCode
    }

    fun getUsedStatus(): Int? {
        return usedStatus
    }

    fun setUsedStatus(usedStatus: Int) {
        this.usedStatus = usedStatus
    }

    fun getUsedDate(): String? {
        return usedDate
    }

    fun setUsedDate(usedDate: String) {
        this.usedDate = usedDate
    }

}

