package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WalletTransfer {

    @SerializedName("requestedAmmount")
    @Expose
    private var requestedAmmount: String? = null

    @SerializedName("requestNote")
    @Expose
    private var requestNote: String? = null

    @SerializedName("receiver")
    var receiver: Receiver? = null


    constructor() {
    }

    constructor(requestedAmmount: String?, receiver: Receiver?, requestNote: String?) {
        this.requestedAmmount = requestedAmmount
        this.receiver = receiver
        this.requestNote = requestNote
    }

    class Receiver {
        @SerializedName("netiID")
        var netiID: Long ? = 0

        constructor(netiID: Long ?) {
            this.netiID = netiID
        }
    }
}



