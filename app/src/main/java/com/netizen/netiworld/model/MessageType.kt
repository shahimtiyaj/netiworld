package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class MessageType {

    @SerializedName("productID")
    @Expose
    private var productID: Long? = null

    @SerializedName("productName")
    @Expose
    private var productName: String? = null

    @SerializedName("salesPrice")
    @Expose
    private var salesPrice: Double? = null

    @SerializedName("percentVat")
    @Expose
    private var percentVat: Double? = null



    private var messageTypeList: List<MessageType>? = null

    fun getMessageTypelist(): List<MessageType>? {
        return messageTypeList
    }

    fun setMessageTypest(messageTypeList: List<MessageType>) {
        this.messageTypeList = messageTypeList
    }

    constructor() {

    }

    constructor(productID: Long?, productName: String?) {
        this.productID = productID
        this.productName = productName
    }

    constructor(productName: String?) {
        this.productName = productName
    }

    fun getProductID(): Long? {
        return productID
    }

    fun setProductID(productID: Long?) {
        this.productID = productID
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String?) {
        this.productName = productName
    }

    fun getSalesPrice(): Double? {
        return salesPrice
    }

    fun setSalesPrice(salesPrice: Double?) {
        this.salesPrice = salesPrice
    }

    fun getPercentVat(): Double? {
        return percentVat
    }

    fun setPercentVat(percentVat: Double?) {
        this.percentVat = percentVat
    }

}