package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName

class DepositMobile {

    @SerializedName("transactionDate")
    var transactionDate: String? = null

    @SerializedName("requestedAmount")
    var requestedAmount: String? = null

    @SerializedName("transactionNumber")
    var transactionNumber: String? = null

    @SerializedName("fromWhere")
    var fromWhere: String? = null

    @SerializedName("requestNote")
    var requestNote: String? = null

    @SerializedName("attachFileName")
    var attachFileName: String? = null

    @SerializedName("attachFileContent")
    var attachFileContent: String? = null

    @SerializedName("attachFileSaveOrEditable")
    var attachFileSaveOrEditable: Boolean? = null

    @SerializedName("coreBankAccountInfoDTO")
    var coreBankAccountInfoDTO: CoreBankAccountInfoDTO? = null



    constructor() {
    }

    constructor(
        transactionDate: String?,
        requestedAmount: String?,
        transactionNumber: String?,
        fromWhere: String?,
        coreBankAccountInfoDTO: CoreBankAccountInfoDTO?
    ) {
        this.transactionDate = transactionDate
        this.requestedAmount = requestedAmount
        this.transactionNumber = transactionNumber
        this.fromWhere = fromWhere
        this.coreBankAccountInfoDTO = coreBankAccountInfoDTO
    }

    constructor(
        transactionDate: String?,
        requestedAmount: String?,
        transactionNumber: String?,
        fromWhere: String?,
        requestNote: String?,
        attachFileName: String?,
        attachFileContent: String?,
        attachFileSaveOrEditable: Boolean?,
        coreBankAccountInfoDTO: CoreBankAccountInfoDTO?
    ) {
        this.transactionDate = transactionDate
        this.requestedAmount = requestedAmount
        this.transactionNumber = transactionNumber
        this.fromWhere = fromWhere
        this.requestNote = requestNote
        this.attachFileName = attachFileName
        this.attachFileContent = attachFileContent
        this.attachFileSaveOrEditable = attachFileSaveOrEditable
        this.coreBankAccountInfoDTO = coreBankAccountInfoDTO
    }
}


class CoreBankAccountInfoDTO {
    @SerializedName("coreBankAccId")
    var coreBankAccId: Int? = 0

    constructor(coreBankAccId: Int?) {
        this.coreBankAccId = coreBankAccId
    }

}