package com.netizen.netiworld.model

import android.graphics.Movie


class MobileBankList {
    private var moblieBanks: List<MobileBank>? = null

    fun getBankLIst(): List<MobileBank>? {
        return moblieBanks
    }

    fun setBankList(moblieBanks: List<MobileBank>) {
        this.moblieBanks = moblieBanks
    }
}