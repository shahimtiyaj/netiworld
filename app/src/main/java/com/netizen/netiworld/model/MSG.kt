package com.netizen.netiworld.model

import android.R.id.message



class MSG {

    private var success: Int? = null
    private var message: String? = null

    constructor() {}

    constructor(success: Int?, message: String) : super() {
        this.success = success
        this.message = message
    }

    fun getSuccess(): Int? {
        return success
    }

    fun setSuccess(success: Int?) {
        this.success = success
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}