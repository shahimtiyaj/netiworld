package com.netizen.netiworld

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile01
import com.netizen.netiworld.activity.balanceMessage.BalanceMessageRecharge
import com.netizen.netiworld.activity.balanceTransfer.BalanceWalletTransfer01
import com.netizen.netiworld.activity.balanceWithdraw.BalanceWalletWithdraw01
import com.netizen.netiworld.activity.purchaseGeneralProduct.GeneralProduct01
import com.netizen.netiworld.activity.purchaseOfferProduct.PurchaseOfferProduct
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.fragment.FragmentDashboard
import com.netizen.netiworld.reports.*
import com.waspar.falert.Falert

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    internal var close: ImageView? = null
    private var mDrawerLayout: DrawerLayout? = null
    private var falert: Falert? = null
    internal var ok: TextView? = null
    internal var no: TextView? = null

    private var myDialog: Dialog? = null
    var view: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dashboardFragment = FragmentDashboard()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
        fragmentTransaction.commit()

        myDialog = Dialog(this)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val menuLeft = findViewById<ImageButton>(R.id.menuLeft)
        val menuRight = findViewById<ImageButton>(R.id.menuRight)

        menuLeft.setOnClickListener {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                drawer.openDrawer(GravityCompat.START)
            }
        }

        menuRight.setOnClickListener {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END)
            } else {
                drawer.openDrawer(GravityCompat.END)
            }
        }

        val navigationView1 = findViewById<NavigationView>(R.id.nav_view)
        val navigationView2 = findViewById<NavigationView>(R.id.nav_view2)
        navigationView1.setNavigationItemSelectedListener(this)
        navigationView2.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else if (!drawer.isDrawerOpen(GravityCompat.START)) {
            val sharedPreferences = getSharedPreferences("back", Context.MODE_PRIVATE)

            when (sharedPreferences.getString("now", "")) {
                //"std_reg" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container, FragmentStudentDashboard()).commit()
                "home" -> LogOut()
            }

        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }

    fun LogOut() {

        val logout: Button
        val cencel: Button
        val txtclose: TextView

        myDialog?.setContentView(R.layout.logout_pop_up_final)
        logout = myDialog?.findViewById(R.id.logout) as Button
        cencel = myDialog?.findViewById(R.id.cencel) as Button
        // myDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        myDialog?.setCancelable(false)

        logout.setOnClickListener {
            try {
                val da = this.let { DAO(it) }
                da.open()
                da.deleteProfileList()
                da.deleteAccountNoList()
                da.deleteBankNameList()
                da.deleteUserRoleAssignList()
                da.close()

                val intent = Intent(this, SignInActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.right_in, R.anim.right_out)
                finish()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        cencel.setOnClickListener { myDialog?.dismiss() }

        txtclose = myDialog?.findViewById(R.id.txtclose)!!
        txtclose.setOnClickListener { myDialog?.dismiss() }
        myDialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        when (item.itemId) {
           R.id.nav_dashboard -> {
                val dashboardFragment = FragmentDashboard()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
                fragmentTransaction.commit()
            }
            R.id.nav_profile -> {
                val intent = Intent(this, ProfileActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()

                /*val dashboardFragment = FragmentDashboard()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
                fragmentTransaction.commit()*/
            }
            R.id.nav_balance -> {
                val intent = Intent(this, BalanceWalletDepositMobile01::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }
            R.id.nav_balance_withdraw -> {
                val intent = Intent(this, BalanceWalletWithdraw01::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

            R.id.nav_balance_transfer -> {

                val intent = Intent(this, BalanceWalletTransfer01::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }
            R.id.nav_message -> {
                val intent = Intent(this, BalanceMessageRecharge::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }
            R.id.nav_general_product -> {
                val intent = Intent(this, GeneralProduct01::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }
            R.id.nav_offer_product -> {

                val intent = Intent(this, PurchaseOfferProduct::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

            R.id.nav_log_out -> {

                LogOut()
            }

            //Reports parts of left Navigation
            R.id.nav_wallet_log -> {
                val intent = Intent(this, BalanceDepositDashboard::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

            R.id.nav_message_log -> {

                val intent = Intent(this, BalanceMessageDateSelection::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

            R.id.nav_balance_statement -> {
                val intent = Intent(this, BalanceStatementDateSelection::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()

            }

            R.id.nav_general_product_log -> {
                val intent = Intent(this, ProductGeneralDateSelection::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

            R.id.nav_offer_product_log -> {
               // Toast.makeText(this, "Offer Product Log", Toast.LENGTH_LONG).show()
                val intent = Intent(this, ProductOfferDateSelection::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()

            }

            R.id.nav_purchase_code -> {
               /* val fragmentSemesterExam = TabFragmentPurchaseCode()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, fragmentSemesterExam)
                fragmentTransaction.commit()*/

                val intent = Intent(this, PurchaseCodeDashboard::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
            }

    }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        drawer.closeDrawer(GravityCompat.END)
        return true
    }

    companion object {
        private val TAG = "BalanceDepositDateSelection"
        var messageRecharge = ""
        var balanceStatement = ""
    }
}
