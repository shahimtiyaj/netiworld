package com.netizen.netiworld.adapter

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.BalanceStatementGetData
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*
import kotlin.collections.ArrayList

class BalanceStatementListAdapter(var mContext: Context, var balanceStatementList: List<BalanceStatementGetData>) :
    RecyclerView.Adapter<BalanceStatementListAdapter.BalanceStatementViewHolder>(), Filterable {
    private var filteredbalanceStatementList: List<BalanceStatementGetData>? = null

    inner class BalanceStatementViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var transfer_for: TextView
        var cash_in: TextView
        var cash_out: TextView

        init {
            date= view.findViewById(R.id.request_date) as TextView
            transfer_for= view.findViewById(R.id.trans_for_val) as TextView
            cash_in = view.findViewById(R.id.cash_in_val) as TextView
            cash_out = view.findViewById(R.id.cash_out_val) as TextView
        }
    }

    init {
        this.filteredbalanceStatementList = balanceStatementList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BalanceStatementViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_wallet_balance_statement_row, parent, false)
        // Return a new holder instance
        return BalanceStatementViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: BalanceStatementViewHolder, position: Int) {
        // Get the item model based on position
        val balanceStatement = filteredbalanceStatementList?.get(position)

        holder.date.text = balanceStatement?.transactionDate?.toLong()?.let { MyUtilsClass.getDate(it) }

        holder.transfer_for.text = balanceStatement?.getTransType()
        holder.cash_in.text = String.format("%,.2f",balanceStatement?.income)
        holder.cash_out.text = String.format("%,.2f",balanceStatement?.expense)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredbalanceStatementList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredbalanceStatementList = balanceStatementList
                } else {
                    val filteredList = ArrayList<BalanceStatementGetData>()
                    for (row in balanceStatementList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTransType()!!.toLowerCase().contains(charString.toLowerCase()) || row.getTransType()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredbalanceStatementList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredbalanceStatementList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredbalanceStatementList = filterResults.values as ArrayList<BalanceStatementGetData>
                notifyDataSetChanged()
            }
        }
    }

    fun setFilter(newList: List<BalanceStatementGetData>) {
        balanceStatementList = ArrayList()
        (balanceStatementList as ArrayList<BalanceStatementGetData>).addAll(newList)
        notifyDataSetChanged()
    }

}
