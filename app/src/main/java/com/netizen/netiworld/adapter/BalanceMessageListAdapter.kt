package com.netizen.netiworld.adapter

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.BalanceMessageGetData
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*
import kotlin.collections.ArrayList


class BalanceMessageListAdapter(var mContext: Context, var balanceMessageList: List<BalanceMessageGetData>) :
    RecyclerView.Adapter<BalanceMessageListAdapter.BalanceMessageViewHolder>(), Filterable {
    private var filteredbalanceMessageList: List<BalanceMessageGetData>? = null

    inner class BalanceMessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var message_type: TextView
        var message_Quantity: TextView
        var payable_amount: TextView

        init {
            date= view.findViewById(R.id.request_date) as TextView
            message_type= view.findViewById(R.id.messageType_val) as TextView
            message_Quantity = view.findViewById(R.id.messageQuantity_val) as TextView
            payable_amount = view.findViewById(R.id.payableAmount_val) as TextView
        }
    }

    init {
        this.filteredbalanceMessageList = balanceMessageList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BalanceMessageViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_wallet_message_row, parent, false)
        // Return a new holder instance
        return BalanceMessageViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: BalanceMessageViewHolder, position: Int) {
        // Get the item model based on position
        val messageRecharge = filteredbalanceMessageList?.get(position)

        holder.date.text = messageRecharge?.getTrxDate()?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.message_type.text = messageRecharge?.getProductName().toString()
        holder.message_Quantity.text = messageRecharge?.getQuantity().toString()
        holder.payable_amount.text = String.format("%,.2f",messageRecharge?.getPayableAmount())
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredbalanceMessageList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredbalanceMessageList = balanceMessageList
                } else {
                    val filteredList = ArrayList<BalanceMessageGetData>()
                    for (row in balanceMessageList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getProductName()!!.toLowerCase().contains(charString.toLowerCase()) || row.getTrxDate()!!.contains(charSequence)) {
                            filteredList.add(row)
                        }
                    }

                    filteredbalanceMessageList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredbalanceMessageList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredbalanceMessageList = filterResults.values as ArrayList<BalanceMessageGetData>
                notifyDataSetChanged()
            }
        }
    }

}
