package com.netizen.netiworld.adapter

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.BalanceTransferGetData
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*
import kotlin.collections.ArrayList

class BalanceTransferListAdapter(val mContext: Context, val balanceTransferList: List<BalanceTransferGetData>) :
    RecyclerView.Adapter<BalanceTransferListAdapter.BalanceTransferViewHolder>(), Filterable {
    private var filteredBalanceTransferList: List<BalanceTransferGetData>? = null

    inner class BalanceTransferViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var trans_type_val: TextView
        var neti_id: TextView
        var name: TextView
        var mobile_no: TextView
        var note: TextView
        var transfer_amt: TextView

        init {
            date= view.findViewById(R.id.request_date) as TextView
            trans_type_val= view.findViewById(R.id.trans_type_val) as TextView
            neti_id= view.findViewById(R.id.netiId_val) as TextView
            name = view.findViewById(R.id.name_val) as TextView
            mobile_no = view.findViewById(R.id.mobile_no_val) as TextView
            note = view.findViewById(R.id.note_val) as TextView
            transfer_amt = view.findViewById(R.id.trans_amount_val) as TextView
        }
    }

    init {
        this.filteredBalanceTransferList = balanceTransferList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BalanceTransferViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_wallet_transfer_row, parent, false)
        // Return a new holder instance
        return BalanceTransferViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: BalanceTransferViewHolder, position: Int) {
        // Get the item model based on position
        val balanceTransfer = filteredBalanceTransferList?.get(position)

        holder.date.text = balanceTransfer?.transactionDate?.toLong()?.let { MyUtilsClass.getDate(it) }
        if (balanceTransfer?.transactionFor=="Receive"){
            holder.trans_type_val.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_left_black_24dp, 0);
        }
        else if(balanceTransfer?.transactionFor=="Send"){
            holder.trans_type_val.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_black_24dp, 0);
        }
        holder.trans_type_val.text = balanceTransfer?.transactionFor
        holder.neti_id.text = balanceTransfer?.netiID.toString()
        holder.name.text = balanceTransfer?.fullName
        holder.mobile_no.text = balanceTransfer?.basicMobile
        holder.note.text = balanceTransfer?.note
        holder.transfer_amt.text = String.format("%, .2f", balanceTransfer?.amount)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredBalanceTransferList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredBalanceTransferList = balanceTransferList
                } else {
                    val filteredList = ArrayList<BalanceTransferGetData>()
                    for (row in balanceTransferList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.fullName!!.toLowerCase().contains(charString.toLowerCase()) || row.fullName!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredBalanceTransferList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredBalanceTransferList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredBalanceTransferList = filterResults.values as ArrayList<BalanceTransferGetData>
                notifyDataSetChanged()
            }
        }
    }

}
