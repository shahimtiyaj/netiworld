package com.netizen.netiworld.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.app.AppController.Companion.getDate
import com.netizen.netiworld.model.BalanceDepositGetData

class PaginationAdapter(val mContext: Context, val balanceDepositList: MutableList<BalanceDepositGetData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var filterededBalanceDepositList: MutableList<BalanceDepositGetData>? = null
    private val LOADING = 0
    private var isLoadingAdded = false
    private val ITEM = 1

    init {
        filterededBalanceDepositList = balanceDepositList
    }

    fun setBalanceDepositGetDataList(BalanceDepositGetDataList: MutableList<BalanceDepositGetData>) {
        this.filterededBalanceDepositList = BalanceDepositGetDataList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val viewItem = inflater.inflate(R.layout.logs_wallet_deposit_row, parent, false)
                viewHolder = BankDepositViewHolder(viewItem)
            }
            LOADING -> {
                val viewLoading = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(viewLoading)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val balanceDeposit = filterededBalanceDepositList?.get(position)

        when (getItemViewType(position)) {
            ITEM -> {
                // Get the item model based on position
                val bholder = holder as BankDepositViewHolder

                bholder.request_date.text = balanceDeposit?.requestDate?.toLong()?.let { getDate(it) }
                bholder.bank_name_val.text = balanceDeposit?.bank
                bholder.ac_number_val.text = balanceDeposit?.accountNumber
                bholder.request_amt_val.text = balanceDeposit?.requestedAmount.toString()
                bholder.trans_date_val.text = balanceDeposit?.transactionDate?.toLong()?.let { getDate(it) }
                bholder.trans_id_val.text = balanceDeposit?.transactionNumber
                bholder.processed_date_val.text = balanceDeposit?.approveDate?.toLong()?.let { getDate(it) }
                bholder.status_val.text = balanceDeposit?.requestStatus
            }

            LOADING -> {
                val loadingViewHolder = holder as LoadingViewHolder
                loadingViewHolder.progressBar.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return if (filterededBalanceDepositList == null) 0 else filterededBalanceDepositList!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == filterededBalanceDepositList!!.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(BalanceDepositGetData())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = filterededBalanceDepositList!!.size - 1
        val result = getItem(position)

        if (result != null) {
            filterededBalanceDepositList!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun add(BalanceDepositGetData: BalanceDepositGetData) {
        filterededBalanceDepositList!!.add(BalanceDepositGetData)
        notifyItemInserted(filterededBalanceDepositList!!.size - 1)
    }

    fun addAll(moveResults: List<BalanceDepositGetData>) {
        for (result in moveResults) {
            add(result)
        }
    }

    fun getItem(position: Int): BalanceDepositGetData? {
        return filterededBalanceDepositList!![position]
    }


    inner class BankDepositViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var request_date: TextView
        var bank_name_val: TextView
        var ac_number_val: TextView
        var request_amt_val: TextView
        var trans_date_val: TextView
        var trans_id_val: TextView
        var processed_date_val: TextView
        var status_val: TextView

        init {
            request_date= view.findViewById(R.id.request_date) as TextView
            bank_name_val = view.findViewById(R.id.bank_name_val) as TextView
            ac_number_val = view.findViewById(R.id.ac_number_val) as TextView
            request_amt_val = view.findViewById(R.id.request_amt_val) as TextView
            trans_date_val = view.findViewById(R.id.trans_date_val) as TextView
            trans_id_val = view.findViewById(R.id.trans_id_val) as TextView
            processed_date_val = view.findViewById(R.id.processed_date_val) as TextView
            status_val = view.findViewById(R.id.status_val) as TextView
        }
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val progressBar: ProgressBar

        init {
            progressBar = itemView.findViewById(R.id.loadmore_progress)

        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filterededBalanceDepositList = balanceDepositList
                } else {
                    val filteredList = ArrayList<BalanceDepositGetData>()
                    for (row in balanceDepositList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.accountNumber!!.toLowerCase().contains(charString.toLowerCase()) || row.accountNumber!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filterededBalanceDepositList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filterededBalanceDepositList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filterededBalanceDepositList = filterResults.values as ArrayList<BalanceDepositGetData>
                notifyDataSetChanged()
            }
        }
    }

    companion object {
        private val LOADING = 0
        private val ITEM = 1
    }


}