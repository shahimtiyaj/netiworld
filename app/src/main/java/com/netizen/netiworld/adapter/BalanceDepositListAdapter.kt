package com.netizen.netiworld.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.BalanceDepositGetData
import com.netizen.netiworld.utils.MyUtilsClass

class BalanceDepositListAdapter(val mContext: Context, val balanceDepositList: List<BalanceDepositGetData>) :
    RecyclerView.Adapter<BalanceDepositListAdapter.BankDepositViewHolder>(), Filterable {
    var filterededBalanceDepositList: List<BalanceDepositGetData>? = null

    inner class BankDepositViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var request_date: TextView
        var bank_name_val: TextView
        var bank_branch_val: TextView
        var ac_number_val: TextView
        var request_amt_val: TextView
        var trans_date_val: TextView
        var trans_id_val: TextView
        var processed_date_val: TextView
        var status_val: TextView

        init {
            request_date= view.findViewById(R.id.request_date) as TextView
            bank_name_val = view.findViewById(R.id.bank_name_val) as TextView
            bank_branch_val = view.findViewById(R.id.bank_branch_val) as TextView
            ac_number_val = view.findViewById(R.id.ac_number_val) as TextView
            request_amt_val = view.findViewById(R.id.request_amt_val) as TextView
            trans_date_val = view.findViewById(R.id.trans_date_val) as TextView
            trans_id_val = view.findViewById(R.id.trans_id_val) as TextView
            processed_date_val = view.findViewById(R.id.processed_date_val) as TextView
            status_val = view.findViewById(R.id.status_val) as TextView
        }
    }

    init {
        this.filterededBalanceDepositList = balanceDepositList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankDepositViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.logs_wallet_deposit_row, parent, false)
        // Return a new holder instance
        return BankDepositViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: BankDepositViewHolder, position: Int) {
        // Get the item model based on position
        val balanceDeposit = filterededBalanceDepositList?.get(position)
        //hr list animation.....
        /*   val animation = AnimationUtils.loadAnimation(mContext, R.anim.translate)
           animation.startOffset = (30 * position).toLong()//Provide delay here
           holder.itemView.startAnimation(animation)*/
        // Set item views based on our views and data model

        holder.request_date.text = balanceDeposit?.requestDate?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.bank_name_val.text = balanceDeposit?.bank
        holder.bank_branch_val.text = balanceDeposit?.fromWhere
        holder.ac_number_val.text = balanceDeposit?.accountNumber
       //holder.request_amt_val.text = String.format("%.2f",balanceDeposit?.requestedAmount)
        holder.request_amt_val.text =String.format("%,.2f",balanceDeposit?.requestedAmount)
        holder.trans_date_val.text = balanceDeposit?.transactionDate?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.trans_id_val.text = balanceDeposit?.transactionNumber
        holder.processed_date_val.text = balanceDeposit?.approveDate?.toLong()?.let { MyUtilsClass.getDate(it) }

        if (balanceDeposit?.requestStatus=="Approved"){
            holder.status_val.text = balanceDeposit.requestStatus
            holder.status_val.setTextColor(Color.parseColor("#44bd32"))
        }

        else if(balanceDeposit?.requestStatus=="Rejected"){
            holder.status_val.text = balanceDeposit.requestStatus
            holder.status_val.setTextColor(Color.parseColor("#e84118"))
        }
        else{
            holder.status_val.text = balanceDeposit?.requestStatus
            holder.status_val.setTextColor(Color.parseColor("#0097e6"))
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filterededBalanceDepositList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filterededBalanceDepositList = balanceDepositList
                } else {
                    val filteredList = ArrayList<BalanceDepositGetData>()
                    for (row in balanceDepositList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.bank!!.toLowerCase().contains(charString.toLowerCase()) || row.bank!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filterededBalanceDepositList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filterededBalanceDepositList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filterededBalanceDepositList = filterResults.values as List<BalanceDepositGetData>?
                notifyDataSetChanged()
            }
        }
    }

  /*  private fun getDate(time: Long): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = time
        return DateFormat.format("dd MMM, yyyy", cal).toString()
    }*/
}