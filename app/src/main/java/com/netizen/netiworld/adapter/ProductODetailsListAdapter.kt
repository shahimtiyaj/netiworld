package com.netizen.netiworld.adapter

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.ProductGDetailsGetData
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*


class ProductODetailsListAdapter(
    private val mContext: Context,
    private val productOfferDetailsList: List<ProductGDetailsGetData>
) :
    RecyclerView.Adapter<ProductODetailsListAdapter.ProductGDetailsViewHolder>() {
    private var filteredproductOfferDetailsList: List<ProductGDetailsGetData>? = null

    inner class ProductGDetailsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var purchaseCode: TextView
        var purchaseStatus: TextView
        var usedCodeDate: TextView

        init {
            purchaseCode = view.findViewById(R.id.purchase_code) as TextView
            purchaseStatus = view.findViewById(R.id.product_status) as TextView
            usedCodeDate = view.findViewById(R.id.codeUsedDate) as TextView
        }
    }

    init {
        this.filteredproductOfferDetailsList = productOfferDetailsList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductGDetailsViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_product_general_details_row, parent, false)
        // Return a new holder instance
        return ProductGDetailsViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: ProductGDetailsViewHolder, position: Int) {
        try {

            // Get the item model based on position
            val productGDetails = filteredproductOfferDetailsList?.get(position)

            holder.purchaseCode.text = productGDetails?.getPurchaseCode()
            if (productGDetails?.getUsedStatus() == 1) {
                holder.purchaseStatus.text = "Used"
            } else {
                holder.purchaseStatus.text = "Unused"
            }
            holder.usedCodeDate.text = productGDetails?.getUsedDate()?.toLong()?.let { MyUtilsClass.getDate(it) }

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredproductOfferDetailsList?.size!!
    }


}
