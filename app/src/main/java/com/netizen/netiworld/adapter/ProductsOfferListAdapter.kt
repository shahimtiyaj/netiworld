package com.netizen.netiworld.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.ProductsOfferGetData
import com.netizen.netiworld.reports.ProductOfferDetailsListActivity
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*
import kotlin.collections.ArrayList


class ProductsOfferListAdapter(val mContext: Context, val productOfferList: List<ProductsOfferGetData>) :
    RecyclerView.Adapter<ProductsOfferListAdapter.ProductOfferViewHolder>(), Filterable {
    
    private var filteredproductOfferList: List<ProductsOfferGetData>? = null

    inner class ProductOfferViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var productType: TextView
        var productName: TextView
        var offerCode: TextView
        var productQnty: TextView
        var totalAmt: TextView
        var discountAmt: TextView
        var payableAmt: TextView
        var purchaseCodeView: TextView


        init {
            date = view.findViewById(R.id.request_date) as TextView
            productType = view.findViewById(R.id.product_type_val) as TextView
            productName = view.findViewById(R.id.product_name_val) as TextView
            offerCode = view.findViewById(R.id.offer_code_val) as TextView
            productQnty = view.findViewById(R.id.product_quantity_val) as TextView
            totalAmt = view.findViewById(R.id.total_amount_val) as TextView
            discountAmt = view.findViewById(R.id.discount_amount_val) as TextView
            payableAmt = view.findViewById(R.id.payable_amount_val) as TextView

            purchaseCodeView = view.findViewById(R.id.purchase_code_view) as TextView

        }
    }

    init {
        this.filteredproductOfferList = productOfferList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductOfferViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_product_offer_row, parent, false)
        // Return a new holder instance
        return ProductOfferViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: ProductOfferViewHolder, position: Int) {
        // Get the item model based on position
        val productOffer = filteredproductOfferList?.get(position)

        holder.date.text = productOffer?.getOfferUseDate()?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.offerCode.text = productOffer?.getOfferCode().toString()
        holder.productType.text = productOffer?.getProductType()
        holder.productName.text = productOffer?.getProductName()
        holder.productQnty.text = productOffer?.getProductQuantity().toString()
        holder.totalAmt.text = String.format("%,.2f", productOffer?.getTotalPrice())
        holder.discountAmt.text = String.format("%,.2f", productOffer?.getTotalDiscount())
        holder.payableAmt.text = String.format("%,.2f", productOffer?.getPayableAmount())

        if (productOffer?.getGenCodeStatus() == 0) {
            holder.purchaseCodeView.visibility = View.INVISIBLE
        } else {
            holder.purchaseCodeView.visibility = View.VISIBLE
            holder.purchaseCodeView.setOnClickListener {  }

            holder.purchaseCodeView.setOnClickListener {

                val intent = Intent(mContext, ProductOfferDetailsListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("productPurchaseLogID", productOffer?.getProductPurchaseLogID().toString())
                intent.putExtra("purchasePoint", productOffer?.getPurchasePoint().toString())
                intent.putExtra("date", productOffer?.getOfferUseDate().toString())
                intent.putExtra("productType", productOffer?.getProductType())
                intent.putExtra("offerCode", productOffer?.getOfferCode())
                intent.putExtra("productName", productOffer?.getProductName())
                intent.putExtra("productQnty", productOffer?.getProductQuantity().toString())

                 mContext.startActivity(intent)
                (it.context as Activity).finish()

            }
        }

    }


    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredproductOfferList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredproductOfferList = productOfferList
                } else {
                    val filteredList = ArrayList<ProductsOfferGetData>()
                    for (row in productOfferList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getOfferCode()!!.toLowerCase().contains(charString.toLowerCase()) || row.getProductName()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredproductOfferList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredproductOfferList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredproductOfferList =
                    filterResults.values as ArrayList<ProductsOfferGetData>
                notifyDataSetChanged()
            }
        }
    }

}
