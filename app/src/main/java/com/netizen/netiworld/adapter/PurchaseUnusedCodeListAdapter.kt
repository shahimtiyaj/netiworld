package com.netizen.netiworld.adapter

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.PurchaseCodeLogGetdata
import com.netizen.netiworld.utils.MyUtilsClass
import java.lang.NumberFormatException
import java.util.*
import kotlin.collections.ArrayList

class PurchaseUnusedCodeListAdapter(
    private val mContext: Context,
    private val productGeneralList: List<PurchaseCodeLogGetdata>
) :
    RecyclerView.Adapter<PurchaseUnusedCodeListAdapter.PurchaseCodeViewHolder>(), Filterable {
    private var filteredPurchaseCodeList: List<PurchaseCodeLogGetdata>? = null

    inner class PurchaseCodeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var purchasePoint: TextView
        var productType: TextView
        var productName: TextView
        var purchaseCode: TextView

        init {
            date = view.findViewById(R.id.request_date) as TextView
            purchasePoint = view.findViewById(R.id.purchase_point_val) as TextView
            productType = view.findViewById(R.id.product_type_val) as TextView
            productName = view.findViewById(R.id.product_name_val) as TextView
            purchaseCode = view.findViewById(R.id.purchase_code_val) as TextView
        }
    }

    init {
        this.filteredPurchaseCodeList = productGeneralList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PurchaseCodeViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_unused_purchase_code, parent, false)
        // Return a new holder instance
        return PurchaseCodeViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: PurchaseCodeViewHolder, position: Int) {
        try {

            // Get the item model based on position
            val purchaseCode = filteredPurchaseCodeList?.get(position)

            if(purchaseCode?.getPurchaseDate()=="null"){
                holder.date.text = "---"
            }
            else{
                holder.date.text = purchaseCode?.getPurchaseDate()?.toLong()?.let { MyUtilsClass.getDate(it) }
            }
            holder.purchasePoint.text = purchaseCode?.getCoreRoleNote()
            holder.productType.text = purchaseCode?.getProductType()
            holder.productName.text = purchaseCode?.getProductName()
            holder.purchaseCode.text = purchaseCode?.getPurchaseCode()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }


    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredPurchaseCodeList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredPurchaseCodeList = productGeneralList
                } else {
                    val filteredList = ArrayList<PurchaseCodeLogGetdata>()
                    for (row in productGeneralList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getProductName()!!.toLowerCase().contains(charString.toLowerCase()) || row.getProductName()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredPurchaseCodeList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredPurchaseCodeList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredPurchaseCodeList =
                    filterResults.values as ArrayList<PurchaseCodeLogGetdata>
                notifyDataSetChanged()
            }
        }
    }

}
