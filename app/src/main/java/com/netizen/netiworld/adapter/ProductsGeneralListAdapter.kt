package com.netizen.netiworld.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.model.ProductsGeneralGetData
import com.netizen.netiworld.reports.ProductGeneralDetailsListActivity
import com.netizen.netiworld.utils.MyUtilsClass
import java.util.*
import kotlin.collections.ArrayList


class ProductsGeneralListAdapter(
    private val mContext: Context,
    private val productGeneralList: List<ProductsGeneralGetData>
) :
    RecyclerView.Adapter<ProductsGeneralListAdapter.ProductGeneralViewHolder>(), Filterable {
    private var filteredproductGeneralList: List<ProductsGeneralGetData>? = null

    inner class ProductGeneralViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date: TextView
        var purchasePoint: TextView
        var productType: TextView
        var productName: TextView
        var productQnty: TextView
        var payableAmt: TextView
        var purchaseCodeView: TextView

        init {
            date = view.findViewById(R.id.request_date) as TextView
            purchasePoint = view.findViewById(R.id.purchase_point_val) as TextView
            productType = view.findViewById(R.id.product_type_val) as TextView
            productName = view.findViewById(R.id.product_name_val) as TextView
            productQnty = view.findViewById(R.id.product_quantity_val) as TextView
            payableAmt = view.findViewById(R.id.payable_amount_val) as TextView

            purchaseCodeView = view.findViewById(R.id.purchase_code_view) as TextView
        }
    }

    init {
        this.filteredproductGeneralList = productGeneralList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductGeneralViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.logs_product_general_row, parent, false)
        // Return a new holder instance
        return ProductGeneralViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: ProductGeneralViewHolder, position: Int) {
        // Get the item model based on position
        val productGeneral = filteredproductGeneralList?.get(position)

        holder.date.text = productGeneral?.getPurchaseDate()?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.purchasePoint.text = productGeneral?.getCoreRoleNote().toString()
        holder.productType.text = productGeneral?.getCategoryName()
        holder.productName.text = productGeneral?.getProductName()
        holder.productQnty.text = productGeneral?.getPurchaseQuantity().toString()
        holder.payableAmt.text = String.format("%,.2f",productGeneral?.getTotalAmount())

        if (productGeneral?.getGenCodeStatus() == 0) {
            holder.purchaseCodeView.visibility = View.INVISIBLE
        } else {
            holder.purchaseCodeView.visibility = View.VISIBLE

            holder.purchaseCodeView.setOnClickListener {

                val intent = Intent(mContext, ProductGeneralDetailsListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("productPurchaseID", productGeneral?.getProductPurchaseID().toString())
                intent.putExtra("purchasePoint", productGeneral?.getCoreRoleNote().toString())
                intent.putExtra("date", productGeneral?.getPurchaseDate().toString())
                intent.putExtra("productType", productGeneral?.getCategoryName())
                intent.putExtra("productName", productGeneral?.getProductName())
                intent.putExtra("productQnty", productGeneral?.getPurchaseQuantity().toString())

                mContext.startActivity(intent)
                (it.context as Activity).finish()

                // (mContext as Activity).finish()

                // val bundle = Bundle()
                //bundle.putString("productPurchaseID", productGeneral?.getGenCodeStatus().toString())

                //AppController.context?.startActivity(Intent(AppController.context, ProductGeneralDetailsListActivity::class.java))

            }
        }


    }


    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredproductGeneralList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredproductGeneralList = productGeneralList
                } else {
                    val filteredList = ArrayList<ProductsGeneralGetData>()
                    for (row in productGeneralList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCoreRoleNote()!!.toLowerCase().contains(charString.toLowerCase()) || row.getCoreRoleNote()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredproductGeneralList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredproductGeneralList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredproductGeneralList =
                    filterResults.values as ArrayList<ProductsGeneralGetData>
                notifyDataSetChanged()
            }
        }
    }

}
