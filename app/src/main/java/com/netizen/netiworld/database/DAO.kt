package com.netizen.netiworld.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.model.*
import java.util.ArrayList

/*
 * Data access object class
 * Data Access Objects are the main classes where we define our database interactions
 */
class DAO(context: Context) {

    // Database fields
    private var db: SQLiteDatabase? = null
    private val dbHelper: DBHelper

    init {
        dbHelper = DBHelper(context)
    }

    @Throws(Throwable::class)
    protected fun finalize() {
        // TODO Auto-generated method stub
        if (db != null && db!!.isOpen)
            db!!.close()
    }

    /*
    Open any close database object
     */
    @Throws(SQLException::class)
    fun open() {
        //Create and/or open a database that will be used for reading and writing.
        db = dbHelper.writableDatabase
    }

    /*
     Close any open database object.
     */
    fun close() {
        dbHelper.close()
    }

    /*
    select query for retrieving data from table
    returns a set of rows and columns in a Cursor.
     */
    fun getRecordsCursor(sql: String, param: Array<String>): Cursor? {
        var curs: Cursor? = null
        curs = db?.rawQuery(sql, param)
        return curs
    }


    /*
    execSQL doesn't return anything and used for creating,updating, replacing
     */
    @Throws(SQLException::class)
    fun execSQL(sql: String, param: Array<String>) {
        db!!.execSQL(sql, param)
    }


    companion object {

        private val TAG = DAO::class.java.simpleName

        /*
        executeSQL doesn't return anything and used for creating,updating, replacing
        Not need to create extra DAO object . */

        fun executeSQL(sql: String, param: Array<String>) {
            val da = AppController.instance?.let { DAO(it) }
            da?.open()
            try {
                da?.execSQL(sql, param)
            } catch (e: Exception) {
                throw e
            } finally {
                da?.close()
            }
        }
    }


    fun getUserLoginDetails(): UserLogin1 {

        //  val selectQuery = "SELECT *FROM Login where user_id= (SELECT max(user_id) FROM Login order by user_id desc limit 1)"
        try {


            val selectQuery = "SELECT *FROM UserLogin"

            val cursor = db?.rawQuery(selectQuery, null)
            val user = UserLogin1()
            // looping through all rows and adding to list
            if (cursor?.moveToFirst()!!) {
                do {
                    //user.setuserId(cursor.getString(1))
                    user.setuserName(cursor.getString(1))
                    // user.setcheck(cursor.getString(1))
                    // user.setuserPassword(cursor.getString(2))

                } while (cursor.moveToNext())
            }

            return user
        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }

    fun getUserProfileDetails(): Info {

        //  val selectQuery = "SELECT *FROM Login where user_id= (SELECT max(user_id) FROM Login order by user_id desc limit 1)"
        try {


            val selectQuery = "SELECT *FROM UserProfileInfo"

            val cursor = db?.rawQuery(selectQuery, null)
            val user = Info()
            // looping through all rows and adding to list
            if (cursor?.moveToFirst()!!) {
                do {
                    //user.setuserId(cursor.getString(1))
                    user.setNetiID(cursor.getInt(0))
                    user.setFullName(cursor.getString(1))
                    user.setBasicMobile(cursor.getString(2))
                    user.setBasicEmail(cursor.getString(3))
                    user.setGender(cursor.getString(4))
                    user.setReligion(cursor.getString(5))
                    user.setDateOfBirth(cursor.getString(6))
                    user.setUserWalletBalance(cursor.getDouble(7))
                    user.setSmsBalance(cursor.getDouble(8))
                    user.setAddress(cursor.getString(9))
                    user.setImagePath(cursor.getString(10))

                } while (cursor.moveToNext())
            }

            return user
        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }


    // TODO Auto-generated catch block
    val allMobileBankData: ArrayList<MobileBank>
        get() {
            val mobilebankArrayList = ArrayList<MobileBank>()
            var mobilebank: MobileBank?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "${DBHelper.TABLE_MOBILE_BANK} ORDER BY coreCategoryID ASC",
                    arrayOf(
                        "[categoryName]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        mobilebank = MobileBank(
                            curs.getString(0)
                        )
                        mobilebankArrayList.add(mobilebank)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return mobilebankArrayList
        }

    // TODO Auto-generated catch block
    val allMobileBankAccData: ArrayList<MobileBankAcNo>
        get() {
            val mobilebankAccArrayList = ArrayList<MobileBankAcNo>()
            var mobileAccBank: MobileBankAcNo?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "${DBHelper.TABLE_BANK_ACC_NUMBER} ORDER BY coreBankAccId ASC",
                    arrayOf(
                        "[accShortName]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        mobileAccBank = MobileBankAcNo(
                            curs.getString(0)
                        )
                        mobilebankAccArrayList.add(mobileAccBank)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return mobilebankAccArrayList
        }

    fun getCoreCategoryID(coreCategoryName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [coreCategoryID] FROM MobileBank WHERE categoryName = '$coreCategoryName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getCoreBankAccID(accShortName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [coreBankAccId] FROM BankAccNumber WHERE accShortName = '$accShortName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun getCategoryDefaultCode(coreCategoryName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [categoryDefaultCode] FROM MobileBank WHERE categoryName = '$coreCategoryName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    // TODO Auto-generated catch block
    val allMessageTypeData: ArrayList<MessageType>
        get() {
            val messageTypeArrayList = ArrayList<MessageType>()
            var messageType: MessageType
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "${DBHelper.TABLE_MESSAGE_TYPE} ORDER BY productID ASC",
                    arrayOf(
                        "[productName]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        messageType = MessageType(
                            curs.getString(0)
                        )
                        messageTypeArrayList.add(messageType)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return messageTypeArrayList
        }

    fun getProductID(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [productID] FROM MessageType WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getUnitPrice(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [salesPrice] FROM MessageType WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun getMessageTypeAll(): MessageType {

        //  val selectQuery = "SELECT *FROM Login where user_id= (SELECT max(user_id) FROM Login order by user_id desc limit 1)"
        try {

            val selectQuery = "SELECT *FROM MessageType"

            val cursor = db?.rawQuery(selectQuery, null)
            val messageType = MessageType()
            // looping through all rows and adding to list
            if (cursor?.moveToFirst()!!) {
                do {
                    //user.setuserId(cursor.getString(1))
                    messageType.setProductID(cursor.getLong(0))
                    messageType.setProductName(cursor.getString(1))
                    messageType.setSalesPrice(cursor.getDouble(2))
                    messageType.setPercentVat(cursor.getDouble(3))

                } while (cursor.moveToNext())
            }

            return messageType

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }


    // TODO Auto-generated catch block
    val allAssignRole: ArrayList<PurchasePoint>
        get() {
            val purchasepoinyArrayList = ArrayList<PurchasePoint>()
            var purchaseType: PurchasePoint
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "${DBHelper.TABLE_USER_ROLE_ASSIGN} ORDER BY coreRoleID ASC",
                    arrayOf(
                        "[coreRoleName]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        purchaseType = PurchasePoint(
                            curs.getString(0))
                        purchasepoinyArrayList.add(purchaseType)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return purchasepoinyArrayList
        }

    fun getRoleID(coreRoleName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [coreRoleID] FROM UserRoleAssign WHERE coreRoleName = '$coreRoleName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getRoleAssignID(coreRoleName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [userRoleAssignID] FROM UserRoleAssign WHERE coreRoleName = '$coreRoleName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getUserProductID(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [productID] FROM ProductsInfo WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getUserProductRoleAssignID(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [productRolesAssign] FROM ProductsInfo WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getUserProductUnitPrice(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [salesPrice] FROM ProductsInfo WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getUserProductVatPercentage(productName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [percentVat] FROM ProductsInfo WHERE productName = '$productName'"
            curs = db?.rawQuery(selectQuery, null)
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    // TODO Auto-generated catch block
    val allProductsName: ArrayList<Products>
        get() {
            val productsArrayList = ArrayList<Products>()
            var productName: Products
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "${DBHelper.TABLE_PRODUCTS_INFO} ORDER BY productID ASC",
                    arrayOf(
                        "[productName]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        productName = Products(
                            curs.getString(0))
                        productsArrayList.add(productName)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return productsArrayList
        }


    fun getproductAll(): Products {

        //  val selectQuery = "SELECT *FROM Login where user_id= (SELECT max(user_id) FROM Login order by user_id desc limit 1)"
        try {

            val selectQuery = "SELECT *FROM ProductsInfo"

            val cursor = db?.rawQuery(selectQuery, null)
            val products = Products()
            // looping through all rows and adding to list
            if (cursor?.moveToFirst()!!) {
                do {
                    //user.setuserId(cursor.getString(1))
                    products.setProductID(cursor.getInt(0))
                    products.setProductName(cursor.getString(1))
                    products.setSalesPrice(cursor.getDouble(2))
                    products.setPercentVat(cursor.getDouble(3))

                } while (cursor.moveToNext())
            }

            return products

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }

    fun deleteBankNameList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_MOBILE_BANK, null, null)

            Log.d(TAG, "deleteAccountList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    fun deleteAccountNoList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_BANK_ACC_NUMBER, null, null)

            Log.d(TAG, "deleteAccountList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteProductNameList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_PRODUCTS_INFO, null, null)

            Log.d(TAG, "deleteProductInfoList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteMessageTypeList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_MESSAGE_TYPE, null, null)

            Log.d(TAG, "messageTypeList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    fun deleteProfileList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_USER_PROFILE_INFO, null, null)

            Log.d(TAG, "deleteProfileList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    fun deleteUserRoleAssignList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(DBHelper.TABLE_USER_ROLE_ASSIGN, null, null)

            Log.d(TAG, "deleteUserRoleAssign")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

}

