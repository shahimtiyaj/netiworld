package com.netizen.netiworld.activity.utils

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.toptoche.searchablespinnerlibrary.SearchableSpinner

class CustomSearchableSpinner : SearchableSpinner {

    private var mLastClickTime: Long = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}


    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {

            val lastClickTime = mLastClickTime
            val now = System.currentTimeMillis()
            mLastClickTime = now
            return if (now - lastClickTime < MIN_DELAY_MS) {
                // Too fast: ignore
                true
            } else {
                // Register the click
                super.onTouch(v, event)
            }
        }
        return true
    }

    companion object {

        var isCountriesSpinnerOpen = false

        private val MIN_DELAY_MS: Long = 500
    }
}