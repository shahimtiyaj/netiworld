package com.netizen.netiworld.activity.balanceTransfer

import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import com.chaos.view.PinView
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceTransfer.BalanceWalletTransfer01.Companion.PhoneNo
import com.netizen.netiworld.activity.balanceTransfer.BalanceWalletTransfer01.Companion.fullName
import com.netizen.netiworld.model.WalletTransfer
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import com.orhanobut.logger.Logger
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BalanceWalletTransferOtp02 : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var transfer: Button? = null

    private var pinView: PinView? = null
    private var next: Button? = null
    private var topText: TextView? = null
    private var time_count: TextView? = null
    private var textU: TextView? = null
    private var resendOtp: TextView? = null
    private var userName: EditText? = null
    private var userPhone: EditText? = null
    private var first: ConstraintLayout? = null
    private var second: ConstraintLayout? = null
    var viewDialog: ViewDialog? = null
    var myView: View?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_transfer_otp_02)
        toolBarInit()

        topText = findViewById(R.id.topText)
        pinView = findViewById(R.id.pinView)
        userName = findViewById(R.id.username)
        userPhone = findViewById(R.id.userPhone)
        first = findViewById(R.id.first_step)
        second = findViewById(R.id.secondStep)
        textU = findViewById(R.id.textView_noti)
        first?.visibility = View.VISIBLE
        viewDialog = ViewDialog(this)

        time_count = findViewById(R.id.time_count)
       // myView=findViewById(R.id.myview3)

        userName?.setText(fullName)
        userPhone?.setText(PhoneNo)

        transfer = findViewById<Button>(R.id.button)
        transfer?.setOnClickListener {
            onClick()
        }

        resendOtp = findViewById(R.id.resend_otp)
        //resendOtp?.underline()

        resendOtp?.setOnClickListener {
            reSendRequestForOTP()
        }
    }

    private fun TextView.underline() {
        paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletTransfer01::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Transfer(OTP)"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }



    private fun onClick() {

        if (transfer?.text == "Let's go!") {
            val name = userName?.text.toString()
            val phone = userPhone?.text.toString()

            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(phone)) {
                transfer?.text = "Verify"
                first?.visibility = View.GONE
                second?.visibility = View.VISIBLE
                topText?.text = "I Still don't trust you.\nTell me something that only two of us know."
            } else {
                Toast.makeText(this, "Please enter the details", Toast.LENGTH_SHORT).show()
            }

            timer()
        }

        else if (transfer?.text == "Verify") {
            time_count?.text = ""
            getWalletTransferData()
        }

        else if (transfer?.text == "Transfer") {
            time_count?.text = ""
            submitWalletTransferData()
            startActivity(Intent(this@BalanceWalletTransferOtp02, MainActivity::class.java))
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            overridePendingTransition(R.anim.right_in, R.anim.right_out)
        }
    }

    fun timer() {
        val timer = object: CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                time_count?.text = "OTP expired on after " + millisUntilFinished / 1000 + " seconds"
                resendOtp?.visibility=View.GONE
                //myView?.visibility=View.GONE
            }

            override fun onFinish() {
                time_count?.text = " "
                resendOtp?.visibility=View.VISIBLE
               // myView?.visibility=View.VISIBLE
            }
        }
        timer.start()
    }

    private fun getWalletTransferData() {

        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val OTP = pinView?.text!!.toString()

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.OTPVarify(header, OTP)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "OTP Check response :" + response.body().toString())

                if (response.code() == 200) {
                    pinView?.setLineColor(Color.GREEN)
                    textU?.text = "OTP Verified"
                    textU?.setTextColor(Color.GREEN)
                    transfer?.text = "Transfer"
                } else {
                    pinView?.setLineColor(Color.RED)
                    textU?.text = "X Incorrect OTP"
                    textU?.setTextColor(Color.RED)
                }

                viewDialog?.hideDialog()
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

            }
        })
    }

    private fun submitWalletTransferData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"
        try {

            val balanceWalletTransfer = WalletTransfer(
                BalanceWalletTransfer01.TransferAmount, WalletTransfer.Receiver(
                    BalanceWalletTransfer01.netiMainID.toLongOrNull()
                ), BalanceWalletTransfer01.note
            )

            Log.d("Transfer wallet final:", "JSON:" + balanceWalletTransfer)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.WalletBalanceTransferFinal(balanceWalletTransfer, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Log.d("onResponse", "Balance Wallet post final:" + response.body())
                    Logger.d("Balance Wallet post final:", response.body().toString())

                    if (response.code() == 202) {
                        Log.d("onResponse", "Success final:" + response.body())
                        Toasty.success(
                            applicationContext,
                            "Balance Wallet Transferred Successfully!!",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else if (response.code() == 400) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    viewDialog?.hideDialog()

                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()

                    Toasty.error(
                        applicationContext,
                        "Balance Wallet Transfer Fail !!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }


    private fun reSendRequestForOTP() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"
        try {

            val balanceWalletTransfer = WalletTransfer(
                BalanceWalletTransfer01.TransferAmount, WalletTransfer.Receiver(
                    BalanceWalletTransfer01.customNetiID.toLongOrNull()),
                BalanceWalletTransfer01.note
            )

            Log.d("Transfer wallet:", "JSON:" + balanceWalletTransfer)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.WalletBalanceTransfer(balanceWalletTransfer, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    Log.d("onResponse", "Balance Wallet post:" + response.body())
                    Logger.d("Balance Wallet post:", response.body().toString())

                    if (response.code() == 200) {
                        Log.d("onResponse", "Success:" + response.body())

                        Toasty.success(
                            applicationContext,
                            "6 Digit OTP code has re sent !!",
                            Toast.LENGTH_LONG,
                            true
                        ).show()

                        timer()
                    }
                    else if (response.code() == 400) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }
                    viewDialog?.hideDialog()
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()

                    Toasty.error(
                        applicationContext,
                        "Balance Wallet Check Fail !!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    companion object {
        private val TAG = "BalanceDepositDateSelection"
        var timer = ""
    }
}
