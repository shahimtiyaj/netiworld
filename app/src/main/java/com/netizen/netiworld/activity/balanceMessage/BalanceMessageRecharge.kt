package com.netizen.netiworld.activity.balanceMessage

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.purchaseGeneralProduct.GeneralProduct01
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.*
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import com.orhanobut.logger.Logger
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class BalanceMessageRecharge : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var recharge: Button? = null

    private var txt_wallet_balance: TextView? = null
    private var txt_message_balance: TextView? = null
    private var inputMessageQuantity: EditText? = null

    private var txt_unit_price: TextView? = null
    private var txt_total_price: TextView? = null
    private var txt_vat_amopunt: TextView? = null
    private var txt_payable_amount: TextView? = null

    private var spinner_purchase_point: Spinner? = null
    private var spinner_message_type: Spinner? = null
    private var selectPurchaseP: String? = null

    private var messageTypeArrayList: ArrayList<MessageType>? = null
    private var purchasePointArrayList: ArrayList<PurchasePoint>? = null

    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_message_recharge)
        toolBarInit()
        initializeViews()
        getPurchasePointDataFromServer()

        recharge = findViewById<Button>(R.id.recharge)
        recharge?.setOnClickListener {

            if (selectPurchasePoint.trim { it <= ' ' }.isEmpty() || selectPurchasePoint == "Select Purchase Point") {
                Toasty.error(applicationContext, "Select Purchase Point", Toast.LENGTH_SHORT, true).show()
            }

            else if (selectMessageType.trim { it <= ' ' }.isEmpty() || selectMessageType == "Select Message Type") {
                Toasty.error(applicationContext, "Select Message Type", Toast.LENGTH_SHORT, true).show()
            }

            else if (inputMessageQuantity == null || inputMessageQuantity?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputMessageQuantity?.error = getString(R.string.err_mssg_messageQty)
                inputMessageQuantity?.let { requestFocus(it) }
            }

            else{
                submitData()
            }
        }

        /*try {

            Thread(Runnable {
                var i = 0;
                while (i < Int.MAX_VALUE) {
                    i++
                }
                this.runOnUiThread {

                    messagettypeSpinnerData()

                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    val messageType = dao?.getMessageTypeAll()
                    txt_unit_price?.setText(messageType?.getSalesPrice().toString())

                    // val totalPrice = messageQuantity.toInt() * messageType?.getSalesPrice()!!
                    val totalPrice = 5 * messageType?.getSalesPrice()!!
                    Log.d("Total Price :", totalPrice.toString())
                    txt_total_price?.setText(totalPrice.toString())

                    txt_vat_amopunt?.setText(messageType.getPercentVat().toString())

                    val totalPayable = totalPrice + messageType.getPercentVat()!!
                    Log.d("Total Payable :", totalPayable.toString())
                    txt_payable_amount?.setText(totalPayable.toString())

                    val userProfile = dao.getUserProfileDetails()
                    txt_message_balance?.setText(userProfile.getSmsBalance().toString())

                }
            }).start()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }*/
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Message Recharge"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {

        viewDialog = ViewDialog(this)

        inputMessageQuantity = findViewById<View>(R.id.et_message_quantity) as EditText
        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        txt_message_balance = findViewById<View>(R.id.txt_message_balance) as TextView

        txt_unit_price = findViewById<View>(R.id.unit_price_value) as TextView
        txt_total_price = findViewById<View>(R.id.total_price_value) as TextView
        txt_vat_amopunt = findViewById<View>(R.id.vat_amt_value) as TextView
        txt_payable_amount = findViewById<View>(R.id.payable_amt_value) as TextView

        spinner_message_type = findViewById<View>(R.id.spinner_message_type) as Spinner
        spinner_message_type?.onItemSelectedListener = this

        spinner_purchase_point = findViewById<View>(R.id.spinner_purchase_point) as Spinner
        spinner_purchase_point?.onItemSelectedListener = this

        inputMessageQuantity?.addTextChangedListener(MyTextWatcher(inputMessageQuantity!!))

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        val userProfile = dao?.getUserProfileDetails()

        if(walletBalance?.getUserWalletBalance()==null){
            txt_wallet_balance?.text = "00"
        }
        else {
            txt_wallet_balance?.text = String.format("%, .2f",walletBalance.getUserWalletBalance())
        }

        if(userProfile?.getSmsBalance()==null){
            txt_message_balance?.text="00"
        }
        else {
            txt_message_balance?.text = String.format("%, .0f",userProfile.getSmsBalance())
            //txt_message_balance?.text = userProfile.getSmsBalance().toString()
        }
        dao?.close()
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_purchase_point) {
                selectPurchasePoint = spinner_purchase_point?.selectedItem.toString()
                if (selectPurchasePoint == resources.getString(R.string.select_purchase_point)) {
                    selectPurchasePoint = ""
                } else {
                    selectPurchaseP = parent.getItemAtPosition(position).toString()

                    val da = this.let { DAO(it) }
                    da.open()
                    roleID = da.getRoleID(selectPurchasePoint)
                    useRoleAssignID = da.getRoleAssignID(selectPurchasePoint)

                    da.deleteMessageTypeList()
                    getMessageTypeDataFromServer()

                    Log.d("Role ", "ID:" + GeneralProduct01.roleID)
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_message_type) {
                selectMessageType = spinner_message_type?.selectedItem.toString()
                if (selectMessageType == resources.getString(R.string.select_message_type)) {
                    selectMessageType = ""
                } else {
                    selectMessageType = parent.getItemAtPosition(position).toString()
                    Log.d("Message", "Type:" + selectMessageType)

                    val da = this.let { DAO(it) }
                    da.open()
                    productID = da.getProductID(selectMessageType)
                    productUnitPrice = da.getUnitPrice(selectMessageType)

                    if (selectMessageType.trim { it <= ' ' }.isEmpty() || selectMessageType == "Select Message Type") {
                        txt_unit_price?.text = "0.00"
                    }
                    else{
                        txt_unit_price?.text =String.format("%, .2f", productUnitPrice.toDouble())
                    }

                    inputMessageQuantity?.text?.clear()
                    txt_total_price?.text = "0.00"
                    txt_vat_amopunt?.text="0.00"
                    txt_payable_amount?.text="0.00"

                    Log.d("Product", "ID:" + productID)

                    da.close()
                }
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Message Recharge:", "Spinner data")
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun messagettypeSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val messageTypeArrayList = dao?.allMessageTypeData

            val mobileBank = ArrayList<String>()
            mobileBank.add(resources.getString(R.string.select_message_type))
            for (i in messageTypeArrayList?.indices!!) {
                messageTypeArrayList[i].getProductName()?.let { mobileBank.add(it) }
            }

            val sectionAdapter = ArrayAdapter(applicationContext, R.layout.spinner_item, mobileBank)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_message_type?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }

    private fun submitData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()

        header["Authorization"] = "bearer ${SignInActivity.Token}"

        try {

            messageQuantity = inputMessageQuantity?.text.toString()

            val messageRecharge = MessageRecharge(
                messageQuantity.toIntOrNull(),
                ProductInfoDTO(productID.toIntOrNull()),
                ProductPurchaseLogDTO(ProductRoleAssignDTO(3)))

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.messageRechargepostData(messageRecharge, header)

            //calling the api
            call?.enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    Log.d("onResponse", "Message Recharge:" + response.body())

                    if (response.code() == 201) {
                        Log.d("onResponse", "Token:" + response.body())
                        startActivity(Intent(this@BalanceMessageRecharge, MainActivity::class.java))
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        overridePendingTransition(R.anim.right_in, R.anim.right_out)

                        Toasty.success(
                            applicationContext,
                            "Balance Message Recharge Successfully !",
                            Toast.LENGTH_LONG,
                            true
                        ).show()

                    }

                    else if (response.code()== 400) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }
                    else if (response.code()== 409) {
                        Toasty.error(
                            applicationContext,
                            "Duplicate Entry",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else if (response.code()== 500) {
                        Toasty.error(
                            applicationContext,
                            "An Error Occurred",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else  {
                        Toasty.error(
                            applicationContext,
                            "Opps!! Something Wrong",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    viewDialog?.hideDialog()

                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()

                    Toasty.error(
                        applicationContext,
                        "Balance Message Recharge Fail",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun getMessageTypeDataFromServer() {
       // viewDialog?.showDialog()

        val header = java.util.HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getMessageType(header, roleID)

        userCall?.enqueue(object : Callback<MessageType> {

            override fun onResponse(
                call: Call<MessageType>,
                response: Response<MessageType>
            ) {
                try {


                    Log.d("onResponse", "Main :" + response.errorBody().toString())

                    if (response.code() == 302) {

                        val messageTypeInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                        val getData = JSONArray(messageTypeInfo!!)

                        for (i in 0 until getData.length()) {
                            //continue to loop it getting null value
                            if (getData.isNull(i))
                                continue
                            // Getting json object node
                            val c = getData.getJSONObject(i)
                            val c1 = c.getJSONObject("productInfoDTO")

                            // Get the item model
                            val messageTypeList = MessageType()
                            //set the json data in the model
                            messageTypeList.setProductID(c1.getLong("productID"))
                            messageTypeList.setProductName(c1.getString("productName"))

                            Log.d("ProductName", "MessageRecharge :" + c1.getString("productName"))

                            messageTypeArrayList?.add(messageTypeList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_MESSAGE_TYPE + "(productID, productName, salesPrice, percentVat) " +
                                        "VALUES(?, ?, ?, ?)", arrayOf(
                                    c1.getLong("productID").toString(),
                                    c1.getString("productName"),
                                    c1.getString("salesPrice"),
                                    c1.getString("percentVat")
                                )
                            )

                            Log.d("Product ID :", messageTypeList.getProductID()?.toString()!!)
                            Log.d("Product Name :", messageTypeList.getProductName()!!)

                        }
                    }

                    messagettypeSpinnerData()
                    //  viewDialog?.hideDialog()

                }

                catch (e: NullPointerException){
                    e.printStackTrace()
                }
                catch (e:Exception){
                    e.printStackTrace()

                }
            }

            override fun onFailure(call: Call<MessageType>, t: Throwable) {
               // viewDialog?.hideDialog()

                Log.d("onFailure", t.toString())
            }
        })
    }

    private inner class MyTextWatcher(private val view: View?) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            try {
                  messageQuantity = inputMessageQuantity?.text.toString()

                   val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    val messageType = dao?.getMessageTypeAll()

                    // val totalPrice = messageQuantity.toInt() * messageType?.getSalesPrice()!!
                    val totalPrice = messageQuantity.toDouble() * productUnitPrice.toDouble()
                    Log.d("Total Price :", totalPrice.toString())

                    txt_total_price?.text = String.format("%, .2f", totalPrice)

                    val vatCal= messageType?.getPercentVat()?.times(totalPrice)?.div(100)
                    txt_vat_amopunt?.text = String.format("%, .2f", vatCal)

                    val totalPayable = totalPrice + vatCal!!
                    Log.d("Total Payable :", totalPayable.toString())
                    txt_payable_amount?.text = String.format("%, .2f", totalPayable)

                    dao.close()
            }

            catch (e: NumberFormatException){
                e.printStackTrace()
                txt_total_price?.text = "0.00"
                txt_vat_amopunt?.text="0.00"
                txt_payable_amount?.text="0.00"
            }
        }

        override fun afterTextChanged(editable: Editable) {

            try {

            /*    val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                val messageType = dao?.getMessageTypeAll()

                // val totalPrice = messageQuantity.toInt() * messageType?.getSalesPrice()!!
                val totalPrice = messageQuantity.toDouble() * productUnitPrice.toDouble()
                Log.d("Total Price :", totalPrice.toString())
                txt_total_price?.text = totalPrice.toString()

                val vatCal= messageType?.getPercentVat()?.times(totalPrice)?.div(100)
                txt_vat_amopunt?.text = vatCal.toString()

                val totalPayable = totalPrice + vatCal!!
                Log.d("Total Payable :", totalPayable.toString())
                txt_payable_amount?.text = totalPayable.toString()

                dao.close()*/
            }

            catch (e: NumberFormatException){
                e.printStackTrace()
            }
        }
    }


    class HttpRequestMessageType() : AsyncTask<Void, Void, List<MessageType>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): List<MessageType>? {
            try {
                val headers = HttpHeaders()
                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                val httpEntity = HttpEntity<String>(headers)
                val url =
                    ApiClient.BASE_URL + "user/message/types/by/point?roleID=1" // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())
                val res = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    Array<MessageType>::class.java
                )

                val messageType = MessageType()
                messageType.setMessageTypest(res.body.toList())

                Log.d("RestTemplate", res.body.toString())
                Logger.d(res)


                return res.body.toList();

            } catch (e: Exception) {
                Log.e("Message Type", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: List<MessageType>?) {
            super.onPostExecute(result)
            try {
                for (i in result?.indices!!) {

                    val messageType = MessageType()
                    messageType.setProductID(result[i].getProductID()!!)
                    messageType.setProductName(result[i].getProductName()!!)

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_MESSAGE_TYPE + "(productID, productName, salesPrice, percentVat) " +
                                "VALUES(?, ?, ?, ?)", arrayOf(
                            result.get(i).getProductID().toString(),
                            result.get(i).getProductName().toString(),
                            result.get(i).getSalesPrice().toString(),
                            result.get(i).getPercentVat().toString()
                        )
                    )

                    Log.d("Product ID :", messageType.getProductID()?.toString()!!)
                    Log.d("Product Name :", messageType.getProductName()!!)
                }

            } catch (e: NullPointerException) {
                Log.e("MessageType", e.message, e)
            }
        }
    }

    private fun getPurchasePointDataFromServer() {

        viewDialog?.showDialog()

        val header = java.util.HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getPurchasePoint(header)

        userCall?.enqueue(object : Callback<PurchasePoint> {

            override fun onResponse(
                call: Call<PurchasePoint>,
                response: Response<PurchasePoint>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())
                viewDialog?.hideDialog()

                if (response.code() == 302) {

                    val purchasePointInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(purchasePointInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        // Get the item model
                        val purchasePointList = PurchasePoint()
                        //set the json data in the model
                        purchasePointList.setUserRoleAssignID(c.getLong("userRoleAssignID"))
                        purchasePointList.setCoreRoleName(c.getString("coreRoleName"))
                        purchasePointList.setCoreRoleID(c.getLong("coreRoleID"))

                        Log.d("coreRoleName", "General Purchase :" + c.getString("coreRoleName"))

                        purchasePointArrayList?.add(purchasePointList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_ROLE_ASSIGN + "(userRoleAssignID, coreRoleID, coreRoleName) " +
                                    "VALUES(?, ?, ?)", arrayOf(
                                c.getLong("userRoleAssignID").toString(),
                                c.getLong("coreRoleID").toString(),
                                c.getString("coreRoleName")
                            )
                        )

                        Log.d("UserRole ID :", purchasePointList.getCoreRoleID()?.toString()!!)
                        Log.d("UserRole Name :", purchasePointList.getCoreRoleName()!!)
                    }
                }

                purchasePointSpinnerData()

            }

            override fun onFailure(call: Call<PurchasePoint>, t: Throwable) {
                viewDialog?.hideDialog()

                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun purchasePointSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val userRoleArrayList = dao?.allAssignRole
            val purchasePoint = ArrayList<String>()
            purchasePoint.add(resources.getString(R.string.select_purchase_point))
            for (i in userRoleArrayList?.indices!!) {
                userRoleArrayList[i].getCoreRoleName()?.let { purchasePoint.add(it) }
            }

            val sectionAdapter =
                ArrayAdapter(
                    applicationContext,
                    R.layout.spinner_item,
                    purchasePoint
                )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_purchase_point?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }


    companion object {
        private val TAG = "BalanceMessageRecharge"
        var selectMessageType = ""
        var productID = ""
        var productUnitPrice = ""
        var messageQuantity = ""

        var selectPurchasePoint = ""
        var roleID = ""
        var useRoleAssignID = ""
        var userProductID = ""
    }

}
