package com.netizen.netiworld.activity.balanceDepositBkash


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.balanceDepositBank.BalanceWalletDeposit03
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.utils.DecimalDigitsInputFilter
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class BalanceWalletDepositMobile02 : AppCompatActivity(), View.OnClickListener{

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next02: ImageButton? = null
    var previous: ImageButton? = null
    private var txt_wallet_balance: TextView? = null

    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null

    private var inputDepositAmount: EditText? = null
    private var inputDepositDate: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_mobile_02)
        toolBarInit()
        initializeViews()
        ProfileActivity().getUserProfileInfoData()

        next02 = findViewById<ImageButton>(R.id.next02)
        previous = findViewById<ImageButton>(R.id.previous)

        previous?.setOnClickListener {
            depositAmount = inputDepositAmount?.text.toString()
            depositDate = inputDepositDate?.text.toString()

            val intent = Intent(this, BalanceWalletDepositMobile01::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.right_in, R.anim.right_out)
            finish()
        }

        next02?.setOnClickListener {

            depositAmount = inputDepositAmount?.text.toString()
            depositDate = inputDepositDate?.text.toString()

             if (inputDepositAmount == null || inputDepositAmount?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputDepositAmount?.error = getString(R.string.err_mssg_amount)
                inputDepositAmount?.let { requestFocus(it) }
            }

             else if(inputDepositAmount?.text.toString()=="0") {
                 inputDepositAmount?.error = "Deposit amount can't be 0"
                 inputDepositAmount?.let { requestFocus(it) }
             }


             else if (inputDepositDate == null || inputDepositDate?.text.toString().trim { it <= ' ' }.isEmpty()) {
                 Toasty.error(applicationContext, "Select Deposit Date", Toast.LENGTH_SHORT, true).show()
                // inputDepositDate?.error = getString(R.string.err_mssg_date)
                //inputDepositDate?.let { requestFocus(it) }
            }

           else if(BalanceWalletDepositMobile01.localCategoryDefaultCode == "T10002") {

                startActivity(Intent(this@BalanceWalletDepositMobile02, BalanceWalletDepositMobile03::class.java))
                 overridePendingTransition(R.anim.left_in, R.anim.left_out)

             }

            else if(BalanceWalletDepositMobile01.localCategoryDefaultCode == "T10001"){
                startActivity(Intent(this@BalanceWalletDepositMobile02, BalanceWalletDepositBank03::class.java))
                 overridePendingTransition(R.anim.left_in, R.anim.left_out)
             }

        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar!!.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if(BalanceWalletDepositMobile01.localCategoryDefaultCode == "T10002") {
            toolbarTitle?.text = "Balance Wallet Deposit(Mobile Banking)"
        }
        else {
            toolbarTitle?.text = "Balance Wallet Deposit(Bank)"
        }

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {
        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        inputDepositAmount = findViewById<View>(R.id.et_deposit_amount) as EditText
        inputDepositDate = findViewById<View>(R.id.et_deposit_date) as EditText

        dateFormatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()) //yyyy-MM-dd previous format
        inputDepositDate?.inputType = InputType.TYPE_NULL
        inputDepositDate?.setOnClickListener(this)

        setDateTimeField()

        inputDepositAmount?.filters = arrayOf(DecimalDigitsInputFilter())
        inputDepositAmount?.setText(depositAmount)
        inputDepositDate?.setText(depositDate)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f",walletBalance?.getUserWalletBalance())
        dao?.close()

    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletDepositMobile01::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    override fun onClick(v: View) {
        if (v === inputDepositDate) {
            fromDatePickerDialog?.show()
        }
    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                inputDepositDate!!.setText(dateFormatter!!.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(
                Calendar.DAY_OF_MONTH)
        )

        fromDatePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()

    }


   /* override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(keyCode, event)
            return true
        }
        return false
    }*/

    companion object {
        private val TAG = "BalanceWalletDepositMobile02"
        var depositAmount: String? = null
        var depositDate: String? = null
    }

}
