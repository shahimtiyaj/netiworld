package com.netizen.netiworld.activity.purchaseGeneralProduct

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.*
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class GeneralProduct02 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var purchase: Button? = null

    private var txt_wallet_balance: TextView? = null
    private var txt_product_name: TextView? = null
    private var txt_unit_price: TextView? = null
    private var txt_total_price: TextView? = null
    private var txt_vat_amopunt: TextView? = null
    private var txt_payable_amount: TextView? = null

    private var inputProductQuantity: EditText? = null
    private var productQuantity: String? = null

    var viewDialog: ViewDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchase_general_product_02)
        toolBarInit()
        initializeViews()

        purchase = findViewById<Button>(R.id.purchase)
        purchase?.setOnClickListener {
            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val walletBalance = dao?.getUserProfileDetails()

            if (inputProductQuantity == null || inputProductQuantity?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputProductQuantity?.error = getString(R.string.err_mssg_productQty)
                inputProductQuantity?.let { requestFocus(it) }
            }

            /* else if (String.format("%.2f", totalPayable) > String.format("%.2f", walletBalance?.getUserWalletBalance())) {
                   Toasty.error( applicationContext,
                       "Insufficient Wallet Balance",
                       Toast.LENGTH_LONG,
                       true
                   ).show()
               }*/
            else {
                submitGeneralProductData()
            }
        }

    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Purchase General Product"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this)

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        txt_product_name = findViewById<View>(R.id.product_name) as TextView
        txt_product_name?.text = GeneralProduct01.selectPurchasePoint
        Log.d("General Product:", "Purchase Point:" + GeneralProduct01.selectPurchasePoint)

        txt_unit_price = findViewById<View>(R.id.unit_price_value) as TextView
        txt_total_price = findViewById<View>(R.id.total_price_value) as TextView
        txt_vat_amopunt = findViewById<View>(R.id.vat_amt_value) as TextView
        txt_payable_amount = findViewById<View>(R.id.payable_amt_value) as TextView

        inputProductQuantity = findViewById<View>(R.id.et_product_quantity) as EditText

        inputProductQuantity?.addTextChangedListener(MyTextWatcher(inputProductQuantity!!))

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f", walletBalance?.getUserWalletBalance())
        txt_unit_price?.text = String.format("%, .2f", GeneralProduct01.unitPrice.toDouble())

        dao?.close()

    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            try {
                productQuantity = inputProductQuantity?.text.toString()

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()

                /*if (productQuantity == "." || productQuantity == " ") {
                    txt_unit_price?.text = String.format("%, .2f", GeneralProduct01.unitPrice.toDouble())
                }*/

                 if (inputProductQuantity?.text.toString().startsWith("0")) {
                    inputProductQuantity?.setText(inputProductQuantity?.text.toString().substring(1))
                }

               /* else {
                    txt_unit_price?.text = String.format("%, .2f", GeneralProduct01.unitPrice.toDouble())
                } */

                totalPrice = productQuantity!!.toDouble() * GeneralProduct01.unitPrice.toDouble()
                Log.d("Total Price :", totalPrice.toString())
                txt_total_price?.text = String.format("%, .2f", totalPrice)

                vat_amopunt = GeneralProduct01.vatPercentage.toDouble().times(totalPrice).div(100)
                txt_vat_amopunt?.text = String.format("%, .2f", vat_amopunt)

                totalPayable = totalPrice + vat_amopunt
                Log.d("Total Payable :", totalPayable.toString())
                txt_payable_amount?.text = String.format("%, .2f", totalPayable)

                dao?.close()

            } catch (e: NumberFormatException) {
                e.printStackTrace()
                //txt_unit_price?.text = "0.00"
                txt_total_price?.text = "0.00"
                txt_vat_amopunt?.text = "0.00"
                txt_payable_amount?.text = "0.00"
            } catch (i: IllegalFormatConversionException) {
                i.printStackTrace()
            }
        }

        override fun afterTextChanged(editable: Editable) {


        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, GeneralProduct01::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }


    private fun submitGeneralProductData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        try {

            val generalProduct = GeneralProductPost(
                productQuantity?.toIntOrNull(),
                GeneralProduct01.unitPrice.toDouble(),
                totalPrice,
                vat_amopunt,
                totalPayable,
                _ProductInfoDTO(
                    GeneralProduct01.userProductID.toIntOrNull()
                ),
                _ProductRoleAssignDTO(
                    GeneralProduct01.productRolesAssign.toIntOrNull()
                )
            )

            Log.d("General Product:", "JSON:" + generalProduct.toString())


            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.generalProductPostData(generalProduct, header)

            //calling the api
            call?.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Log.d("onResponse", "Message Recharge:" + response.body())

                    if (response.code() == 201) {
                        startActivity(Intent(this@GeneralProduct02, MainActivity::class.java))
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        overridePendingTransition(R.anim.right_in, R.anim.right_out)

                        Log.d("onResponse", "Token:" + response.body())
                        Toasty.success(
                            applicationContext,
                            "General Product Purchase Successfully !!",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    } else if (response.code() == 400 || response.code() == 404) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    } else if (response.code() == 409) {
                        Toasty.error(
                            applicationContext,
                            "Duplicate Entry",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    } else if (response.code() == 500) {
                        Toasty.error(
                            applicationContext,
                            "An Error Occurred",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    } else {
                        Toasty.error(
                            applicationContext,
                            "Opps!! Something Wrong",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    viewDialog?.hideDialog()
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()
                    Toasty.error(
                        applicationContext,
                        "General Product Purchase Fail !!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }


    }

    companion object {
        private val TAG = "BalanceMessageRecharge"
        var selectPurchasePoint = ""
        var totalPrice = 0.0
        var totalPayable = 0.0
        var unit_price = 0.0
        var vat_amopunt = 0.0
    }
}
