package com.netizen.netiworld.activity.balanceTransfer

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.WalletTransfer
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.DecimalDigitsInputFilter
import com.netizen.netiworld.utils.ViewDialog
import com.orhanobut.logger.Logger
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BalanceWalletTransfer01 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next01: ImageButton? = null
    var serachNetiID: ImageButton? = null
    private var txt_wallet_balance: TextView? = null
    private var txt_et_name: TextView? = null
    private var txt_et_mobile_no: TextView? = null

    private var inputNetiID: EditText? = null
    private var inputTransferAmount: EditText? = null
    private var inputNote: EditText? = null
    var viewDialog: ViewDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_transfer_01)
        toolBarInit()
        initializeViews()

        ProfileActivity().getUserProfileInfoData()

        next01 = findViewById<ImageButton>(R.id.next01)
        next01?.setOnClickListener {
            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val walletBalance = dao?.getUserProfileDetails()

            val customID= walletBalance?.getNetiID()

            txt_wallet_balance?.text = String.format("%.2f", walletBalance?.getUserWalletBalance())
            dao?.close()

            if (inputNetiID == null || inputNetiID?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputNetiID?.error = getString(R.string.err_mssg_neti)
                inputNetiID?.let { requestFocus(it) }
            }

            else if (inputNetiID?.text.toString() == customID.toString()) {
                inputNetiID?.error = "You can't transfer balance to your own account"
                inputNetiID?.let { requestFocus(it) }
            }

            else if (inputTransferAmount == null || inputTransferAmount?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputTransferAmount?.error = getString(R.string.err_mssg_transfer_Amt)
                inputTransferAmount?.let { requestFocus(it) }
            }

            else if(inputTransferAmount?.text.toString()=="0") {
                inputTransferAmount?.error = "Transfer amount can't be 0"
                inputTransferAmount?.let { requestFocus(it) } }

              else if (walletBalance?.getSmsBalance()!!.toDouble()<=0.0 || walletBalance.getSmsBalance()!!.toDouble()<=0 || walletBalance.getSmsBalance()==null) {
                Toasty.error(applicationContext, "Insufficient Message Balance", Toast.LENGTH_SHORT, true).show()
            }

            else if(txt_et_name?.text.toString().isEmpty()){ Toasty.error(applicationContext, "Please Search Neti ID", Toast.LENGTH_LONG, true).show() }

          /*  else if (inputTransferAmount?.text.toString() > String.format("%.2f", walletBalance?.getUserWalletBalance())) {
                inputTransferAmount?.error = getString(R.string.err_balance)
                inputTransferAmount?.let { requestFocus(it) }
            }*/

            else if (inputNote == null || inputNote?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputNote?.error = getString(R.string.err_mssg_note)
                inputNote?.let { requestFocus(it) }
            }
            else {
                submitWalletTransferData()
            }
        }

        serachNetiID = findViewById<ImageButton>(R.id.serach_neti_id)
        serachNetiID?.setOnClickListener {
            if (inputNetiID == null || inputNetiID?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputNetiID?.error = getString(R.string.err_mssg_neti)
            }
            else {
                getWalletTransferData()

            }
        }
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Transfer"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this);

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        txt_et_name = findViewById<View>(R.id.et_name) as TextView
        txt_et_mobile_no = findViewById<View>(R.id.et_mobile_no) as TextView

        inputNetiID = findViewById<View>(R.id.et_neti_id) as EditText
        inputTransferAmount = findViewById<View>(R.id.et_transfer_amt) as EditText
        inputNote = findViewById<View>(R.id.et_note) as EditText

        inputTransferAmount?.filters = arrayOf(DecimalDigitsInputFilter())
        inputNetiID?.addTextChangedListener(MyTextWatcher(inputNetiID!!))

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f",walletBalance?.getUserWalletBalance())

        dao?.close()
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {


        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            next01?.isEnabled = false
            next01?.setBackgroundColor(Color.GRAY)// From android.graphics.Color
            next01?.setBackgroundResource(R.drawable.disable_color)
        }

        override fun afterTextChanged(editable: Editable) {
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    private fun getWalletTransferData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        Log.d("Custom Neti Id: ", customNetiID)

        customNetiID = inputNetiID?.text.toString()

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getDataSearchByNetiID(header, customNetiID)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Main Wallet Transfer response :" + response.body().toString())

                if (response.code() == 302) {

                    val walletTransfer = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val jsonObj = JSONObject(walletTransfer!!)

                    fullName = jsonObj.getString("fullName")
                    txt_et_name?.text = fullName

                    PhoneNo = jsonObj.getString("basicMobile")
                    netiMainID = jsonObj.getString("netiID")
                    //txt_et_mobile_no?.text = PhoneNo
                    val chunks = PhoneNo.chunked(3)
                    val c1=chunks[1]
                    val c3=chunks[3]
                    PhoneNo="0"+c1.substring(0,2)+ "*****"+c3
                    txt_et_mobile_no?.text =PhoneNo

                    Log.d("onResponse", "Wallet Transfer :$walletTransfer")
                    next01?.isEnabled = true
                    //next01?.setBackgroundColor(Color.parseColor("#2196F3"))
                    next01?.setBackgroundResource(R.drawable.login_btn_back)

                }
                else {
                    txt_et_name?.text=""
                    txt_et_mobile_no?.text=""
                    Toasty.error(
                        applicationContext,
                        "User doesn't exist",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

            }
        })
    }

    private fun submitWalletTransferData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"
        try {

            customNetiID = inputNetiID?.text.toString().trim()
            TransferAmount = inputTransferAmount?.text.toString().trim()
            note = inputNote?.text.toString().trim()

            val balanceWalletTransfer = WalletTransfer(TransferAmount, WalletTransfer.Receiver(customNetiID.toLongOrNull()), note)

            Log.d("Transfer wallet:", "JSON:" + balanceWalletTransfer)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.WalletBalanceTransfer(balanceWalletTransfer, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    Log.d("onResponse", "Balance Wallet post:" + response.body())
                    Logger.d("Balance Wallet post:", response.body().toString())

                    if (response.code() == 200) {
                        Log.d("onResponse", "Success:" + response.body())
                        startActivity(Intent(this@BalanceWalletTransfer01, BalanceWalletTransferOtp02::class.java))
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        overridePendingTransition(R.anim.left_in, R.anim.left_out)

                        Toasty.success(applicationContext, "6 Digit OTP code has sent !!", Toast.LENGTH_LONG, true).show()
                    }
                    else if (response.code()== 400) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }
                    else if (response.code()== 409) {
                        Toasty.error(
                            applicationContext,
                            "Duplicate Entry",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else if (response.code()== 500) {
                        Toasty.error(
                            applicationContext,
                            "An Error Occurred",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else  {
                        Toasty.error(
                            applicationContext,
                            "Opps!! Something Wrong",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }
                    viewDialog?.hideDialog()
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()

                    Toasty.error(
                        applicationContext,
                        "Balance Wallet Check Fail !!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = "BalanceWalletTransfer"
        var customNetiID = ""
        var TransferAmount = ""
        var note = ""
        var fullName = ""
        var PhoneNo= ""
        var netiMainID= ""
    }
}
