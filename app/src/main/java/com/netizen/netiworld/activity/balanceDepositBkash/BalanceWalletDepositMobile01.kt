package com.netizen.netiworld.activity.balanceDepositBkash


import am.appwise.components.ni.NoInternetDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.utils.CustomSearchableSpinner
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.MobileBank
import com.netizen.netiworld.model.MobileBankAcNo
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.Logger.addLogAdapter
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.util.*


class BalanceWalletDepositMobile01 : AppCompatActivity(), AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next01: ImageButton? = null
    private var txt_wallet_balance: TextView? = null

    private var spinner_mobile_bank: CustomSearchableSpinner? = null
    private var spinner_account_number: CustomSearchableSpinner? = null
    private var selectMobile: String? = null
    private var selectMobileAcNo: String? = null
    private var coreBankAccArrayList: ArrayList<MobileBankAcNo>? = null
    private var coreBankNameArrayList: ArrayList<MobileBank>? = null

    private val mLastSpinnerPosition = 0

    var viewDialog: ViewDialog? = null
    var noInternetDialog: NoInternetDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_mobile_01)

        addLogAdapter(AndroidLogAdapter())

        toolBarInit()

        initializeViews()
        viewDialog = ViewDialog(this)

        ProfileActivity().getUserProfileInfoData()
        BankAccountName()

        /*if (!AppController.instance?.isNetworkAvailable()!!) {
         noInternetDialog =  NoInternetDialog.Builder(this).build()
        }
        else{
            ProfileActivity().getUserProfileInfoData()
            BankAccountName()
        }*/

        next01 = findViewById<ImageButton>(R.id.next01)
        next01?.setOnClickListener {
            if (selectMobile == null || selectMobile?.trim { it <= ' ' }!!.isEmpty() || selectMobile == "Select Bank Name") {
                Toasty.error(applicationContext, "Select Bank Name", Toast.LENGTH_SHORT, true)
                    .show()
            } else if (selectMobileAcNo == null || selectMobileAcNo?.trim { it <= ' ' }!!.isEmpty() || selectMobileAcNo == "Select Account Number") {
                Toasty.error(applicationContext, "Select Account Number", Toast.LENGTH_SHORT, true)
                    .show()
            } else {

                startActivity(Intent(this@BalanceWalletDepositMobile01, BalanceWalletDepositMobile02::class.java))
                overridePendingTransition(R.anim.left_in, R.anim.left_out)

            }
        }
    }

    /* override fun onDestroy() {
         super.onDestroy()
         noInternetDialog?.onDestroy()
     }*/

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Deposit"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun initializeViews() {

        coreBankAccArrayList = ArrayList()
        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        spinner_mobile_bank = findViewById<View>(R.id.spinner_bank_name) as CustomSearchableSpinner
        spinner_account_number =
            findViewById<View>(R.id.spinner_account_number) as CustomSearchableSpinner
        spinner_mobile_bank?.onItemSelectedListener = this
        spinner_account_number?.onItemSelectedListener = this

        spinner_mobile_bank?.setTitle("Search Bank")

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        //txt_wallet_balance?.text = String.format("%.2f",walletBalance?.getUserWalletBalance())

        txt_wallet_balance?.text = String.format("%, .2f", walletBalance?.getUserWalletBalance())

        /*val formatter =  DecimalFormat("#,###,###")
        val yourFormattedString = formatter.format(walletBalance)
        txt_wallet_balance?.text = String.format("%.2f",yourFormattedString)*/

        dao?.close()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }

  /*  override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(keyCode, event)
            return true
        }
        return false
    }*/

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            /*  if (mLastSpinnerPosition == position) {
                  return; //do nothing
              } else {*/

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_bank_name) {
                selectMobile = spinner_mobile_bank?.selectedItem.toString()
                if (selectMobile == resources.getString(R.string.select_bank_name)) {
                    selectMobile = ""
                } else {
                    selectMobile = parent.getItemAtPosition(position).toString()
                    val da = this.let { DAO(it) }
                    da.open()
                    localBankCategoryID = da.getCoreCategoryID(selectMobile!!)
                    Log.d("Category ID: ", localBankCategoryID)
                    localCategoryDefaultCode = da.getCategoryDefaultCode(selectMobile!!)
                    Log.d("Category Default Code: ", localCategoryDefaultCode)

                    da.deleteAccountNoList()

                    BankAccountNoInformation()

                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_account_number) {
                selectMobileAcNo = spinner_account_number?.selectedItem.toString()
                if (selectMobileAcNo == resources.getString(R.string.select_mobile_bank_acc)) {
                    selectMobileAcNo = ""
                } else {

                    selectMobileAcNo = parent.getItemAtPosition(position).toString()

                    val da = this.let { DAO(it) }
                    da.open()
                    localCoreBankAccId = da.getCoreBankAccID(selectMobileAcNo!!)
                    Log.d("Core Bank Acc ID: ", localCoreBankAccId)

                    da.close()
                }
            }

        }
        //  }

        catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Balance Deposit:", "Spinner data")
        }
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun mobileBankSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val allMobileBankDataList = dao?.allMobileBankData

            val mobileBank = ArrayList<String>()
            mobileBank.add(resources.getString(R.string.select_bank_name))
            for (i in allMobileBankDataList?.indices!!) {
                allMobileBankDataList[i].getCategoryName()?.let { mobileBank.add(it) }
            }

            val sectionAdapter =
                ArrayAdapter(applicationContext, R.layout.spinner_item, mobileBank)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_mobile_bank?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }

    private fun mobileBankAccSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val allMobileBankAccDataList = dao?.allMobileBankAccData

            val mobileAccBank = ArrayList<String>()
            mobileAccBank.add(resources.getString(R.string.select_mobile_bank_acc))
            for (i in allMobileBankAccDataList?.indices!!) {
                allMobileBankAccDataList[i].getAccShortName()?.let { mobileAccBank.add(it) }
            }

            val mobileAccAdapter =
                ArrayAdapter(
                    applicationContext,
                    R.layout.spinner_item,
                    mobileAccBank
                )
            mobileAccAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_account_number?.adapter = mobileAccAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }

    fun BankAccountName() {

        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getAccountNameInfo(header)

        userCall?.enqueue(object : Callback<MobileBank> {

            override fun onResponse(
                call: Call<MobileBank>,
                response: Response<MobileBank>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val bankAccountNameInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(bankAccountNameInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        val c1 = c.getJSONObject("parentTypeInfoDTO")
                        val categoryDefaultCode = c1.getString("categoryDefaultCode")

                        // Get the item model
                        val bankNameList = MobileBank()
                        //set the json data in the model
                        bankNameList.setCoreCategoryID(c.getInt("coreCategoryID"))
                        bankNameList.setCategoryName(c.getString("categoryName"))

                        Log.d("CategoryName", "BankAccNameInfo :" + c.getString("categoryName"))

                        coreBankNameArrayList?.add(bankNameList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_MOBILE_BANK + "(coreCategoryID, categoryName, categoryDefaultCode) " +
                                    "VALUES(?, ?, ?)", arrayOf(
                                c.getInt("coreCategoryID").toString(),
                                c.getString("categoryName"),
                                categoryDefaultCode
                            )
                        )
                    }
                }

                mobileBankSpinnerData()
                viewDialog?.hideDialog()

            }


            override fun onFailure(call: Call<MobileBank>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

            }
        })

    }


    private fun BankAccountNoInformation() {
        //viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getAccountNoInfo(header, localBankCategoryID)

        userCall?.enqueue(object : Callback<MobileBankAcNo> {

            override fun onResponse(
                call: Call<MobileBankAcNo>,
                response: Response<MobileBankAcNo>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val bankAccountInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(bankAccountInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val bankAccIdList = MobileBankAcNo()
                        //set the json data in the model
                        bankAccIdList.setCoreBankAccountId(c.getInt("coreBankAccId"))
                        bankAccIdList.setAccShortName(c.getString("accShortName"))
                        Log.d(
                            "coreBankAccId",
                            "BankAccountNoInformation :" + c.getInt("coreBankAccId")
                        )

                        coreBankAccArrayList?.add(bankAccIdList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_BANK_ACC_NUMBER + "(coreBankAccId, accShortName) " +
                                    "VALUES(?, ?)",
                            arrayOf(
                                c.getInt("coreBankAccId").toString(),
                                c.getString("accShortName")
                            )
                        )
                    }
                }
                mobileBankAccSpinnerData()
                //viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<MobileBankAcNo>, t: Throwable) {
                Log.d("onFailure", t.toString())
                //viewDialog?.hideDialog()

            }
        })
    }


    class HttpRequestBankAccountName() : AsyncTask<Void, Void, List<MobileBank>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): List<MobileBank>? {
            try {
                val headers = HttpHeaders()
                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                val httpEntity = HttpEntity<String>(headers)
                val url =
                    ApiClient.BASE_URL + "user/category/by/type/2nd_parent_type?typeDefaultCode=T100" // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())
                val res = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    Array<MobileBank>::class.java
                )

                Log.d("RestTemplate", res.body.toString())
                Logger.d(res)

                return res.body.toList();

            } catch (e: Exception) {
                Log.e("MainActivity", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: List<MobileBank>?) {
            super.onPostExecute(result)

            try {

                for (i in result?.indices!!) {
                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_MOBILE_BANK + "(coreCategoryID, categoryName) " +
                                "VALUES(?, ?)", arrayOf(
                            result.get(i).getCoreCategoryID().toString(),
                            result.get(i).getCategoryName().toString()
                        )
                    )

                    Logger.d(result.get(i).getCoreCategoryID().toString())
                }

                // Toast.makeText(AppController.context, "M. Bank Name: " + result?.getCategoryName().toString(), Toast.LENGTH_LONG).show()

            } catch (e: NullPointerException) {
                Log.e("BalanceWalletDe_01", e.message, e)
            }
        }
    }

    class HttpRequestBankAccountNo() : AsyncTask<Void, Void, List<MobileBankAcNo>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): List<MobileBankAcNo>? {
            try {
                val headers = HttpHeaders()
                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                val httpEntity = HttpEntity<String>(headers)
                val url =
                    ApiClient.BASE_URL + "user/account/by/corebankid?coreBankID=71" // the  url from where to fetch data(json)
                // val url = ApiClient.BASE_URL + "user/account/by/corebankid?coreBankID="+localBankCategoryID // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())

                val res = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    Array<MobileBankAcNo>::class.java
                )

                Log.d("RestTemplate", res.body.toString())
                Log.d("ACCOUNT NO. URL: ", url)
                Logger.d(res)

                return res.body.toList()

            } catch (e: Exception) {
                Log.e("MainActivity", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: List<MobileBankAcNo>?) {
            super.onPostExecute(result)

            try {

                for (i in result?.indices!!) {

                    val mobileBankAcNo = MobileBankAcNo()
                    mobileBankAcNo.setCoreBankAccountId(result[i].getCoreBankAccountId()!!)
                    mobileBankAcNo.setAccShortName(result[i].getAccShortName()!!)

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_BANK_ACC_NUMBER + "(coreBankAccId, accShortName) " +
                                "VALUES(?, ?)", arrayOf(
                            result[i].getCoreBankAccountId().toString(),
                            result[i].getAccShortName().toString()
                        )
                    )

                    Logger.d(result.get(i).getCoreBankAccountId().toString())
                }

                // Toast.makeText(AppController.context, "M. Bank Name: " + result?.getCategoryName().toString(), Toast.LENGTH_LONG).show()

            } catch (e: NullPointerException) {
                Log.e("BalanceWalletDe_01", e.message, e)
            }
        }
    }

    companion object {
        private val TAG = "FragmentTakeAtdSelection"
        var localBankCategoryID = ""
        var localCoreBankAccId = ""
        var localCategoryDefaultCode = ""
    }
}
