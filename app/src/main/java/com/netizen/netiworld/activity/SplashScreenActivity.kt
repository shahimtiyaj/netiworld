package com.netizen.netiworld.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatDelegate
import com.netizen.netiworld.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFullScreenWindow()
        setContentView(R.layout.activity_splash_screen)
        viewInitialization()
        splashThread()
    }


    private fun requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    private fun viewInitialization() {
        val imageView = findViewById<View>(R.id.imageView)
        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.translate)

        imageView.startAnimation(anim)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

    }

    private fun splashThread() {
        val timer = object : Thread() {
            override fun run() {
                try {

                    sleep(2000)
                    val intent = Intent(applicationContext, SignInActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.left_in, R.anim.left_out)
                    finish()

                    super.run()
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        timer.start()
    }
}
