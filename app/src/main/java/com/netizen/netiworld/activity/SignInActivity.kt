package com.netizen.netiworld.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.android.volley.*
import com.android.volley.Response.ErrorListener
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.netizen.eduman.network.CloudRequest
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.PostResponse
import com.netizen.netiworld.network.ApiRequest
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.JWTUtils
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class SignInActivity : AppCompatActivity() {
    private val TAG = "LoginActivity"
    var toolbar: Toolbar? = null
    var name: TextInputEditText? = null
    var password: TextInputEditText? = null
    var checkBox: CheckBox? = null
    //var signin: ImageButton? = null
    var signup: Button? = null
    var viewDialog: ViewDialog? = null
    var signin: Button? = null

    internal var inputLayoutUserName: TextInputLayout? = null
    internal var inputLayoutUserPass: TextInputLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_in_layout)

        setSupportActionBar(toolbar)
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        viewDialog = ViewDialog(this)

        name = findViewById<TextInputEditText>(R.id.name)
        password = findViewById<TextInputEditText>(R.id.password)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val userLogin = dao?.getUserLoginDetails()
        name?.setText(userLogin?.getuserName())

        inputLayoutUserName = findViewById(R.id.etUsernameLayout)
        inputLayoutUserPass = findViewById(R.id.etPasswordLayout)
        name?.addTextChangedListener(MyTextWatcher(name!!))
        password?.addTextChangedListener(MyTextWatcher(password!!))

        signin = findViewById<Button>(R.id.signin)

        // signup = findViewById<Button>(R.id.signup)

        signin?.setOnClickListener {

            if (!AppController.instance?.isNetworkAvailable()!!) {
                Toast.makeText(applicationContext, "No Internet Connection", Toast.LENGTH_LONG)
                    .show()
            } else if (name?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputLayoutUserName?.setError(getString(R.string.err_msg_name))
                requestFocus(name!!)
            } else if (password?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputLayoutUserPass?.error = getString(R.string.err_msg_pass)
                requestFocus(inputLayoutUserPass!!)
            } else {
                sentLoginInformation()
            }
        }

        /* signup?.setOnClickListener {
             Toast.makeText(applicationContext, "Sign Un Button Clicked", Toast.LENGTH_LONG).show()
         }*/
    }

    private fun sentLoginInformation() {
        if (!validateName()) {
            return
        }
        if (!validatePassword()) {
            return
        }

        try {

            submitData()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun submitData() {

        viewDialog?.showDialog()

        val inputName = name?.text.toString()
        val pass = password?.text.toString()

        val params = HashMap<String?, String?>()
        params["username"] = "test"
        params["password"] = "test"
        params["grant_type"] = "password"

        val header = HashMap<String?, String?>()
        header["Authorization"] = "Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ="
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["NZUSER"] = "$inputName:$pass:password"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postData(params, header)

        //calling the api
        call?.enqueue(object : Callback<PostResponse> {
            override fun onResponse(call: Call<PostResponse>, response: Response<PostResponse>) {
                Log.d("onResponse", "Token:" + response.body()?.getToken())

                Token = response.body()?.getToken().toString()

                if (response.code() == 200) {

                    val separateBody = response.body()?.getToken()?.split("\\.".toRegex())
                        ?.dropLastWhile { it.isEmpty() }?.toTypedArray()

                    val header = separateBody?.get(0)?.let { JWTUtils.getJson(it) }
                    val Body = separateBody?.get(1)?.let { JWTUtils.getJson(it) }

                    val jsonObject = JSONObject(Body)
                    val name = jsonObject.getString("user_name")
                    val tokens = StringTokenizer(name, "@")
                    val username = tokens.nextToken()
                    val natiid = tokens.nextToken()

                    Log.d("JSON OBJECT HEADER", "Header Data: $header")
                    Log.d("JSON OBJECT BODY", "Body Data: $Body")
                    Log.d("User Name", "Name & ID: $name")
                    Log.d("User Name", "Name : $username")
                    Log.d("Neti ID", "ID : $natiid")

                    //Insert user login data into local database for  showing data into offline
                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_LOGIN + "(netiId, userName, password) " +
                                "VALUES(?, ?, ?)", arrayOf(natiid, inputName, pass)
                    )


                    val getAuthoritiesData = jsonObject.getJSONArray("authorities")

                    var user = ""

                    for (i in 0 until getAuthoritiesData.length()) {
                        //continue to loop it getting null value
                        if (getAuthoritiesData.isNull(i))
                            continue
                        // Getting json object node
                        user = getAuthoritiesData.getString(i)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_ROLE + "(RoleType) " +
                                    "VALUES(?)", arrayOf(
                                user
                            )
                        )
                    }

                    startActivity(Intent(this@SignInActivity, MainActivity::class.java))
                    overridePendingTransition(R.anim.left_in, R.anim.left_out)
                    finish()

                    Toasty.success(
                        applicationContext,
                        "Login Successfully!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()

                    finish()

                }

                else if (response.code() == 400){

                    Toasty.error(
                        applicationContext,
                        "Invalid Username or Password",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                else{
                    Toasty.error(
                        applicationContext,
                        "Oops! Something wrong",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<PostResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

                Toasty.error(
                    applicationContext,
                    "Credential doesn't match",
                    Toast.LENGTH_LONG,
                    true
                ).show()
            }
        })
    }


    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    fun loginToServer() {

        val hitURL = "http://192.168.31.37:9000/oauth/token"
        val params = HashMap<String?, String?>()
        params["username"] = "a54544"
        params["password"] = "1235645"
        params["grant_type"] = "gfgfg"

        //JSON Post Request --------------------------------
        val postRequest = object : ApiRequest(
            Method.POST,
            hitURL,
            params,
            com.android.volley.Response.Listener { response ->
                Log.d(TAG, "Server Response: $response")
                try {
                    val acesstoken = response.getString("access_token")
                    startActivity(Intent(this@SignInActivity, ProfileActivity::class.java))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            ErrorListener { error ->
                error.printStackTrace()
                VolleyLog.d(TAG, "Error: " + error.message)

                when (error) {
                    is NetworkError -> Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is ServerError -> Toast.makeText(
                        applicationContext,
                        "The server could not be found.!",
                        Toast.LENGTH_SHORT
                    ).show()
                    is AuthFailureError -> Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is ParseError -> Toast.makeText(
                        applicationContext,
                        "Parse Error !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is TimeoutError -> Toasty.error(
                        applicationContext,
                        "Connection TimeOut !",
                        Toast.LENGTH_SHORT,
                        true
                    ).show()
                    else -> Toasty.error(
                        applicationContext,
                        "Login credentials not matching ",
                        Toast.LENGTH_SHORT,
                        true
                    )
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ="
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["NZUSER"] = "absiddik:123:password"

                return headers
            }
        }
        // Volley socket time its use for loading huge amount data

        val socketTimeout = 100000//100000 milliseconds = 1 minute
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {

            when (view.id) {
                R.id.name -> validateName()
                R.id.password -> validatePassword()
            }
        }
    }

    private fun validateName(): Boolean {
        if (name?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutUserName?.setError(getString(R.string.err_msg_name))
            requestFocus(name!!)
            return false
        } else {
            inputLayoutUserName?.setErrorEnabled(false)
        }

        return true
    }

    private fun validatePassword(): Boolean {
        if (password?.getText()!!.toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutUserPass?.setError(getString(R.string.err_msg_pass))
            requestFocus(inputLayoutUserPass!!)
            return false
        } else {
            inputLayoutUserPass?.setErrorEnabled(false)
        }

        return true
    }

    companion object {
        var BaseUrl = " "
        var AppId = " "
        var Token = ""

    }
}
