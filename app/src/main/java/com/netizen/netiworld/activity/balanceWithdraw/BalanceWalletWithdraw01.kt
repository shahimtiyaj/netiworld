package com.netizen.netiworld.activity.balanceWithdraw

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile01
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.MobileBank
import com.netizen.netiworld.model.MobileBankAcNo
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class BalanceWalletWithdraw01 : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next01: ImageButton? = null
    private var txt_wallet_balance: TextView? = null

    private var spinner_bank_name: Spinner? = null
    private var spinner_account_number: Spinner? = null
    private var selectBank: String? = null
    private var selecAccountNo: String? = null

    private var coreBankAccArrayList: ArrayList<MobileBankAcNo>? = null
    private var coreBankNameArrayList: ArrayList<MobileBank>? = null

    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_withdraw_01)

        toolBarInit()
        initializeViews()
        ProfileActivity().getUserProfileInfoData()
        viewDialog = ViewDialog(this)

        BankAccountName()

        next01 = findViewById<ImageButton>(R.id.next01)

        next01?.setOnClickListener {

            if (selectBank == null || selectBank?.trim { it <= ' ' }!!.isEmpty() || selectBank == "Select Bank") {
                Toasty.error(applicationContext, "Select Bank", Toast.LENGTH_SHORT, true).show()
            }

            else if (selecAccountNo == null || selecAccountNo?.trim { it <= ' ' }!!.isEmpty() || selecAccountNo == "Select Account Number") {
                Toasty.error(applicationContext, "Select Account Number", Toast.LENGTH_SHORT, true).show()
            }

            else {
                startActivity(Intent(this@BalanceWalletWithdraw01, BalanceWalletWithdraw02::class.java))
                overridePendingTransition(R.anim.left_in, R.anim.left_out)

            }
        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Withdraw"

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    private fun initializeViews() {

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        spinner_bank_name = findViewById<View>(R.id.spinner_bank_name) as Spinner
        spinner_account_number = findViewById<View>(R.id.spinner_account_number) as Spinner
        spinner_bank_name?.onItemSelectedListener = this
        spinner_account_number?.onItemSelectedListener = this

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f",walletBalance?.getUserWalletBalance())

        dao?.close()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_bank_name) {
                selectBank = spinner_bank_name!!.selectedItem.toString()
                if (selectBank == resources.getString(R.string.select_bank_name)) {
                    selectBank = ""
                } else {
                    selectBank = parent.getItemAtPosition(position).toString()

                    val da = this.let { DAO(it) }
                    da.open()
                    localBankCategoryID = da.getCoreCategoryID(selectBank!!)
                    Log.d("Category ID: ", BalanceWalletDepositMobile01.localBankCategoryID)

                    da.deleteAccountNoList()

                    BankAccountNoInformation()

                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_account_number) {
                selecAccountNo = spinner_account_number!!.selectedItem.toString()
                if (selecAccountNo == resources.getString(R.string.select_bank_acc)) {
                    selecAccountNo = ""
                } else {

                    selecAccountNo = parent.getItemAtPosition(position).toString()
                    val da = this.let { DAO(it) }
                    da.open()
                    localCoreBankAccId = da.getCoreBankAccID(selecAccountNo!!)
                    Log.d("Core Bank Acc ID: ", BalanceWalletDepositMobile01.localCoreBankAccId)
                    da.close()

                }
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Balance Wallet  ", "Spinner data")
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun mobileBankSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val allMobileBankDataList = dao?.allMobileBankData

            val mobileBank = ArrayList<String>()
            mobileBank.add(resources.getString(R.string.select_bank_name))
            for (i in allMobileBankDataList?.indices!!) {
                allMobileBankDataList[i].getCategoryName()?.let { mobileBank.add(it) }
            }

            val sectionAdapter =
                ArrayAdapter(applicationContext, R.layout.spinner_item, mobileBank)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_bank_name?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }

    private fun mobileBankAccSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val allMobileBankAccDataList = dao?.allMobileBankAccData

            val mobileAccBank = ArrayList<String>()
            mobileAccBank.add(resources.getString(R.string.select_mobile_bank_acc))

            for (i in allMobileBankAccDataList?.indices!!) {
                allMobileBankAccDataList[i].getAccShortName()?.let { mobileAccBank.add(it) }
            }

            val mobileAccAdapter =
                ArrayAdapter(applicationContext,
                    R.layout.spinner_item,
                    mobileAccBank
                )
            mobileAccAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_account_number?.adapter = mobileAccAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }



    fun BankAccountName() {

        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getAccountNameInfo(header)

        userCall?.enqueue(object : Callback<MobileBank> {

            override fun onResponse(
                call: Call<MobileBank>,
                response: Response<MobileBank>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val bankAccountNameInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(bankAccountNameInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        val c1 = c.getJSONObject("parentTypeInfoDTO")
                        val categoryDefaultCode = c1.getString("categoryDefaultCode")

                        // Get the item model
                        val bankNameList = MobileBank()
                        //set the json data in the model
                        bankNameList.setCoreCategoryID(c.getInt("coreCategoryID"))
                        bankNameList.setCategoryName(c.getString("categoryName"))

                        Log.d("CategoryName", "BankAccNameInfo :" + c.getString("categoryName"))

                        coreBankNameArrayList?.add(bankNameList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_MOBILE_BANK + "(coreCategoryID, categoryName, categoryDefaultCode) " +
                                    "VALUES(?, ?, ?)", arrayOf(
                                c.getInt("coreCategoryID").toString(),
                                c.getString("categoryName"),
                                categoryDefaultCode
                            )
                        )
                    }
                }

                mobileBankSpinnerData()
                viewDialog?.hideDialog()

            }


            override fun onFailure(call: Call<MobileBank>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

            }
        })
    }


    private fun BankAccountNoInformation() {
       // viewDialog?.showDialog()

        val header = java.util.HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getAccountNoInfo(header, localBankCategoryID)

        userCall?.enqueue(object : Callback<MobileBankAcNo> {

            override fun onResponse(
                call: Call<MobileBankAcNo>,
                response: Response<MobileBankAcNo>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val bankAccountInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(bankAccountInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val bankAccIdList = MobileBankAcNo()
                        //set the json data in the model
                        bankAccIdList.setCoreBankAccountId(c.getInt("coreBankAccId"))
                        bankAccIdList.setAccShortName(c.getString("accShortName"))
                        Log.d(
                            "coreBankAccId",
                            "BankAccountNoInformation :" + c.getInt("coreBankAccId")
                        )


                        coreBankAccArrayList?.add(bankAccIdList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_BANK_ACC_NUMBER + "(coreBankAccId, accShortName) " +
                                    "VALUES(?, ?)",
                            arrayOf(
                                c.getInt("coreBankAccId").toString(),
                                c.getString("accShortName")
                            )
                        )
                    }
                }

                mobileBankAccSpinnerData()
               // viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<MobileBankAcNo>, t: Throwable) {
                Log.d("onFailure", t.toString())
                //viewDialog?.hideDialog()

            }
        })
    }

    companion object {
        private val TAG = "BalanceWalletWithdrwal01"
        var localCoreBankAccId = ""
        var localBankCategoryID = ""
    }
}
