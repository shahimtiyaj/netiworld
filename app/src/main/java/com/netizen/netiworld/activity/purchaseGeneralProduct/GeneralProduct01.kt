package com.netizen.netiworld.activity.purchaseGeneralProduct

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceMessage.BalanceMessageRecharge
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.MessageType
import com.netizen.netiworld.model.Products
import com.netizen.netiworld.model.PurchasePoint
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import com.orhanobut.logger.Logger
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class GeneralProduct01 : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next01: ImageButton? = null
    private var txt_wallet_balance: TextView? = null

    private var spinner_purchase_point: Spinner? = null
    private var spinner_product_type: Spinner? = null
    private var spinner_product_name: Spinner? = null

    private var selectProductType: String? = null
    private var selectProductName: String? = null
    private var selectPurchaseP: String? = null
    private val productSpinnerPosition = 0

    private var purchasePointArrayList: ArrayList<PurchasePoint>? = null
    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchase_general_product_01)
        toolBarInit()
        initializeViews()
        ProfileActivity().getUserProfileInfoData()

        // HttpRequestRolesAssign().execute()
        getPurchasePointDataFromServer()

        next01 = findViewById<ImageButton>(R.id.next01)
        next01?.setOnClickListener {
            if (selectPurchasePoint.trim { it <= ' ' }.isEmpty() || selectPurchasePoint == "Select Purchase Point") {
                Toasty.error(applicationContext, "Select Purchase Point", Toast.LENGTH_SHORT, true).show()
            }

            else if (selectProductName==null || selectProductName!!.trim { it <= ' ' }.isEmpty() || selectProductName == "Select Product Name") {
                Toasty.error(applicationContext, "Select Product Name", Toast.LENGTH_SHORT, true).show()
            }

            else {
                //selectPurchasePoint=selectPurchaseP.toString()
                startActivity(Intent(this@GeneralProduct01, GeneralProduct02::class.java))
                overridePendingTransition(R.anim.left_in, R.anim.left_out)

            }
        }

        /*try {

            Thread(Runnable {
                var i = 0;
                while (i < Int.MAX_VALUE) {
                    i++
                }
                this.runOnUiThread {
                    purchasePointSpinnerData()
                }
            }).start()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }*/
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar!!.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Purchase General Product"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this)
        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        spinner_purchase_point = findViewById<View>(R.id.spinner_purchase_point) as Spinner
       // spinner_product_type = findViewById<View>(R.id.spinner_product_type) as Spinner
        spinner_product_name = findViewById<View>(R.id.spinner_product_name) as Spinner
        spinner_purchase_point?.onItemSelectedListener = this
       // spinner_product_type?.onItemSelectedListener = this
        spinner_product_name?.onItemSelectedListener = this

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f",walletBalance?.getUserWalletBalance())
        dao?.close()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner
          /*  if (productSpinnerPosition == position) {
                return; //do nothing
            } else {*/

                if (spinner.id == R.id.spinner_purchase_point) {
                    selectPurchasePoint = spinner_purchase_point?.selectedItem.toString()
                    if (selectPurchasePoint == resources.getString(R.string.select_purchase_point)) {
                        selectPurchasePoint = ""
                    } else {
                        selectPurchaseP = parent.getItemAtPosition(position).toString()

                        val da = this.let { DAO(it) }
                        da.open()
                        roleID = da.getRoleID(selectPurchasePoint)
                        useRoleAssignID = da.getRoleAssignID(selectPurchasePoint)

                        da.deleteProductNameList()

                        getProductNameDataFromServer()

                        Log.d("Role ", "ID:" + roleID)
                        Log.d("Role Assign", "ID:" + useRoleAssignID)

                        da.close()
                    }
                }

               /* if (spinner.id == R.id.spinner_product_type) {
                    selectProductType = spinner_product_type!!.selectedItem.toString()
                    if (selectProductType == resources.getString(R.string.select_product_type)) {
                        selectProductType = ""
                    } else {
                        selectProductType = parent.getItemAtPosition(position).toString()
                    }
                }*/

                if (spinner.id == R.id.spinner_product_name) {
                    selectProductName = spinner_product_name!!.selectedItem.toString()
                    if (selectProductName == resources.getString(R.string.select_product_name)) {
                        selectProductName = ""
                    } else {

                        selectProductName = parent.getItemAtPosition(position).toString()

                        val da = this.let { DAO(it) }
                        da.open()
                        userProductID = da.getUserProductID(selectProductName!!)
                        productRolesAssign = da.getUserProductRoleAssignID(selectProductName!!)

                        unitPrice = da.getUserProductUnitPrice(selectProductName!!)
                        vatPercentage = da.getUserProductVatPercentage(selectProductName!!)

                    }
                }
         //   }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Balance Wallet  ", "Spinner data")
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun getPurchasePointDataFromServer() {
        viewDialog?.showDialog()

        val header = java.util.HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getPurchasePoint(header)

        userCall?.enqueue(object : Callback<PurchasePoint> {

            override fun onResponse(
                call: Call<PurchasePoint>,
                response: Response<PurchasePoint>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val purchasePointInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(purchasePointInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        // Get the item model
                        val purchasePointList = PurchasePoint()
                        //set the json data in the model
                        purchasePointList.setUserRoleAssignID(c.getLong("userRoleAssignID"))
                        purchasePointList.setCoreRoleName(c.getString("coreRoleName"))
                        purchasePointList.setCoreRoleID(c.getLong("coreRoleID"))

                        Log.d("coreRoleName", "General Purchase :" + c.getString("coreRoleName"))

                        purchasePointArrayList?.add(purchasePointList)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_ROLE_ASSIGN + "(userRoleAssignID, coreRoleID, coreRoleName) " +
                                    "VALUES(?, ?, ?)", arrayOf(
                                c.getLong("userRoleAssignID").toString(),
                                c.getLong("coreRoleID").toString(),
                                c.getString("coreRoleName")
                            )
                        )

                        Log.d("UserRole ID :", purchasePointList.getCoreRoleID()?.toString()!!)
                        Log.d("UserRole Name :", purchasePointList.getCoreRoleName()!!)
                    }
                }

                purchasePointSpinnerData()
                viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<PurchasePoint>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }



    class HttpRequestRolesAssign() : AsyncTask<Void, Void, List<PurchasePoint>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): List<PurchasePoint>? {
            try {
                val headers = HttpHeaders()
                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                val httpEntity = HttpEntity<String>(headers)
                val url =
                    ApiClient.BASE_URL + "user/roles/assigned" // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())
                val res = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    Array<PurchasePoint>::class.java
                )

                Log.d("RestTemplate", res.body.toString())
                Logger.d(res)

                return res.body.toList();

            } catch (e: Exception) {
                Log.e("Message Type", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: List<PurchasePoint>?) {
            super.onPostExecute(result)

            try {
                for (i in result?.indices!!) {

                    val userRole = PurchasePoint()
                    userRole.setUserRoleAssignID(result[i].getUserRoleAssignID()!!)
                    userRole.setCoreRoleID(result[i].getCoreRoleID()!!)
                    userRole.setCoreRoleName(result[i].getCoreRoleName()!!)

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_ROLE_ASSIGN + "(userRoleAssignID, coreRoleID, coreRoleName) " +
                                "VALUES(?, ?, ?)", arrayOf(
                            result.get(i).getUserRoleAssignID().toString(),
                            result.get(i).getCoreRoleID().toString(),
                            result.get(i).getCoreRoleName().toString()
                        )
                    )

                    Log.d("UserRole ID :", userRole.getCoreRoleID()?.toString()!!)
                    Log.d("UserRole Name :", userRole.getCoreRoleName()!!)
                }

            } catch (e: NullPointerException) {
                Log.e("MessageType", e.message, e)
            }
        }
    }


    private fun getProductNameDataFromServer() {

        val header = java.util.HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getProductName(header, roleID)

        userCall?.enqueue(object : Callback<Products> {

            override fun onResponse(
                call: Call<Products>,
                response: Response<Products>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {

                    val productNameInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(productNameInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        //productRolesAssign = c.getString("productRoleAssignID")
                        val c1 = c.getJSONObject("productInfoDTO")

                        // Get the item model
                        val productNameList = Products()

                        productNameList.setProductID(c1.getInt("productID"))
                        productNameList.setProductName(c1.getString("productName"))
                        productNameList.setSalesPrice(c1.getDouble("salesPrice"))
                        productNameList.setPercentVat(c1.getDouble("percentVat"))

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_PRODUCTS_INFO + "(productID, productName, salesPrice, percentVat, productRolesAssign) " +
                                    "VALUES(?, ?, ?, ?, ?)", arrayOf(
                                c1.getInt("productID").toString(),
                                c1.getString("productName"),
                                c1.getDouble("salesPrice").toString(),
                                c1.getDouble("percentVat").toString(),
                                c.getString("productRoleAssignID")
                            )
                        )

                        Log.d("Product ID :", productNameList.getProductID()?.toString()!!)
                        Log.d("Product Name :", productNameList.getProductName()!!)


                    }
                }

                productNameSpinnerData()

            }

            override fun onFailure(call: Call<Products>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    class HttpRequestProductName() : AsyncTask<Void, Void, List<Products>>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): List<Products>? {
            try {
                val headers = HttpHeaders()
                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                val httpEntity = HttpEntity<String>(headers)

                val url =
                    ApiClient.BASE_URL + "user/products/by/role?roleID=" + roleID // the  url from where to fetch data(json)

                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())
                val res = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    Array<Products>::class.java
                )

                Log.d("RestTemplate", res.body.toString())
                Logger.d(res)

                return res.body.toList();

            } catch (e: Exception) {
                Log.e("Message Type", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: List<Products>?) {
            super.onPostExecute(result)

            try {
                for (i in result?.indices!!) {

                    val products = Products()
                    products.setProductID(result[i].getProductID()!!)
                    products.setProductName(result[i].getProductName()!!)
                    products.setSalesPrice(result[i].getSalesPrice()!!)
                    products.setPercentVat(result[i].getPercentVat()!!)

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_PRODUCTS_INFO + "(productID, productName, salesPrice, percentVat) " +
                                "VALUES(?, ?, ?, ?)", arrayOf(
                            result.get(i).getProductID().toString(),
                            result.get(i).getProductName().toString(),
                            result.get(i).getSalesPrice().toString(),
                            result.get(i).getPercentVat().toString()
                        )
                    )

                    Log.d("Product ID :", products.getProductID()?.toString()!!)
                    Log.d("Product Name :", products.getProductName()!!)
                }

            } catch (e: NullPointerException) {
                Log.e("Product Info: ", e.message, e)
            }
        }
    }

    private fun purchasePointSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val userRoleArrayList = dao?.allAssignRole
            val purchasePoint = ArrayList<String>()
            purchasePoint.add(resources.getString(R.string.select_purchase_point))
            for (i in userRoleArrayList?.indices!!) {
                userRoleArrayList[i].getCoreRoleName()?.let { purchasePoint.add(it) }
            }

            val sectionAdapter =
                ArrayAdapter(
                    applicationContext,
                    R.layout.spinner_item,
                    purchasePoint
                )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_purchase_point?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }

    private fun productNameSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val productNameArrayList = dao?.allProductsName

            val purchasePoint = ArrayList<String>()
            purchasePoint.add(resources.getString(R.string.select_product_name))

            for (i in productNameArrayList?.indices!!) {
                productNameArrayList[i].getProductName()?.let { purchasePoint.add(it) }
            }

            val sectionAdapter =
                ArrayAdapter(
                    applicationContext,
                    R.layout.spinner_item,
                    purchasePoint
                )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_product_name?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }



    companion object {
        private val TAG = "BalanceMessageRecharge"
        var selectPurchasePoint = ""
        var roleID = ""
        var useRoleAssignID = ""
        var userProductID = ""
        var messageQuantity = ""
        var unitPrice = ""
        var vatPercentage= ""
        var productRolesAssign= ""
    }

}
