package com.netizen.netiworld.activity.balanceDepositBank

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.util.Base64
import android.util.Base64.NO_WRAP
import android.util.Base64OutputStream
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.view.drawToBitmap
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile01
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile02
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.app.AppController.Companion.context
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.CoreBankAccountInfoDTO
import com.netizen.netiworld.model.DepositMobile
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.balance_wallet_deposit_03.*
import lib.folderpicker.FolderPicker
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import yogesh.firzen.filelister.FileListerDialog
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException


class BalanceWalletDeposit03 : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var submit: Button? = null
    var deposit_slip: Button? = null

    private var inputBankBranchName: EditText? = null
    private var inputNote: EditText? = null
    private var spinner_deposit_type: Spinner? = null
    private var txt_wallet_balance: TextView? = null
    private var imageView: ImageView? = null


    private var branchName: String? = null
    private var note: String? = null
    private var selectBankType: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_03)
        toolBarInit()
        initializeViews()

        submit = findViewById<Button>(R.id.submit)
        deposit_slip = findViewById<Button>(R.id.et_deposit_slip)

        deposit_slip?.setOnClickListener {

            selectImage()
        }

        submit?.setOnClickListener {

            submitData()

            startActivity(Intent(this@BalanceWalletDeposit03, MainActivity::class.java))
        }
    }

    /**
     * SetUp toolbar method
     */
    private fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Deposit(Bank)"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun initializeViews() {
        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        inputBankBranchName = findViewById<View>(R.id.et_bank_branch) as EditText
        inputNote = findViewById<View>(R.id.et_deposit_note) as EditText

        /*spinner_deposit_type = findViewById<View>(R.id.spinner_deposit_type) as Spinner
        spinner_deposit_type?.onItemSelectedListener = this*/

        imageView = findViewById<ImageView>(R.id.imageview) as ImageView



        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = walletBalance?.getUserWalletBalance().toString()
        dao?.close()


    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

           // val spinner = parent as Spinner

            /*if (spinner.id == R.id.spinner_deposit_type) {
                selectBankType = spinner_deposit_type?.selectedItem.toString()
                if (selectBankType == resources.getString(R.string.select_deposit_type)) {
                    selectBankType = ""
                } else {
                    selectBankType = parent.getItemAtPosition(position).toString()
                }
            }*/


        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result  ", "Spinner data")
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletDepositMobile02::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }


    fun selectImage() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.setType("image/*")
        startActivityForResult(photoPickerIntent, 2)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            2 -> if (resultCode == RESULT_OK) {
                val choosenImage = data?.getData()
               if (choosenImage != null) {
                    val bp = decodeUri(choosenImage, 400)
                    imageView?.setImageBitmap(bp)
                    fileContentByteArray = profileImage(bp!!)

                    Log.d("Byte Array Result: ", fileContentByteArray.toString())
                }
            }
        }

    }

    fun decodeUri(selectedImage: Uri, REQUIRED_SIZE: Int): Bitmap? {

        try {
            // Decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(contentResolver.openInputStream(selectedImage), null, o)
            // The new size we want to scale to
            // final int REQUIRED_SIZE =  size;
            // Find the correct scale value. It should be the power of 2.
            var width_tmp = o.outWidth
            var height_tmp = o.outHeight
            var scale = 1
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break
                }
                width_tmp /= 2
                height_tmp /= 2
                scale *= 2
            }

            // Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(
                contentResolver.openInputStream(selectedImage),
                null,
                o2
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    private fun profileImage(b: Bitmap): ByteArray {
        val bos = ByteArrayOutputStream()
        try {
            b.compress(Bitmap.CompressFormat.PNG, 0, bos)
            Log.d("Byte Array Result: ", bos.toString())

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return bos.toByteArray()
    }

    private fun submitData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val imgString = Base64.encodeToString(fileContentByteArray, NO_WRAP)
        Log.d("Base64: ", imgString)

        branchName = inputBankBranchName?.text.toString()
        note = inputNote?.text.toString()

        val deposit = DepositMobile(MyUtilsClass.getDateFormatForSerachData(BalanceWalletDepositMobile02.depositDate!!),
            BalanceWalletDepositMobile02.depositAmount, null, branchName, note, "124557.PNG", imgString, true, CoreBankAccountInfoDTO(
                BalanceWalletDepositMobile01.localCoreBankAccId.toIntOrNull()))

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.depositpostData(deposit, header)

        //calling the api
        call?.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Token:" + response.body())

                if (response.code() >= 201) {
                    Log.d("onResponse", "Success:" + response.body())
                    Toasty.success(
                        applicationContext,
                        "Balance Wallet Deposited Successfully!!",
                        Toast.LENGTH_SHORT,
                        true
                    ).show()
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Toasty.error(
                    applicationContext,
                    "Mobile Balance Deposit fail !",
                    Toast.LENGTH_SHORT,
                    true
                ).show()
            }
        })
    }


//    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (resultCode != RESULT_CANCELED) {
//
//            if (requestCode == 2 && resultCode == RESULT_OK) {
//
//                val pathHolder = data?.data
//                Log.d("File Data :", pathHolder.toString())
//
//                val uri = data?.getData()
//                val file = File(uri?.path!!)
//
//                val contentResolver = contentResolver
//                val mimeTypeMap = MimeTypeMap.getSingleton()
//                fileNameWithExtension = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
//
//                fileName = file.name + "." + fileNameWithExtension
//
//                Log.d("File Name Ex :", fileNameWithExtension!!)
//
//                val bitmap = decodeUri(uri, 400)
//                imageView?.setImageBitmap(bitmap)
//                fileContentByteArray = profileImage(bitmap!!)
//
//                Log.d("Byte Array Result: ", fileContentByteArray.toString())
//
////                val stream = ByteArrayOutputStream()
////                bitmap?.compress(Bitmap.CompressFormat.PNG, 0, stream)
////                fileContentByteArray = stream.toByteArray()
////
////                val baos = ByteArrayOutputStream()
////
////                val fis: FileInputStream
////                try {
////                    //   val data=File(uri.path!!).inputStream().readBytes().toString(Charsets.UTF_8)
////
////                    fis = FileInputStream(File(uri.path!!))
////                    val buf = ByteArray(1024)
////                    var n: Int
////                    while (fis.read(buf).let { n = it; it != -1 }) {
////                        baos.write(buf, 0, n)
////                    }
////                    //  Log.d("Byte new: ", data)
////                } catch (e: Exception) {
////                    e.printStackTrace()
////                }
////
////                fileContentByteArray = baos.toByteArray()
////
////                Log.d("Byte Array Result: ", fileContentByteArray.toString()) *//*
////
////                        Log.d("File Name :", fileName!!)
////
////                if (fileContentByteArray != null) {
////                    for (b in fileContentByteArray!!) {
////                        Log.i("myactivity", String.format("0x%20x", b))
////                    }
////                }
//
//                //  Toast.makeText(this, fileName, Toast.LENGTH_LONG).show()
//                //  Toast.makeText(this, fileContentByteArray.toString(), Toast.LENGTH_LONG).show()
//
//            }
//        }
//
//    }


    private fun fileToBase64Conversion(file:Uri):String? {
        var inputStream: org.springframework.util.support.Base64.InputStream? //You can get an inputStream using any IO API
        val output = ByteArrayOutputStream()
        try
        {
            inputStream = context?.getContentResolver()?.openInputStream(file) as org.springframework.util.support.Base64.InputStream
            val buffer = ByteArray(8192)
            var bytesRead:Int
            val outputBase64 = Base64OutputStream(output, Base64.DEFAULT)
            try
            {

                while (inputStream.read(buffer).let { bytesRead = it; it != -1 }) {
                    outputBase64.write(buffer, 0, bytesRead)                   }
            }
            catch (e: IOException) {
                e.printStackTrace()
            }
            outputBase64.close()
        }
        catch (ex:Exception) {
        }
        return null
    }

    companion object {
        private val TAG = "BalanceWalletDeposit03"
        var fileContentByteArray: ByteArray? = null
        var fileName: String? = null
        var fileNameWithExtension: String? = null
    }

}

