package com.netizen.netiworld.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.network.CloudRequest
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity.Companion.Token
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.ProfileImage
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import com.victor.loading.newton.NewtonCradleLoading
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.profile_view_layout.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ProfileActivity : AppCompatActivity() {

    private val TAG = "ProfileActivity"

    var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    var txt_neti: TextView? = null
    internal var txt_name: TextView? = null
    internal var txt_profile_name: TextView? = null
    internal var txt_gender: TextView? = null
    internal var txt_religion: TextView? = null
    internal var txt_date_of_birth: TextView? = null
    internal var txt_address: TextView? = null
    internal var txt_phone: TextView? = null
    internal var txt_email: TextView? = null
    internal var image: CircleImageView? = null

    var newtonCradleLoading: NewtonCradleLoading? = null

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_new)
        toolBarInit()

        newtonCradleLoading = this.findViewById(R.id.newton_cradle_loading)
        newtonCradleLoading?.start()
        submitGeneralProductData()

        image = findViewById<CircleImageView>(R.id.profile_pic_id)
        txt_neti = findViewById<TextView>(R.id.txt_neti_id)
        txt_name = findViewById<TextView>(R.id.txt_name)
        txt_gender = findViewById<TextView>(R.id.txt_gender)
        txt_religion = findViewById<TextView>(R.id.txt_religion)
        txt_date_of_birth = findViewById<TextView>(R.id.txt_date_of_birth)
        txt_address = findViewById<TextView>(R.id.txt_address)
        txt_phone = findViewById<TextView>(R.id.txt_phone)
        txt_email = findViewById<TextView>(R.id.txt_email)
        txt_profile_name = findViewById<TextView>(R.id.txt_profile_name)
        //toolbar?.title = "Profile"

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val userProfile = dao?.getUserProfileDetails()

        val dateFormatprev = SimpleDateFormat("yyyy-mm-dd")
        val dateFormatprevParse = dateFormatprev.parse(userProfile?.getDateOfBirth()!!)
        val dateFormat = SimpleDateFormat("dd/mm/yyyy")
        val userDateOfBirth = dateFormat.format(dateFormatprevParse!!)

        userProfile.getNetiID()?.let { txt_neti?.setText("Neti ID: " + it) }
        txt_name?.setText("Name            :  " + userProfile.getFullName())
        txt_address?.setText(userProfile.getAddress())
        txt_phone?.setText(userProfile.getBasicMobile())
        txt_email?.setText(userProfile.getBasicEmail())
        txt_gender?.setText("Gender          :  " + userProfile.getGender())
        txt_religion?.setText("Religion         :  " + userProfile.getReligion())
        txt_date_of_birth?.setText("Date of Birth :  " + MyUtilsClass.getDateFormat(userProfile?.getDateOfBirth()!!))
        txt_profile_name?.text = userProfile.getFullName()

        txt_neti?.visibility = (View.VISIBLE)
        txt_name?.visibility = (View.VISIBLE)
        txt_address?.visibility = (View.VISIBLE)
        txt_profile_name?.visibility = (View.VISIBLE)
        txt_phone?.visibility = (View.VISIBLE)
        txt_email?.visibility = (View.VISIBLE)
        txt_gender?.visibility = (View.VISIBLE)
        txt_religion?.visibility = (View.VISIBLE)
        txt_date_of_birth?.visibility = (View.VISIBLE)

        newtonCradleLoading?.stop()
        newtonCradleLoading?.visibility = View.INVISIBLE

    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Profile"
        toolbarTitle?.textSize = 20F

        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }


    fun getUserProfileInfoData() {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer $Token"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getUserProfileInfo(header)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Main :" + response.body().toString())

                if (response.code() == 302) {

                    val profileInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val jsonObj = JSONObject(profileInfo!!)
                    val obj = jsonObj.getJSONObject("globalAreaInfoDTO")

                    val customNetiID = jsonObj.getString("customNetiID")
                    val fullName = jsonObj.getString("fullName")

                    val categoryName = obj.getString("categoryName")
                    val basicMobile = jsonObj.getString("basicMobile")
                    val basicEmail = jsonObj.getString("basicEmail")

                    //val name = jsonObj.getString("fullName")
                    val gender = jsonObj.getString("gender")
                    val religion = jsonObj.getString("religion")
                    val dateOfBirth = jsonObj.getString("dateOfBirth")
                    val userWalletBalance = jsonObj.getString("userWalletBalance")
                    val smsBalance = jsonObj.getString("smsBalance")
                    val imagePath = jsonObj.getString("imagePath")


                    /*txt_neti?.visibility = (View.VISIBLE)
                    txt_name?.visibility = (View.VISIBLE)
                    txt_profile_name?.visibility = (View.VISIBLE)
                    txt_phone?.visibility = (View.VISIBLE)
                    txt_email?.visibility = (View.VISIBLE)
                    txt_gender?.visibility = (View.VISIBLE)
                    txt_religion?.visibility = (View.VISIBLE)
                    txt_date_of_birth?.visibility = (View.VISIBLE)

                    txt_neti?.text = customNetiID
                    txt_name?.text = fullName
                    txt_profile_name?.text=fullName
                    txt_address?.text = categoryName
                    txt_phone?.text = basicMobile
                    txt_email?.text = basicEmail
                    txt_gender?.text = gender
                    txt_religion?.text = religion
                    txt_date_of_birth?.text = dateOfBirth*/

                    //Insert user login data into local database for  showing data into offline
                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_PROFILE_INFO + "(netiId, userName, mobile, email, gender, religion, birthday, userWalletBalance, smsBalance, address, imagePath) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", arrayOf(
                            customNetiID,
                            fullName,
                            basicMobile,
                            basicEmail,
                            gender,
                            religion,
                            dateOfBirth,
                            userWalletBalance,
                            smsBalance,
                            categoryName,
                            imagePath

                        )
                    )

                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun submitGeneralProductData() {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val userProfile = dao?.getUserProfileDetails()

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = userProfile?.getImagePath()?.let { service?.getProfile(header, it) }

        //calling the api
        call?.enqueue(object : Callback<ProfileImage> {
            override fun onResponse(call: Call<ProfileImage>, response: Response<ProfileImage>) {
                Log.d("onResponse", "Main Response:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Profile Image:" + response.body()?.fileContent)

                    val photo = response.body()?.fileContent

                    Log.d("onResponse", "Photo:" + photo)

                    val theByteArray: ByteArray?

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (photo != "") {
                            theByteArray = Base64.getDecoder().decode(photo)
                            if (photo != null || photo != "") {
                                image?.setImageBitmap(theByteArray?.let { convertToBitmap(it) })
                            }
                        }
                    }

                }

            }

            override fun onFailure(call: Call<ProfileImage>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }

/*
    class HttpRequestTask() : AsyncTask<Void, Void, Info>() {

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Info? {

            try {

                val headers = HttpHeaders()
                headers.set("Authorization", "bearer $Token")
                val httpEntity = HttpEntity<String>(headers);
                //val url = "https://api.androidhive.info/volley/person_object.json" // the  url from where to fetch data(json)
                val url = BASE_URL + "user/profile" // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())

                val res = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Info::class.java)

                Log.d("RestTemplate", res.body.toString())
                Log.d("RestTemplate1", res.body.getNetiID().toString())

                //Insert user login data into local database for  showing data into offline
                DAO.executeSQL(
                    "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_PROFILE_INFO + "(netiId, userName, mobile, email, gender, religion, birthday, userWalletBalance, smsBalance) " +
                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", arrayOf(
                        res.body.getCustomNetiID().toString(),
                        res.body.getFullName().toString(),
                        res.body.getBasicMobile().toString(),
                        res.body.getBasicEmail().toString(),
                        res.body.getGender().toString(),
                        res.body.getReligion().toString(),
                        res.body.getDateOfBirth().toString(),
                        String.format("%.2f", res.body.getUserWalletBalance()),
                        res.body.getSmsBalance().toString()
                    )
                )

                Log.d("Wallet Balance", String.format("%.2f", res.body.getUserWalletBalance()))

                return res.body

            } catch (e: Exception) {
                Log.e("MainActivity", e.message, e)
            }

            return null
        }

        override fun onPostExecute(result: Info?) {
            super.onPostExecute(result)
        }
    }*/

/*    val future = doAsync {
        try {
            val headers = HttpHeaders()
            headers.set("Authorization", "bearer $Token")
            val httpEntity = HttpEntity<String>(headers);
            //val url = "https://api.androidhive.info/volley/person_object.json" // the  url from where to fetch data(json)
            val url = BASE_URL + "user/profile" // the  url from where to fetch data(json)
            val restTemplate = RestTemplate()
            restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())

            val res = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Info::class.java)

            //  Logger.json(res.body.toString())

            Log.d("RestTemplate", res.body.toString())
            Log.d("RestTemplate1", res.body.getCustomNetiID().toString())

            res.body

        } catch (e: Exception) {
            Log.e("MainActivity", e.message, e)
        }

        uiThread {
            // use result here if you want to update ui
            val info = Info()
            //txt_neti_id?.text = info.getNetiID().toString()
        }
    }*/


    private fun getUserProfileInfo() {


/*      val handler: Handler? = null

        val thread = Thread() {
            fun run() {
                Log.d("Load ServerData Thread ", "Started..");
                handler?.post {
                    @Override
                    fun run() {
                    }
                }
                try {
                    val headers = HttpHeaders()
                    headers.set("Authorization", "bearer $Token")
                    val httpEntity = HttpEntity<String>(headers);
                    //val url = "https://api.androidhive.info/volley/person_object.json" // the  url from where to fetch data(json)
                    val url =
                        BASE_URL + "user/profile" // the  url from where to fetch data(json)
                    val restTemplate = RestTemplate()
                    restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())

                    val res =
                        restTemplate.exchange(url, HttpMethod.GET, httpEntity, Info::class.java)

                    //  Logger.json(res.body.toString())

                    Log.d("RestTemplate", res.body.toString())
                    Log.d("RestTemplate1", res.body.getCustomNetiID().toString())

                    // return res.body

                } catch (e: Exception) {
                    Log.e("MainActivity", e.message, e)
                }
            }
        }
        thread.start()*/
    }

    private fun loadUserInfo() {

        val hitURL = "http://192.168.31.37:9001/user/profile"

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, hitURL, null,
            com.android.volley.Response.Listener { response ->
                Log.d(TAG, "Main Response: $response")

                try {

                    val netiId = response?.getString("netiID")
                    txt_neti_id?.text = netiId

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            com.android.volley.Response.ErrorListener { error ->
                Log.d(
                    "Error Response: ", error.toString()
                )
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "bearer $Token"

                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            100000//10 Minutes-change to what you want//100000 milliseconds = 1 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }
}


