package com.netizen.netiworld.activity.balanceWithdraw

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile02
import com.netizen.netiworld.activity.balanceWithdraw.BalanceWalletWithdraw01.Companion.localCoreBankAccId
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.MobileBankAcNo
import com.netizen.netiworld.model.UserBankAccountInfoDTO
import com.netizen.netiworld.model.UserWithdraw
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.DecimalDigitsInputFilter
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class BalanceWalletWithdraw02 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var submit: Button? = null
    var previous: ImageButton? = null

    private var inputBankWithDrawAmt: EditText? = null
    private var inputNote: EditText? = null
    private var txt_wallet_balance: TextView? = null

    private var coreBankAccArrayList: ArrayList<MobileBankAcNo>? = null

    var viewDialog: ViewDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_withdraw_02)
        toolBarInit()
        initializeViews()
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Withdraw"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {


        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun afterTextChanged(editable: Editable) {
            /*if (!inputBankWithDrawAmt?.text.toString().startsWith("0") ){
                inputBankWithDrawAmt?.error = "Withdraw amount can't be 0"
                requestFocus(inputBankWithDrawAmt!!)
            }*/
        }
    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this);

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        inputBankWithDrawAmt = findViewById<View>(R.id.et_bank_withdraw_amount) as EditText
        inputNote = findViewById<View>(R.id.et_note) as EditText
        inputBankWithDrawAmt?.addTextChangedListener(MyTextWatcher(inputBankWithDrawAmt!!))

        inputBankWithDrawAmt?.filters = arrayOf(DecimalDigitsInputFilter())
        inputBankWithDrawAmt?.setText(bankWithDrawAmt)
        inputNote?.setText(note)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f", walletBalance?.getUserWalletBalance())

        submit = findViewById<Button>(R.id.submit)
        submit?.setOnClickListener {
            try {
                txt_wallet_balance?.text = String.format("%.2f", walletBalance?.getUserWalletBalance())

                if (inputBankWithDrawAmt == null || inputBankWithDrawAmt?.text.toString().trim { it <= ' ' }.isEmpty()) {
                    inputBankWithDrawAmt?.error = getString(R.string.err_mssg_amount_withdraw)
                    inputBankWithDrawAmt?.let { requestFocus(it) }
                }

              /* else if(inputBankWithDrawAmt?.text.toString()=="0" || inputBankWithDrawAmt?.text.toString()=="0.0") {
                    inputBankWithDrawAmt?.error = "Withdraw amount can't be 0"
                    inputBankWithDrawAmt?.let { requestFocus(it) }
                }*/
               else  if (inputBankWithDrawAmt?.text.toString().startsWith("0")) {
                    inputBankWithDrawAmt?.error = "Withdraw amount can't be 0"
                    requestFocus(inputBankWithDrawAmt!!)
                }

                else if((inputBankWithDrawAmt?.text.toString().toDouble()) > String.format("%.2f", walletBalance?.getUserWalletBalance()).toDouble()) {
                    inputBankWithDrawAmt?.error = getString(R.string.err_balance)
                    inputBankWithDrawAmt?.let { requestFocus(it) }

                    /*Toasty.error(
                        applicationContext,
                        inputBankWithDrawAmt?.text.toString()+">"+  String.format("%.2f", walletBalance?.getUserWalletBalance()),
                        Toast.LENGTH_LONG,
                        true
                    ).show()*/
                }

                else if (inputNote == null || inputNote?.text.toString().trim { it <= ' ' }.isEmpty()) {
                    inputNote?.error = getString(R.string.err_mssg_note)
                    inputNote?.let { requestFocus(it) }
                } else {
                    submitData()
                }

            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
            dao?.close()
        }

        previous = findViewById<ImageButton>(R.id.previous)
        previous?.setOnClickListener {
            bankWithDrawAmt = inputBankWithDrawAmt?.text.toString()
            note = inputNote?.text.toString()
            val intent = Intent(this, BalanceWalletWithdraw01::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.right_in, R.anim.right_out)
            finish()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletWithdraw01::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)
        finish()
    }

    private fun submitData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        bankWithDrawAmt = inputBankWithDrawAmt?.text.toString()
        note = inputNote?.text.toString()

        val balanceWithdraw = UserWithdraw(
            bankWithDrawAmt?.toDouble(),
            note,
            UserBankAccountInfoDTO(localCoreBankAccId.toLongOrNull())
        )

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceWithdrawData(balanceWithdraw, header)

        //calling the api
        call?.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Balance Withdraw:" + response.body())

                if (response.code() == 201) {
                    Log.d("onResponse", "Token:" + response.body())
                    Toasty.success(
                        applicationContext,
                        "Balance Wallet Withdraw Successfully !",
                        Toast.LENGTH_LONG,
                        true
                    ).show()

                    inputBankWithDrawAmt?.text?.clear()
                    bankWithDrawAmt=null
                    note=null
                    startActivity(Intent(this@BalanceWalletWithdraw02, MainActivity::class.java))

                }
                else if (response.code()== 400) {
                    Toasty.error(
                        applicationContext,
                        "Insufficient Wallet Balance",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
                else if (response.code()== 409) {
                    Toasty.error(
                        applicationContext,
                        "Duplicate Entry",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                else if (response.code()== 500) {
                    Toasty.error(
                        applicationContext,
                        "An Error Occurred",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                else  {
                    Toasty.error(
                        applicationContext,
                        "Opps!! Something Wrong",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                viewDialog?.hideDialog()
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

                Toasty.error(
                    applicationContext,
                    "Balance Wallet Withdraw Fail!!",
                    Toast.LENGTH_LONG,
                    true
                ).show()

            }
        })
    }


    class HttpRequestTask() : AsyncTask<Void, Void, String>() {
        private var inputBankWithDrawAmt: EditText? = null
        private var inputNote: EditText? = null
        private var bankWithDrawAmt: String? = null
        private var note: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            bankWithDrawAmt = inputBankWithDrawAmt?.text.toString()
            note = inputNote?.text.toString()
        }

        override fun doInBackground(vararg params: Void?): String? {
            try {
                val headers = HttpHeaders()

                headers.set("Authorization", "bearer ${SignInActivity.Token}")
                headers.set("Content-Type", "application/json")

                val jsonObj = JSONObject()
                val jObj1 = JSONObject()

                jsonObj.put("requestedAmount", 500)
                jObj1.put("userBankAccId", 30)
                jsonObj.put("userBankAccountInfoDTO", jObj1)
                jsonObj.put("requestNote", "Testing for withwraw req")

                val jObjPost = jsonObj.toString()

                Log.d("Withdraw JOSN:", jObjPost)


                val balanceWithdraw =
                    UserWithdraw(bankWithDrawAmt?.toDouble(), note, UserBankAccountInfoDTO(30))

                val httpEntity = HttpEntity(balanceWithdraw, headers)

                val url =
                    ApiClient.BASE_URL + "user/balance/withdraw" // the  url from where to fetch data(json)
                val restTemplate = RestTemplate()
                restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())

                val res =
                    restTemplate.exchange(url, HttpMethod.POST, httpEntity, String::class.java)
                Log.d("Withdraw Model:", res.body)


                return res.body.toString()

            } catch (e: HttpClientErrorException) {
                Log.e("Status: ", e.statusCode.toString(), e)
                Log.e("Body: ", e.responseBodyAsString, e)
            } catch (e: HttpStatusCodeException) {
                Log.e("Status: ", e.statusCode.toString(), e)
                Log.e("Body: ", e.responseBodyAsString, e)
            } catch (e: HttpServerErrorException) {
                Log.e("Status: ", e.statusCode.toString(), e)
                Log.e("Body: ", e.responseBodyAsString, e)
            } catch (e: RestClientException) {
                Log.e("Status: ", e.toString(), e)
                Log.e("Body: ", e.toString(), e)
            }

            return null

        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Toast.makeText(AppController.context, "Withdraw Balance: $result", Toast.LENGTH_LONG).show()
        }
    }



    companion object {
        private var bankWithDrawAmt: String? = null
        private var note: String? = null
    }
}
