package com.netizen.netiworld.activity.balanceDepositBank

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R

class BalanceWalletDeposit01 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var next01: ImageButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_01)
        toolBarInit()

        next01 = findViewById<ImageButton>(R.id.next01)

        next01?.setOnClickListener {
            startActivity(Intent(this@BalanceWalletDeposit01, BalanceWalletDeposit02::class.java))
        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Deposit(Bank)"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

}
