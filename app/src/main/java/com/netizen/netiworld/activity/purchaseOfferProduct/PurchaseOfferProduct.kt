package com.netizen.netiworld.activity.purchaseOfferProduct

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.ProfileActivity
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.OfferProduct
import com.netizen.netiworld.model.OfferProductPost
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchaseOfferProduct : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var purchase: Button? = null
    var search: ImageButton? = null
    private var inputOfferCode: EditText? = null
    //private var offerCode: String? = null

    private var txt_wallet_balance: TextView? = null
    private var txt_discount_percentage: TextView? = null
    private var txt_code: TextView? = null
    private var txt_code_usable: TextView? = null
    private var txt_code_used: TextView? = null
    private var txt_product_quantity: TextView? = null
    private var txt_total_amt: TextView? = null
    private var txt_discount_amt: TextView? = null
    private var txt_payable_amt: TextView? = null
    private var txt_product_name: TextView? = null
    private var txt_product_unit_price: TextView? = null
    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchase_offer_product_03)

        toolBarInit()
        initializeViews()

        ProfileActivity().getUserProfileInfoData()

        purchase = findViewById<Button>(R.id.purchase)
        search = findViewById<ImageButton>(R.id.serach_offer_code)
        purchase?.setOnClickListener {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val walletBalance = dao?.getUserProfileDetails()

            if (inputOfferCode == null || inputOfferCode?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputOfferCode?.error = getString(R.string.err_mssg_offerCode)
                inputOfferCode?.let { requestFocus(it) }
            }

            else if(txt_product_name?.text.toString().isEmpty()){ Toasty.error(applicationContext, "Please Search Offer Code", Toast.LENGTH_LONG, true).show() }

            /*else if (String.format("%.2f", payableAmt) > String.format("%.2f", walletBalance?.getUserWalletBalance())) {
                Toasty.error(
                    applicationContext,
                    "Insufficient Wallet Balance",
                    Toast.LENGTH_LONG,
                    true).show()
            } */

            else {
                submitOfferProductData()
            }
        }

        search?.setOnClickListener {
            if (inputOfferCode == null || inputOfferCode?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputOfferCode?.error = getString(R.string.err_mssg_offerCode)
                inputOfferCode?.let { requestFocus(it) }

            } else {
                getOfferProductData()
            }
        }

    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Purchase Offer Product"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this);

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView

        txt_discount_percentage = findViewById<View>(R.id.et_discount_percentage) as TextView
        txt_code = findViewById<View>(R.id.code) as TextView
        txt_code_usable = findViewById<View>(R.id.code_usable) as TextView
        txt_code_used = findViewById<View>(R.id.code_used) as TextView
        txt_product_quantity = findViewById<View>(R.id.product_quantity) as TextView
        txt_product_name = findViewById<View>(R.id.pname) as TextView
        txt_product_unit_price = findViewById<View>(R.id.unitPrice) as TextView

        txt_total_amt = findViewById<View>(R.id.total_amount) as TextView
        txt_discount_amt = findViewById<View>(R.id.discount_amount) as TextView
        txt_payable_amt = findViewById<View>(R.id.payable_amount) as TextView

        inputOfferCode = findViewById<View>(R.id.et_offer_code) as EditText

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f", walletBalance?.getUserWalletBalance())
        dao?.close()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    private fun getOfferProductData() {


        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        offerCode = inputOfferCode?.text.toString()

        Log.d("Offer Code: ", offerCode)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getOfferProduct(header, offerCode)

        userCall?.enqueue(object : Callback<OfferProduct> {

            override fun onResponse(call: Call<OfferProduct>, response: Response<OfferProduct>) {
                Log.d("onResponse", "Main :" + response.body().toString())

                if (response.code() == 302) {

                    val offer = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val jsonObj = JSONObject(offer!!)

                    val code = jsonObj.getString("offerCode")
                    txt_code?.text = code

                    val codeUsable = jsonObj.getString("offerUseableTime")
                    txt_code_usable?.text = codeUsable

                    val codeUsed = jsonObj.getString("offerUsed")
                    txt_code_used?.text = codeUsed

                    val productQnty = jsonObj.getString("offerQuantity")
                    txt_product_quantity?.text = productQnty

                    unitPrice = jsonObj.getString("actualPrice")
                    txt_product_unit_price?.text =String.format("%, .2f", unitPrice.toDouble())

                    val productName = jsonObj.getString("productName")
                    txt_product_name?.text = productName

                    totalAmount = productQnty.toInt() * unitPrice.toDoubleOrNull()!!
                    txt_total_amt?.text = String.format("%, .2f", totalAmount)

                    val discountAmtPer = jsonObj.getString("discountPercent")
                    txt_discount_percentage?.text = "$discountAmtPer%"

                    discountAmt = (totalAmount * discountAmtPer.toDoubleOrNull()!!) / 100
                    txt_discount_amt?.text = String.format("%, .2f", discountAmt)

                    payableAmt = totalAmount - discountAmt
                    txt_payable_amt?.text = String.format("%, .2f", payableAmt)

                    productOfferID = jsonObj.getString("productOfferID")

                    Log.d("Discount Amount :", discountAmtPer)

                    Log.d("onResponse", "Product Info :$offer")
                    val h1 = response.headers().get("Location")
                    Log.d("onResponse", "Header1 url :$h1")

                }

                else if(response.code()==400){
                    Toasty.error(applicationContext, "Offer Limit Exceed!!!", Toast.LENGTH_LONG, true).show()
                }

                else if(response.code()==404){
                    Toasty.error(applicationContext, "Invalid Offer Code!!!", Toast.LENGTH_LONG, true).show()
                }
            }

            override fun onFailure(call: Call<OfferProduct>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun submitOfferProductData() {
        viewDialog?.showDialog()
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        try {

            val offerProduct = OfferProductPost(
                unitPrice.toDoubleOrNull()!!, payableAmt, discountAmt, totalAmount,
                OfferProductPost.ProductOfferDTO(productOfferID.toIntOrNull()))

            Log.d("Offer Product:", "JSON:" + offerProduct)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.offerProductPostData(offerProduct, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Log.d("onResponse", "Message Recharge:" + response.body())

                    if (response.code() == 200) {
                        startActivity(Intent(this@PurchaseOfferProduct, MainActivity::class.java))
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        overridePendingTransition(R.anim.right_in, R.anim.right_out)

                        Log.d("onResponse", "Token:" + response.body())
                        Toasty.success(
                            applicationContext,
                            "Offer Product Purchase Successfully !!",
                            Toast.LENGTH_LONG,
                            true).show() }

                    else if (response.code()== 400) {
                        Toasty.error(
                            applicationContext,
                            "Insufficient Wallet Balance",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }
                    else if (response.code()== 409) {
                        Toasty.error(
                            applicationContext,
                            "Duplicate Entry",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else if (response.code()== 500) {
                        Toasty.error(
                            applicationContext,
                            "An Error Occurred",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    else  {
                        Toasty.error(
                            applicationContext,
                            "Opps!! Something Wrong",
                            Toast.LENGTH_LONG,
                            true
                        ).show()
                    }

                    viewDialog?.hideDialog()
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    viewDialog?.hideDialog()
                    Toasty.error(
                        applicationContext,
                        "Offer Product Purchase Fail !!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = "BalanceMessageRecharge"
        var productOfferID = ""
        var totalAmount = 0.0
        var payableAmt = 0.0
        var unitPrice = ""
        var discountAmt = 0.0
        var offerCode = " "
    }
}
