package com.netizen.netiworld.activity.balanceDepositBkash


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity.Companion.Token
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile01.Companion.localCoreBankAccId
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile02.Companion.depositAmount
import com.netizen.netiworld.activity.balanceDepositBkash.BalanceWalletDepositMobile02.Companion.depositDate
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.CoreBankAccountInfoDTO
import com.netizen.netiworld.model.DepositMobile
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BalanceWalletDepositMobile03 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var submit: Button? = null
    var previous: ImageButton? = null

    private var inputMobileBankingNo: EditText? = null
    private var inputTransactionID: EditText? = null
    private var txt_wallet_balance: TextView? = null

    var viewDialog: ViewDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_mobile_03)
        toolBarInit()
        initializeViews()

        submit = findViewById<Button>(R.id.submit)
        previous = findViewById<ImageButton>(R.id.previous)

        inputMobileBankingNo?.setText(depositMobileBankingNo)
        inputTransactionID?.setText(depositTransactionNo)

        previous?.setOnClickListener {
            depositMobileBankingNo = inputMobileBankingNo?.text.toString() //01675886072
            depositTransactionNo = inputTransactionID?.text.toString()
            val intent = Intent(this, BalanceWalletDepositMobile02::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.right_in, R.anim.right_out)

            finish()
        }

        submit?.setOnClickListener {
            if (inputMobileBankingNo == null || inputMobileBankingNo?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputMobileBankingNo?.error = getString(R.string.err_mssg_mobile_bank_no)
                inputMobileBankingNo?.let { requestFocus(it) }
            } else if (inputTransactionID == null || inputTransactionID?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputTransactionID?.error = getString(R.string.err_mssg_mobile_transsaction_id)
                inputTransactionID?.let { requestFocus(it) }
            } else {
                submitData()
            }

        }
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Deposit(Mobile Banking)"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {
        viewDialog = ViewDialog(this);

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        inputMobileBankingNo = findViewById<View>(R.id.et_bank_branch) as EditText
        inputTransactionID = findViewById<View>(R.id.et_transaction_id) as EditText

        inputMobileBankingNo?.setText(depositMobileBankingNo)
        inputTransactionID?.setText(depositTransactionNo)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f",walletBalance?.getUserWalletBalance())
        dao?.close()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletDepositMobile02::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

    /*override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(keyCode, event)
            return true
        }
        return false
    }*/

    private fun submitData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer $Token"

        depositMobileBankingNo = inputMobileBankingNo?.text.toString() //01675886072
        depositTransactionNo = inputTransactionID?.text.toString()

        val deposit = DepositMobile(
            MyUtilsClass.getDateFormatForSerachData(depositDate!!),
            depositAmount,
            depositTransactionNo,
            depositMobileBankingNo,
            CoreBankAccountInfoDTO(localCoreBankAccId.toIntOrNull())
        )

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.depositpostData(deposit, header)

        //calling the api
        call?.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Token:" + response.body())

                if (response.code() >= 201) {
                    Log.d("onResponse", "Success:" + response.body())
                    Toasty.success(
                        applicationContext,
                        "Balance Wallet Deposited Successfully!!",
                        Toast.LENGTH_LONG,
                        true
                    ).show()

                    depositAmount=""
                    depositDate=""
                    depositMobileBankingNo=""
                    depositTransactionNo=""

                    startActivity(Intent(this@BalanceWalletDepositMobile03, MainActivity::class.java))
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    finish()
                }

                 if (response.code()== 400) {
                    Toasty.error(
                        applicationContext,
                        "Insufficient Wallet Balance",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
                 if (response.code()== 409) {
                    Toasty.error(
                        applicationContext,
                        "Duplicate Entry",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                 if (response.code()== 500) {
                    Toasty.error(
                        applicationContext,
                        "An Error Occurred",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                else  {
                    Toasty.error(
                        applicationContext,
                        "Opps!! Something Wrong",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

                Toasty.error(
                    applicationContext,
                    "Mobile Balance Deposit fail !",
                    Toast.LENGTH_LONG,
                    true
                ).show()
            }
        })
    }

    companion object {
        private var depositMobileBankingNo: String? = null
        private var depositTransactionNo: String? = null
        private val TAG = "BalanceWalletDepositMobile02"
    }
}
