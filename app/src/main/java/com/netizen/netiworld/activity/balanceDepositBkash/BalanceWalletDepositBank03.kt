package com.netizen.netiworld.activity.balanceDepositBkash

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.netizen.netiworld.MainActivity
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.model.CoreBankAccountInfoDTO
import com.netizen.netiworld.model.DepositMobile
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.netizen.netiworld.utils.MyUtilsClass
import com.netizen.netiworld.utils.ViewDialog
import es.dmoral.toasty.Toasty
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream


class BalanceWalletDepositBank03 : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    var submit: Button? = null
    var deposit_slip: Button? = null
    var previous: ImageButton? = null

    private var inputBankBranchName: EditText? = null
    private var inputNote: EditText? = null
    private var spinner_deposit_type: Spinner? = null
    private var txt_wallet_balance: TextView? = null
    private var imageView: ImageView? = null

    private var selectBankType: String? = null
    var viewDialog: ViewDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.balance_wallet_deposit_03)
        toolBarInit()
        initializeViews()

        submit = findViewById<Button>(R.id.submit)
        deposit_slip = findViewById<Button>(R.id.et_deposit_slip)

        deposit_slip?.setOnClickListener {

            selectImage()
        }

        previous = findViewById<ImageButton>(R.id.previous)

        previous?.setOnClickListener {
            branchName = inputBankBranchName?.text.toString()
            note = inputNote?.text.toString()

            val intent = Intent(this, BalanceWalletDepositMobile02::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(R.anim.right_in, R.anim.right_out)
            finish()
        }

        submit?.setOnClickListener {

            if (inputBankBranchName == null || inputBankBranchName?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputBankBranchName?.error = getString(R.string.err_mssg_branch_name)
                inputBankBranchName?.let { requestFocus(it) }
            } else if (inputNote == null || inputNote?.text.toString().trim { it <= ' ' }.isEmpty()) {
                inputNote?.error = getString(R.string.err_mssg_note)
                inputNote?.let { requestFocus(it) }
            } else if (fileContentByteArray == null || fileContentByteArray?.toString() == "") {
                Toasty.error(applicationContext, R.string.err_mssg_slip, Toast.LENGTH_SHORT, true)
                    .show()
            } else {
                fileContentByteArray
                submitData()
            }


        }
    }

    /**
     * SetUp toolbar method
     */
    private fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar?.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle?.text = "Balance Wallet Deposit(Bank)"

        toolbar?.setNavigationOnClickListener { onBackPressed() }

    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun initializeViews() {

        viewDialog = ViewDialog(this);

        txt_wallet_balance = findViewById<View>(R.id.txt_wallet_balance) as TextView
        inputBankBranchName = findViewById<View>(R.id.et_bank_branch) as EditText
        inputNote = findViewById<View>(R.id.et_deposit_note) as EditText

        inputBankBranchName?.setText(branchName)
        inputNote?.setText(note)

        imageView = findViewById<ImageView>(R.id.imageview) as ImageView

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val walletBalance = dao?.getUserProfileDetails()
        txt_wallet_balance?.text = String.format("%, .2f", walletBalance?.getUserWalletBalance())
        dao?.close()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, BalanceWalletDepositMobile02::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        overridePendingTransition(R.anim.right_in, R.anim.right_out)

        finish()
    }

   /* override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(keyCode, event)
            return true
        }
        return false
    }*/

    fun selectImage() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.setType("image/*")
        startActivityForResult(photoPickerIntent, 2)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            2 -> if (resultCode == RESULT_OK) {
                val choosenImage = data?.getData()
                if (choosenImage != null) {
                    val bp = decodeUri(choosenImage, 400)
                    imageView?.setImageBitmap(bp)
                    fileContentByteArray = profileImage(bp!!)

                    Log.d("Byte Array Result: ", fileContentByteArray.toString())
                }
            }
        }

    }

    fun decodeUri(selectedImage: Uri, REQUIRED_SIZE: Int): Bitmap? {

        try {
            // Decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(contentResolver.openInputStream(selectedImage), null, o)
            // The new size we want to scale to
            // final int REQUIRED_SIZE =  size;
            // Find the correct scale value. It should be the power of 2.
            var width_tmp = o.outWidth
            var height_tmp = o.outHeight
            var scale = 1
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break
                }
                width_tmp /= 2
                height_tmp /= 2
                scale *= 2
            }

            // Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(
                contentResolver.openInputStream(selectedImage),
                null,
                o2
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    private fun profileImage(b: Bitmap): ByteArray {
        val bos = ByteArrayOutputStream()
        try {
            b.compress(Bitmap.CompressFormat.PNG, 0, bos)
            Log.d("Byte Array Result: ", bos.toString())

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return bos.toByteArray()
    }

    private fun submitData() {
        viewDialog?.showDialog()

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val imgString = Base64.encodeToString(fileContentByteArray, Base64.NO_WRAP)
        Log.d("Base64: ", imgString)

        branchName = inputBankBranchName?.text.toString()
        note = inputNote?.text.toString()

        val deposit = DepositMobile(
            MyUtilsClass.getDateFormatForSerachData(BalanceWalletDepositMobile02.depositDate!!),
            BalanceWalletDepositMobile02.depositAmount,
            null,
            branchName,
            note,
            "124557.PNG",
            imgString,
            true,
            CoreBankAccountInfoDTO(
                BalanceWalletDepositMobile01.localCoreBankAccId.toIntOrNull()
            )
        )

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.depositpostData(deposit, header)

        //calling the api
        call?.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Token:" + response.body())

                if (response.code() >= 201) {
                    Log.d("onResponse", "Success:" + response.body())
                    Toasty.success(
                        applicationContext,
                        "Balance Wallet Deposited Successfully!!",
                        Toast.LENGTH_SHORT,
                        true
                    ).show()

                    branchName=""
                    note=""
                    BalanceWalletDepositMobile02.depositAmount=""
                    BalanceWalletDepositMobile02.depositDate=""

                    startActivity(Intent(this@BalanceWalletDepositBank03, MainActivity::class.java))
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    finish()
                }

               else if (response.code() == 400) {
                    Toasty.error(
                        applicationContext,
                        "Insufficient Wallet Balance",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                else if (response.code() == 409) {
                    Toasty.error(
                        applicationContext,
                        "Duplicate Entry",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }
                else  if (response.code() == 500) {
                    Toasty.error(
                        applicationContext,
                        "An Error Occurred",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                } else {
                    Toasty.error(
                        applicationContext,
                        "Opps!! Something Wrong",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

                viewDialog?.hideDialog()

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
                viewDialog?.hideDialog()

                Toasty.error(
                    applicationContext,
                    "Mobile Balance Deposit fail !",
                    Toast.LENGTH_SHORT,
                    true
                ).show()
            }
        })
    }

    companion object {
        private val TAG = "BalanceWalletDeposit03"
        var fileContentByteArray: ByteArray? = null
        var fileName: String? = null
        var fileNameWithExtension: String? = null

        private var branchName: String? = null
        private var note: String? = null
    }

}

