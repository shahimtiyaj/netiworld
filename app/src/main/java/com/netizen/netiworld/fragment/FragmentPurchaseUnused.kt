package com.netizen.netiworld.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.adapter.PurchaseUnusedCodeListAdapter
import com.netizen.netiworld.model.PurchaseCodeLogGetdata
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class FragmentPurchaseUnused: Fragment() {

    private var recyclerView: RecyclerView? = null
    private var adapterUnused: PurchaseUnusedCodeListAdapter? = null
    private var unusedCodeArrayList: ArrayList<PurchaseCodeLogGetdata>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var totalFound: TextView? = null

    private var searchView: SearchView? = null

    private var v: View? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.logs_wallet_deposit_list, container, false)

        recyclerViewInit()
        getPurchaseUnusedData()

        return v
    }

    /*
recycler view initialization method
*/
    fun recyclerViewInit() {
        // Initialize item list
        unusedCodeArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.wallet_balance_deposit_log_list) as RecyclerView
        // Create adapterUnused passing in the sample item data
        adapterUnused = activity?.let { PurchaseUnusedCodeListAdapter(it, unusedCodeArrayList!!) }
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapterUnused to the recyclerview to populate items
        recyclerView?.adapter = adapterUnused
    }


    private fun getPurchaseUnusedData() {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"
        header["Content-Type"] = "application/json"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getPurchaseCodeData(header, 0)

        //calling the api
        call?.enqueue(object : Callback<List<PurchaseCodeLogGetdata>> {
            override fun onResponse(
                call: Call<List<PurchaseCodeLogGetdata>>,
                response: Response<List<PurchaseCodeLogGetdata>>
            ) {
                Log.d("onResponse", "Purchase Code List:" + response.body())

                if (response.code() == 302) {
                    Log.d("onResponse", "Unused List:" + response.body())

                    val purchaseCodeUnuse = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val getData = JSONArray(purchaseCodeUnuse!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val c1 = c.getJSONObject("productPurchaseLogDTO")
                        val c11 = c1.getJSONObject("productInfoDTO")
                        val c2 = c11.getJSONObject("productTypeInfoDTO")
                        val c3 = c1.getJSONObject("productRoleAssignDTO")
                        val c4 = c3.getJSONObject("coreUserRoleDTO")

                        val unusedDate = c1.getString("purchaseDate")
                        val purchasePoint = c4.getString("coreRoleNote") //Product Purchase Point
                        val categoryName = c2.getString("categoryName") //Product Type
                        val productName = c11.getString("productName")
                        val purchaseCode = c.getString("purchaseCode")

                        // tem model
                        val unusedCodeList = PurchaseCodeLogGetdata()
                        //set the json data in the model
                        unusedCodeList.setPurchaseDate(unusedDate)
                        unusedCodeList.setCoreRoleNote(purchasePoint)
                        unusedCodeList.setProductType(categoryName)
                        unusedCodeList.setProductName(productName)
                        unusedCodeList.setPurchaseCode(purchaseCode)

                        unusedCodeArrayList?.add(unusedCodeList)

                    }

                }

                adapterUnused?.notifyDataSetChanged()
                totalFound?.text = unusedCodeArrayList?.size.toString()

            }

            override fun onFailure(call: Call<List<PurchaseCodeLogGetdata>>, t: Throwable) {
                Log.d("onFailure", t.toString())
                activity?.let {
                    Toasty.error(
                        it,
                        "Purchase Unused Code Get Data Fail",
                        Toast.LENGTH_LONG,
                        true
                    ).show()
                }

            }
        })
    }

}