/*
package com.netizen.netiworld.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.netizen.netiworld.R

class TabFragmentPurchaseCode : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val x = inflater.inflate(R.layout.fragment_tab, null)
        tabLayout = x.findViewById<View>(R.id.tabs) as TabLayout
        viewPager = x.findViewById<View>(R.id.viewpager) as ViewPager
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        viewPager.adapter = MyAdapter(childFragmentManager)
        viewPager.offscreenPageLimit = 1

        tabLayout.post {
            tabLayout.setupWithViewPager(viewPager)
        }

        return x
    }

    internal inner class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return FragmentPurchaseUsed()
                1 -> return FragmentPurchaseUnused()
            }
            return null
        }

        override fun getCount(): Int {

            return int_items
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return "Unused Purchase Code"
                1 -> return "Used Purchase Code"
            }
            return null
        }
    }

    companion object {
        lateinit var tabLayout: TabLayout
        lateinit var viewPager: ViewPager
        var int_items = 2
    }
}
*/
