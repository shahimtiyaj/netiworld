package com.netizen.netiworld.fragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.netizen.netiworld.R
import com.netizen.netiworld.activity.SignInActivity
import com.netizen.netiworld.app.AppController
import com.netizen.netiworld.database.DAO
import com.netizen.netiworld.database.DBHelper
import com.netizen.netiworld.model.ProfileImage
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import com.victor.loading.newton.NewtonCradleLoading
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class FragmentDashboard : Fragment() {
    var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    var txt_neti: TextView? = null
    internal var txt_name: TextView? = null
    internal var txt_profile_name: TextView? = null
    internal var txt_gender: TextView? = null
    internal var txt_religion: TextView? = null
    internal var txt_date_of_birth: TextView? = null
    internal var txt_address: TextView? = null
    internal var txt_phone: TextView? = null
    internal var txt_email: TextView? = null
    internal var image: CircleImageView? = null

    private var txt_wallet_balance: TextView? = null
    private var txt_message_balance: TextView? = null

    var newtonCradleLoading: NewtonCradleLoading? = null

    private var v: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        v= inflater.inflate(R.layout.dashboard_layout, container, false)

        txt_wallet_balance = v?.findViewById<View>(R.id.txt_wallet_balance) as TextView
        txt_message_balance = v?.findViewById<View>(R.id.txt_message_balance) as TextView

        newtonCradleLoading =v?.findViewById(R.id.newton_cradle_loading)
        newtonCradleLoading?.start()

        getUserProfileInfoData()

        image = v?.findViewById<CircleImageView>(R.id.profile_pic_id)
        txt_neti = v?.findViewById<TextView>(R.id.txt_neti_id)
        txt_profile_name = v?.findViewById<TextView>(R.id.txt_profile_name)

        newtonCradleLoading?.stop()
        newtonCradleLoading?.visibility = View.INVISIBLE

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "home")
        editor.apply()

        return v
    }


    fun getUserProfileInfoData() {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val userCall = service?.getUserProfileInfo(header)

        userCall?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("onResponse", "Main :" + response.body().toString())

                if (response.code() == 302) {

                    val profileInfo = response.errorBody()?.source()?.buffer()?.readUtf8()

                    val jsonObj = JSONObject(profileInfo!!)
                    val obj = jsonObj.getJSONObject("globalAreaInfoDTO")

                    val customNetiID = jsonObj.getString("customNetiID")
                    val fullName = jsonObj.getString("fullName")

                    val categoryName = obj.getString("categoryName")
                    val basicMobile = jsonObj.getString("basicMobile")
                    val basicEmail = jsonObj.getString("basicEmail")

                    //val name = jsonObj.getString("fullName")
                    val gender = jsonObj.getString("gender")
                    val religion = jsonObj.getString("religion")
                    val dateOfBirth = jsonObj.getString("dateOfBirth")
                    val userWalletBalance = jsonObj.getString("userWalletBalance")
                    val smsBalance = jsonObj.getString("smsBalance")
                    val imagePath = jsonObj.getString("imagePath")

                    txt_neti?.visibility = (View.VISIBLE)
                    txt_profile_name?.visibility = (View.VISIBLE)

                    txt_neti?.text = customNetiID
                    txt_profile_name?.text = fullName

                    txt_wallet_balance?.text = String.format("%, .2f", userWalletBalance.toDouble())
                    txt_message_balance?.text = String.format("%, .0f", smsBalance.toDouble())

                    //Insert user login data into local database for  showing data into offline
                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_PROFILE_INFO + "(netiId, userName, mobile, email, gender, religion, birthday, userWalletBalance, smsBalance, address, imagePath) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", arrayOf(
                            customNetiID,
                            fullName,
                            basicMobile,
                            basicEmail,
                            gender,
                            religion,
                            dateOfBirth,
                            userWalletBalance,
                            smsBalance,
                            categoryName,
                            imagePath

                        )
                    )

                }
                submitGeneralProductData()
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun submitGeneralProductData() {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${SignInActivity.Token}"

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val userProfile = dao?.getUserProfileDetails()

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = userProfile?.getImagePath()?.let { service?.getProfile(header, it) }

        //calling the api
        call?.enqueue(object : Callback<ProfileImage> {
            override fun onResponse(call: Call<ProfileImage>, response: Response<ProfileImage>) {
                Log.d("onResponse", "Main Response:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Profile Image:" + response.body()?.fileContent)

                    val photo = response.body()?.fileContent

                    Log.d("onResponse", "Photo:" + photo)

                    var theByteArray: ByteArray? = null

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (photo != "") {
                            theByteArray = Base64.getDecoder().decode(photo)
                            if (photo != null || photo != "") {
                                image?.setImageBitmap(theByteArray?.let { convertToBitmap(it) })
                            }
                        }
                    }

                }

            }

            override fun onFailure(call: Call<ProfileImage>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
}