package com.netizen.netiworld.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.netizen.netiworld.R

class FragmentBalanceWalletDeposit01 : Fragment() {

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null
    private var v: View? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       v= inflater.inflate(R.layout.balance_wallet_deposit_01, container, false)
        toolBarInit()
        return  v
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = v?.findViewById(R.id.toolbar) as Toolbar
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar!!.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        if(activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }

 /*       //Default home button enable false
        activity?.actionBar!!.setHomeButtonEnabled(true)
        activity?.actionBar!!.setDisplayShowTitleEnabled(false)
        activity?.actionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTitle!!.text = "First..."*/

        //toolbar!!.setNavigationOnClickListener { onBackPressed() }
    }

   /* private fun onBackPressed() {
        super.getActivity()?.onBackPressed()
        val intent =  Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        activity?.finish()
    }*/
}
