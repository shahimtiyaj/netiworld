create TABLE [UserLogin] (
    [netiId] nvarchar(20),
    [userName] nvarchar(20),
    [password] nvarchar(20),
    PRIMARY KEY ([netiId])
);

create TABLE [UserRole] (
    [id] INTEGER PRIMARY KEY,
    [RoleType] nvarchar(250)
);

create TABLE [UserProfileInfo] (
    [netiId] nvarchar(20),
    [userName] nvarchar(20),
    [mobile] nvarchar(20),
    [email] nvarchar(20),
    [gender] nvarchar(20),
    [religion] nvarchar(20),
    [birthday] nvarchar(20),
    [userWalletBalance] DOUBLE,
    [smsBalance] DOUBLE,
    [address] nvarchar(80),
    [imagePath] nvarchar,
    PRIMARY KEY ([netiId])
);

create TABLE [MobileBank] (
    [coreCategoryID] nvarchar(20),
    [categoryName] nvarchar(20),
    [categoryDefaultCode] nvarchar(20),
    PRIMARY KEY ([coreCategoryID], [categoryName])
);

create TABLE [BankAccNumber] (
    [coreBankAccId] nvarchar(20),
    [accShortName] nvarchar(20),
    PRIMARY KEY ([coreBankAccId], [accShortName])
);

create TABLE [MessageType] (
    [productID] nvarchar(20),
    [productName] nvarchar(20),
    [salesPrice] nvarchar(20),
    [percentVat] nvarchar(20),
    [percentTax] nvarchar(20),
    PRIMARY KEY ([productID])
);

create TABLE [UserRoleAssign] (
    [userRoleAssignID] nvarchar(20),
    [coreRoleID] nvarchar(20),
    [coreRoleName] nvarchar(20),
    PRIMARY KEY ([coreRoleID])
);

create TABLE [ProductsInfo] (
    [productID] nvarchar(20),
    [productName] nvarchar(20),
    [salesPrice] nvarchar(20),
    [percentVat] nvarchar(20),
    [productRolesAssign] nvarchar(20),
    PRIMARY KEY ([productID])
);








